<?php

class PromoClass extends Controller {

    /**
     * Get promo cash | value | based on value
     * 
     * @param type $promoObject
     * @param type $fund
     * @return flot
     */
    public static function getPromoCashBack($promoObject,$fund){
        if($promoObject->type=='PERCENTAGE'){
            $promoAmount = BaseClass::getPercentage($fund, $promoObject->value);
        } else {
            $promoAmount = $promoObject->value;
        }
        return $promoAmount;
    }
    
    public static function readAvailablePromoCount($id){
    
        $promoObject = Promo::model()->findByAttributes(array('id'=>$id));
        if($promoObject && $promoObject->can_use > 0){
            return true;
        }
        return false;
        
    }
    
    public static function debitPromoCount($id){
        $promoObject = Promo::model()->findByAttributes(array('id'=>$id));
        if($promoObject && $promoObject->can_use > 0){
            $promoObject->can_use = ($promoObject->can_use-1);
            $promoObject->update();
            return true;
        }
        return false;
    }

    /**
     * Credit promo fund to user wallet and If wallet not existed Create new Promo wallet
     * 
     * @param type $orderObject
     * @return boolean
     */
    public static function creditPromoFund($orderObject){
        //self::debitPromoCount($orderObject->promo_id);
        if($orderObject->promo_cashback > 0){
            
            if(!self::debitPromoCount($orderObject->promo_id)){
                yii::log("Duplilcate use of promo code. already consumed.","warning");
                $orderObject->promo_id = 1;
                $orderObject->promo_cashback = 0;
                $orderObject->update();
                return true;
            }
            
            $txnUserId = $orderObject->transaction()->user_id;
//            $loggedInUserId = Yii::app()->session['userid'];
            $promoWalletObject = Wallet::model()->findByAttributes(array('user_id'=>$txnUserId,'type'=>Wallet::$_PROMO_ID));
            if(!$promoWalletObject){
                $promoWalletObject = Wallet::model()->create($txnUserId, 0, Wallet::$_PROMO_ID);
            }
            /* code to add sponsor transaction */
            $promoDataArray['userId'] = 1; //TODO: 1:admin Id
            $promoDataArray['mode'] = Transaction::$_MODE_PROMO_COUPON;
            $promoDataArray['used_rp'] = $orderObject->promo_cashback;
            $promoDataArray['verified'] = Transaction::$_STATUS_VERIFIED; 
            $userSposorObject = User::model()->findByPk($txnUserId);
            $transactionObject = Transaction::model()->createTransaction($promoDataArray, $userSposorObject, 1);
        
            if($transactionObject){ 
                //create money transfer for promo cashback
                $comment = 'PROMO CashBack added to Promo Wallet';
                //create money transfer
                $postDataArray['fromwallettype'] = Wallet::$_PROMO_ID; 
                $postDataArray['towallettype'] = Wallet::$_PROMO_ID; 

                $postDataArray['comment'] = $comment;
                $moneyTransferObject = MoneyTransfer::model()->createCompleteMoneyTransfer($postDataArray, $transactionObject, $txnUserId , User::$_ADMIN_ID , $orderObject->promo_cashback ,1); //TODO: 1: Success
            }
            //send mail
            $userObject = $orderObject->user();
            BaseClass::sendInternalMail($userObject, 'PROMO Cashback Added.', 'PROMO Cashback Added to your promo wallet.');
        }
        
        return true;
    }
}
