<?php

require_once Yii::app()->basePath . '/components/Mobile_Detect.php';

class BaseClass extends Controller {

    public static $count_slug = 1;
    public $loggedInUser = null;
    public $selectedNavMenu = 1;
    public $randomCategories = null;
    public $dateArr = null;
    public $isMobile = false;

    function init() {
        parent::init();
        $detector = new Mobile_Detect;
        $this->isMobile = $detector->isMobile();
        error_reporting(E_ALL);
    }

    public static function isLoggedIn() {

        $userId = Yii::app()->session['userid']; // die;
        // $adminObject = User::model()->findByAttributes(array('id' => $userId, 'role_id' => '1'));
        if (!isset($userId)) {
            //ob_start();
            //$this->redirect('/user/login');

            header('Location:/user/login');
            die;
        }
    }
    
    public static function dateFormate($originalDate){
        return date("Y-m-d", strtotime($originalDate));
    }

    public static function isNumber($numb) {
        $decryptedNumb = (int) self::mgDecrypt($numb);
        if (is_numeric($decryptedNumb)) {
            return $decryptedNumb;
        } else {
            return "";
        }
    }
    
    public static function getWithDrowStatus($statusId) {
        if ($statusId == 1) {
            $status = "Success";
        } elseif ($statusId == 2) {
            $status = "Reject";
        } elseif ($statusId == 3) {
            $status = "Cancelled";
        } elseif ($statusId == 4) {
            $status = "Pending Payment";
        } else {
            $status = "Pending Approval";
        }
        return $status;
    }

   
    
    public static function updateWithDrawalStatus($aUniques, $status) {
        $withdrawIds = "";
        foreach ($aUniques as $key => $comment) {
            if (!empty($key)) {
                if($status == 1 ){
                    Yii::app()->user->setFlash('requestMsg', "Request has been successfully completed.");
                }elseif ($status == 2){
                    Yii::app()->user->setFlash('requestMsg', "Request has been proceed successfully.");
                }    
                $withDrawObject = WithDrawal::model()->findByPk($key);
                $withDrawObject->comment = $comment ? $comment : $withDrawObject->comment;
                $withDrawObject->status = $status;
                $withDrawObject->activity_date = date('Y-m-d');
                if (!$withDrawObject->update()) {
                    throw new CHttpException(404, 'Unable to find the code you requested.');
                }
                $withdrawIds[]= $key;
            
                if ($status == 2) {
                    //** For the refund to the user.
                    $loggedinUser = Yii::app()->session['userid'];
                    $toUserObject = User::model()->findByPk($withDrawObject->user_id);
                    
                    $postDataArray = $_POST;
                    $walletType = $withDrawObject->wallet_id;
                    $fundAmount = $withDrawObject->paid;
                    $postDataArray['mode'] = Transaction::$_MODE_WITHDRAWAL;
                    $postDataArray['used_rp'] = $fundAmount;
                    /* user wallet object */
                    $toWalletObject = Wallet::model()->findByAttributes(array('user_id' => $toUserObject->id, 'type' => $walletType));
                    
                    //Getting admin or sub-admin wallet amount 
                    $fromWalletObject = Wallet::model()->findByAttributes(array('user_id' => $loggedinUser, 'type' => $walletType));
                    
                    /* Create Transation of the perticular Order or fund */
                    $transactionObject = Transaction::model()->createTransaction($postDataArray, $toUserObject, 'admin');                    
                    $oldTransactionObject = Transaction::model()->findByPk($withDrawObject->transaction_id);
                    $postDataArray['comment'] = 'Withdrawal request cancellation  ( Transaction Id :- ' . $oldTransactionObject->transaction_id . ')';
                    $postDataArray['towallettype'] = $walletType;
                    $postDataArray['fromwallettype'] = $walletType;
                    
                    /* Create Money Transation of the perticular Order or fund */
                    $moneyTransferObject = MoneyTransfer::model()->createCompleteMoneyTransfer($postDataArray, $transactionObject,$withDrawObject->user_id , 1 ,$fundAmount ,1,$toWalletObject, $fromWalletObject);
                    Yii::app()->user->setFlash('requestMsg', "Request has been rejected successfully.");
                }
            }
        }
        return $withdrawIds;
    }

    
    public static function uploadFile($sourceFile, $destinationFolder, $destinationFileName) {
        
        if (!is_dir(strval(str_replace("\0", "", $destinationFolder)))) {
            mkdir(strval(str_replace("\0", "", $destinationFolder)), 0755, true);
        }
        try {
            $destinationFileName = preg_replace('/\s+/', '', $destinationFileName);
            move_uploaded_file($sourceFile, strval(str_replace("\0", "", $destinationFolder)) . $destinationFileName);
            return $destinationFileName;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * get bit amount
     * 
     * @param type $auctionId
     * @return object
     */
    public static function getBidAmount($auctionId) {
        $sql = 'SELECT SUM(bid_value) as bidAmount FROM auction_bids WHERE auction_id = ' . $auctionId;
        $command = Yii::app()->db->createCommand($sql);
        $bidAmountArray = $command->queryRow();
        return $bidAmountArray;
    }

  

    /* function to fetch access /*
     * 
     */

    public static function getMemberAccess() {
        try {
            $userId = Yii::app()->session['userid']; // die;
            $accessArr = array();
            $userAccessObject = UserHasAccess::model()->findByAttributes(array('user_id' => $userId));
            if ($userAccessObject) {
                $accessArr = explode(',', $userAccessObject->access);
            }
        } catch (Exception $ex) {
            echo $ex->message();
            exit;
        }
        return $accessArr;
    }
    
    /**
     * Crop profile picture...
     */
    public static function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale) {
        list($imagewidth, $imageheight, $imageType) = getimagesize($image);
        $imageType = image_type_to_mime_type($imageType);

        $newImageWidth = ceil($width * $scale);
        $newImageHeight = ceil($height * $scale);
        $newImage = imagecreatetruecolor($newImageWidth, $newImageHeight);
        switch ($imageType) {
            case "image/gif":
                $source = imagecreatefromgif($image);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                $source = imagecreatefromjpeg($image);
                break;
            case "image/png":
            case "image/x-png":
                $source = imagecreatefrompng($image);
                break;
        }
        imagecopyresampled($newImage, $source, 0, 0, $start_width, $start_height, $newImageWidth, $newImageHeight, $width, $height);
        switch ($imageType) {
            case "image/gif":
                imagegif($newImage, $thumb_image_name);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                imagejpeg($newImage, $thumb_image_name, 100);
                break;
            case "image/png":
            case "image/x-png":
                imagepng($newImage, $thumb_image_name);
                break;
        }
        chmod($thumb_image_name, 0777);
        return $thumb_image_name;
    }

    public static function getUserName() {
        $userId = Yii::app()->session['userid']; // die; 
        $userName = User::model()->findByPK(array('user_id' => $userId));
        if (!empty($userName)) {
            $name = $userName->name;
        } else {
            $name = "";
        }
        return $name;
    }
    
    public static function getCountryList() {
        return $countryObject = Country::model()->findAll();
    }

    public static function walletAmount($id) {
        $userId = Yii::app()->session['userid'];
        $walletObject = Wallet::model()->findByAttributes(array('user_id'=> $userId , 'type' => $id));
        return $walletObject;
    }

    public static function wishList() {
        $userId = Yii::app()->session['userid'];
        $wishObject = UserHasWishlist::model()->findAll(array('condition' => 'user_id=' . $userId . ' ORDER BY id DESC'));

        return $wishObject;
    }

    public static function createActivity($activity, $user) {
        //$userId = Yii::app()->session['userid'];
        if (!empty($activity)) {
            $model = new UserHasActivity;
            $model->user_id = $user;
            $model->activity = $activity;
            $model->created_at = new CDbExpression('NOW()');
            $model->updated_at = new CDbExpression('NOW()');
            $model->save(false);
        }

        return $model;
    }

    public static function getPassword() {
        $chars = '0123456789abcd345efghijklmnopq*&%$rstuvwxyzAB345CDEFGH!@#$IJKLMNOPQRSTUVWXYZ$#@!&*';
        $randomString = '';

        for ($i = 0; $i < 8; $i++) {
            $randomString .= $chars[rand(0, strlen($chars) - 1)];
        }

        return $randomString;
    }
    
     /**
     * Generate Unique integer
     * 
     * @param int $limit
     * @return int
     */
    public static function getUniqInt($limit = 6) {
        $randNumber = substr(number_format(time() * rand(), 0, '', ''), 0, $limit);
        return $randNumber;
    }
    
    public static function getUniqueAlphanumeric($limit = 10){
        return $randomString = substr(str_shuffle(md5(time())),0,$limit);
    }  
    
    public static function md5Encryption($data) {
        return md5($data);
    }

    public static function transactionStatus() {
        $userId = Yii::app()->session['userid'];
        $transactionObject = Transaction::model()->findByAttributes(array('user_id' => $userId));

        return $transactionObject;
    }

    public static function isUserHavingActiveOrder() {
        $userId = Yii::app()->session['userid'];
        $transactionObject = Order::model()->findByAttributes(array('user_id' => $userId, 'status' => 1));

        return $transactionObject;
    }

    public static function getTransactionId() {

        $randNumber = date("s") . "" . (rand(1, 999999)) . "" . date("mdy");
        return $randNumber;
    }

    public static function getUnredMails($userId) {

        return UserHasConversations::model()->count(array('condition' => 'to_user_id=' . $userId . ' AND status = 0'));
    }

    public static function isAdmin() {
        $userId = Yii::app()->session['userid'];
        $adminObject = User::model()->findByAttributes(array('id' => $userId, 'role_id' => '2'));
        if (!$adminObject) {
            header('Location:/mwadmin');
            die;
        }
    }

    public static function getWalletList() {
//       $walletObjectList = WalletType::model()->findAll(array('condition' => 'id = 1 or id = 2'));
       $walletObjectList = WalletType::model()->findAll();
        return $walletObjectList; 
    }


    /**
     * calculate Percentage
     * 
     * @param int $value1
     * @param int $value2
     * @return int
     */
    public static function getPercentage($value1, $value2, $flag = 0) {
        $percentage = ($value1 * $value2) / 100;
        return $percentage;
    }

    public static function getDirectCommission($userName) {
        $percent = Yii::app()->params['percent'];
        /* On User Basis get referral id */
        $sponserId = "'" . $userName . "'";
        $userChieldListObject = User::model()->findAll(array('condition' => 'sponsor_id = ' . $sponserId));
        $totalCommission = 0.00;
        foreach ($userChieldListObject as $userChieldObject) {
            $orderObject = Order::getOrderByValue('user_id', $userChieldObject->id);
            if (!empty($orderObject->product()->amount)) {
                $totalCommission = BaseClass::getPercentage($orderObject->product()->amount, $percent, 1);
            }
            $totalCommission+=$totalCommission;
        }
        return $totalCommission;
    }

    public static function getReCaptcha() {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < 5; $i++) {
            $randomString .= $chars[rand(0, strlen($chars) - 1)];
        }
        $_SESSION['captcha'] = strtolower($randomString);
        return $_SESSION['captcha'];
    }

    /**
     * Send SMS
     * 
     * @param number $toNumber
     * @param string $message
     * @return response
     */
    public static function sendSMS($config) { 
        try {
            $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
                        'api_key' => Yii::app()->params['smsConfig']['api_key'],
                        'api_secret' => Yii::app()->params['smsConfig']['api_secret'],
                        'to' => $config['to'],
                        'from' => 'MavWealth',
                        'text' => $config['text']
            ]);
            //$url = "";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
        } catch (Exception $ex) {
            echo $ex->message();
            exit;
        }
        return $response;
    }


    public static function downloadFile($outputFilePath) {

        header('Content-Description: File Transfer');

        header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
        header('Pragma: public');
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        // force download dialog
        header('Content-Type: application/force-download');
        header('Content-Type: application/octet-stream', false);
        header('Content-Type: application/download', false);
        header('Content-Type: application/pdf', false);
        // use the Content-Disposition header to supply a recommended filename
        header('Content-Disposition: attachment; filename="' . basename($outputFilePath) . '";');
        header('Content-Transfer-Encoding: binary');
        ob_clean();
        flush();
        readfile($outputFilePath);
    }


    public static function getGenoalogyTree($userId) {
        $genealogyListObject = Genealogy::model()->findAll(array('condition' => 'parent = "' . $userId . '"'));
        return $genealogyListObject;
    }

    public static function getGenoalogyTreeChild($userId, $position) {
        $parentId = (int) $userId; 
        $genealogyListObject = Genealogy::model()->findByAttributes(array('parent' => $parentId , 'position' => $position));
        return $genealogyListObject;
    }


    public static function getBinaryTreeChild($userId, $date, $position) {
        $genealogyListObject = Genealogy::model()->findAll(array('condition' => 'parent = ' . $userId . ' AND position = ' . $position . 'AND updated_at = "2015-05-19" ', 'order' => 'position asc'));
        return $genealogyListObject;
    }
    
    public static function getLeftRightMember($userId, $position) {
        $geneObject = Genealogy::model()->findByAttributes(array('parent' => $userId, 'position' => $position));
        $userCountIncrement = 0;
        if (count($geneObject)) {
            $userCount = User::model()->count();
            for ($i = 1; $i <= $userCount; $i++) {
                if ($i == 1) {
                    $geneObjectNode = Genealogy::model()->findByAttributes(array('parent' => $geneObject->user_id, 'position' => $position));
                    if (count($geneObjectNode)) {
                        $userCountIncrement++;
                        $userId = $geneObjectNode->user_id;
                    } else {
                        $userCountIncrement++;
                        $userId = $geneObject->user_id;
                        break;
                    }
                } else {
                    $geneObjectNode = Genealogy::model()->findByAttributes(array('parent' => $userId, 'position' => $position));
                    if (count($geneObjectNode)) {
                        $userCountIncrement++;
                        $userId = "";
                        $userId .= $geneObjectNode->user_id;
                    } else {
                        $userCountIncrement++;
                        $userId;
                        break;
                    }
                }
            }
            return $userCountIncrement;
        }
    }
    
    public static function getLeftRightPurchase($totalPurchase) {
        $geneObject = Genealogy::model()->findByAttributes(array('user_id' => $totalPurchase));
        return $geneObject;
    }
    
    public static function getLeftPurchase($currentUserId) {
        $totalUserPurchase = 0;
        $todayDate = date('Y-m-d');
        $geneObject = Genealogy::model()->findByAttributes(array('user_id' => $currentUserId));
        $leftUsers = $geneObject->left_users;
        $connection = Yii::app()->db;
        if ($leftUsers != '') {
            /* Query to get left total purchase */
            $command = $connection->createCommand('select sum(package_price) as totalPurchase FROM `order` WHERE user_id IN (' . $leftUsers . ') AND type = "AddCash" AND status = 1 AND created_at = "' . $todayDate . '"');
            $row = $command->queryAll();
            foreach ($row as $key) {
                
            }
            if (!empty($key['totalPurchase'])) {
                $totalUserPurchase = round($key['totalPurchase']);
            }
        } else {
            $totalUserPurchase = 0;
        }
        return $totalUserPurchase;
    }

    /**
     * For the Right Purchase Binary
     * @param type $totalPurchase
     * @return int
     */
    public static function getRightPurchase($totalPurchase) {
        $totalUserPurchase = 0;
        $todayDate = date('Y-m-d');
        $geneObject = Genealogy::model()->findByAttributes(array('user_id' => $totalPurchase));
        $rightUsers = $geneObject->right_users;
        if ($rightUsers != '') {
            $connection = Yii::app()->db;
            /* Query to get left total purchase */
            $command = $connection->createCommand('select sum(package_price) as totalPurchase FROM `order` WHERE user_id IN (' . $rightUsers . ') AND type = "AddCash" AND status = 1 AND created_at = "' . $todayDate . '"');
            $row = $command->queryAll();
            foreach ($row as $key) {
                
            }
            if (!empty($key['totalPurchase'])) {
                $totalUserPurchase = round($key['totalPurchase']);
            }
        } else {
            $totalUserPurchase = 0;
        }
        return $totalUserPurchase;
    }

    public static function getBinaryCalculationChild($userId, $position) {
        $genealogyListObject = BinaryCommissionTest::model()->findAll(array('condition' => 'parent = ' . $userId . ' AND position = "' . $position . '"', 'order' => 'position asc'));
        return $genealogyListObject;
    }
    
    public static function BetweenDates($startDate, $endDate) {
        Yii::trace('Binary Generation initiated: Start Date:' . $startDate . 'End Date:' . $endDate);
        $startStamp = strtotime($startDate);
        $endStamp = strtotime($endDate);

        if ($endStamp > $startStamp) {
            while ($endStamp >= $startStamp) {

                $dateArr[] = date('Y-m-d', $startStamp);

                $startStamp = strtotime(' +1 day ', $startStamp);
            }
            return $dateArr;
        } else {
            $dateArr[] = $startDate;
            return $dateArr;
        }
    }

    public static function getLeftRightNode($binaryCommissionObject, $position) {
//        echo "<pre>"; print_r($binaryCommissionObject->user_id);exit;
        if ($position == 'left') {
            echo "left: " . $binaryCommissionObject->order_amount . "</br>";
            Yii::app()->session['totalLeftCount'] = Yii::app()->session['totalLeftCount'] + $binaryCommissionObject->order_amount;
        } else {
            echo "right : " . $binaryCommissionObject->order_amount . "</br>";
            Yii::app()->session['totalRightCount'] = Yii::app()->session['totalRightCount'] + $binaryCommissionObject->order_amount;
        }
//        echo $binaryCommissionObject->user_id;
        self::binaryCalculation($binaryCommissionObject->user_id, $binaryCommissionObject->position);
    }

    //1. find the right and left node for user
    //2. Find the extreem right for right node 
    //3. Find the extreem Left for left node
    //4. Calculate the purchased amount
    //5. Compare both and leftamount - rightamount
    //remaining amount carryforward. 

    public static function binaryCalculation($userId, $position) {
//        echo "------------".$userId."----------";
        $binaryCommissionLeftObject = BinaryCommissionTest::model()->findByAttributes(array('parent' => $userId, 'position' => $position));
        if (!empty($binaryCommissionLeftObject)) {
            self::getLeftRightNode($binaryCommissionLeftObject, $binaryCommissionLeftObject->position);
            return 1;
        }
        return 0;
    }

    /**
     * Generate Binary calculation
     * 
     * @param int $userId
     * @return int
     */
    public static function getBinaryTest($userId) {

        $binaryCommissionLeftObject = BinaryCommissionTest::model()->findByAttributes(array('parent' => $userId, 'position' => 'left'));
        $binaryCommissionRightObject = BinaryCommissionTest::model()->findByAttributes(array('parent' => $userId, 'position' => 'right'));

        if (!empty($binaryCommissionLeftObject) && !empty($binaryCommissionRightObject)) {

            $commissionAmount = $binaryCommissionRightObject->order_amount;
            if ($binaryCommissionLeftObject->order_amount <= $binaryCommissionRightObject->order_amount) {
                $commissionAmount = $binaryCommissionLeftObject->order_amount;
            }
            $parentCommissionObject = BinaryCommissionTest::model()->findByAttributes(array('user_id' => $userId));

            $getPercentage = self::getPercentage($commissionAmount, 10);
            // self::createBinaryTransaction($userId,$getPercentage);
            $parentCommissionObject->commission_amount = ($parentCommissionObject->commission_amount + $getPercentage);
            $parentCommissionObject->save(false);
            self::getBinaryTest($binaryCommissionRightObject->user_id);
            self::getBinaryTest($binaryCommissionLeftObject->user_id);
        } else {
            return 1;
        }
    }

    public static function parentParentCommission($userId) {
//        echo "parent Id :" . $userId;
        //read parent object
        $parentCommissionObject = BinaryCommissionTest::model()->findByAttributes(array('user_id' => $userId));
        //verify parent object
        if ($parentCommissionObject) {
            echo "My parent Id:" . $parentCommissionObject->user_id . "</br>";
            echo "Commission Amount : " . $parentCommissionObject->commission_amount . "</br>";
            self::parentParentCommission($parentCommissionObject->parent);
            $addParentAmountObject = BinaryCommissionTest::model()->findByAttributes(array('user_id' => $parentCommissionObject->parent));
            if ($addParentAmountObject) {
                $addParentAmountObject->commission_amount = ($addParentAmountObject->commission_amount + $parentCommissionObject->commission_amount);
                $addParentAmountObject->save(false);
            }
        }
    }

    public static function getParentTotalAmount($userId) {
        $binaryCommissionLeftObject = BinaryCommissionTest::model()->findByAttributes(array('parent' => $userId, 'position' => 'left'));
        $binaryCommissionRightObject = BinaryCommissionTest::model()->findByAttributes(array('parent' => $userId, 'position' => 'right'));

        $commissionAmount = $binaryCommissionRightObject->commission_amount;
        if ($binaryCommissionLeftObject->commission_amount <= $binaryCommissionRightObject->commission_amount) {
            $commissionAmount = $binaryCommissionLeftObject->commission_amount;
        }
        $parentCommissionObject = BinaryCommissionTest::model()->findByAttributes(array('user_id' => $userId));
        $getTotalPercentage = (2 * $commissionAmount);
        $parentCommissionObject->commission_amount = ($parentCommissionObject->commission_amount + $getTotalPercentage);
        $parentCommissionObject->save(false);
    }

    public static function createBinaryTransaction($userId, $getPercentage) {

        $postDataArray['paid_amount'] = $getPercentage;
        /* code to fetch parent data */
        $userObject = User::model()->findByPk($userId);

        /* code to fetch parent wallet */
        $toUserWalletObject = Wallet::model()->findByAttributes(array('user_id' => $userId, 'type' => 3));
        if (!$toUserWalletObject) {
            //create wallet for to user
            $fund = 0;
            $toUserWalletObject = Wallet::model()->create($userId, $fund, 3);
        } else {
            $toUserWalletObject->fund = ($toUserWalletObject->fund) + ($getPercentage);
            $toUserWalletObject->save(false);
        }

        /* code to fetch admin wallet data */

        $fromUserWalletObject = Wallet::model()->findByAttributes(array('user_id' => 1, 'type' => 3));
        if ($fromUserWalletObject) {
            $fromUserWalletObject->fund = ($fromUserWalletObject->fund) - ($getPercentage);
            $fromUserWalletObject->save(false);
        }

        $transactionObjectect = Transaction::model()->createTransaction($postDataArray, $userObject);
        if ($transactionObjectect) {
            $postDataArray['comment'] = "Binary Commission Transfered";
            $postDataArray['walletId'] = $fromUserWalletObject->id;
            $postDataArray['toWalletId'] = $toUserWalletObject->id;
            $moneyTransferObject = MoneyTransfer::model()->createMoneyTransfer($postDataArray, $userObject, $transactionObjectect->id, $transactionObjectect->paid_amount);
        }
        return true;
    }

    public static function getRandPosition() {
         $getPosition = mt_rand(1, 2);
        if ($getPosition == 1) {
            $getPosition = 'left';
        } else {
            $getPosition = 'right';
        }
        return $getPosition;
    }

    public static function recurse_copy($src, $dst) {
//        echo "assd"; die;
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
        return $dir;
    }

    /* For the package info */

    public static function getPackageColor($userId) {
         $color = "blank one" ; 
//        $userId = self::isNumber($userId);
//        if (empty($userId)) {
//            return "no";
//        }
        $userObject = User::model()->findByAttributes(array('id' => $userId));

        switch ($userObject->membership_type) {
            case 1:
                $color = "yellow";
                break;
            case 2:
                $color = "red";
                break;
            case 3:
                $color = "green"; 
                break;
            default:
                $color = ($userObject->status == 1) ? "user-active" : "user-inactive";
        }
        
        return $color;
    }


    /**
     * find last purchase node
     * 
     * @param int $nodeId - input node
     */
    public static function setPurchaseNode($parentObject) {
        $nodeId = $parentObject->user_id;
        $todayDate = date('Y-m-d');
        //$date = date('Y-m-d');
        //find left present | not
        //$totalLeftPurchase = 0;
        $binaryCommissionObjectLeft = Genealogy::model()->findByAttributes(array('parent' => $nodeId, 'position' => 'left'));

        if ($binaryCommissionObjectLeft) {
            //echo "<pre>"; print_r($binaryCommissionObjectLeft);//exit;
            //  $totalLeftPurchase = $binaryCommissionObjectLeft->order_amount;
            //echo "Left Id: ".$binaryCommissionObjectLeft->user_id;
            //echo "Left :".$binaryCommissionObjectLeft->order_amount;
            if ($todayDate == $binaryCommissionObjectLeft->order_date) {
                $binaryCommissionObjectLeft = self::setPurchaseNode($binaryCommissionObjectLeft);
                $parentObject->left_purchase = $binaryCommissionObjectLeft->total_purchase_amount;
                $parentObject->save(false);
            } else {
                $binaryCommissionObjectLeft = self::setPurchaseNode($binaryCommissionObjectLeft);
            }
        }
        //echo $totalLeftPurchase;
        // exit;
        //find right present | not

        $binaryCommissionObjectRight = Genealogy::model()->findByAttributes(array('parent' => $nodeId, 'position' => 'right'));
        if ($binaryCommissionObjectRight) {
            //echo "Left Id: ".$binaryCommissionObjectRight->user_id;
            //echo "Left :".$binaryCommissionObjectRight->order_amount;
            if ($todayDate == $binaryCommissionObjectRight->order_date) {
                $binaryCommissionObjectRight = self::setPurchaseNode($binaryCommissionObjectRight);
                $parentObject->right_purchase = $binaryCommissionObjectRight->total_purchase_amount;
                $parentObject->save(false);
            } else {
                $binaryCommissionObjectRight = self::setPurchaseNode($binaryCommissionObjectRight);
            }
        }
//        exit;
        // Total Purchase amount
        $totalPurchase = ($parentObject->right_purchase + $parentObject->left_purchase + $parentObject->order_amount);
        $parentObject->total_purchase_amount = $totalPurchase;
        $parentObject->save(false);
        //binary calculation
        $parentObject = self::setBinary($parentObject);
        return $parentObject;
    }

    /**
     * Calculate Binary for specific node
     * 
     * @param objectd $parentObject
     * @return object
     */
    public static function setBinary($parentObject) {
        $nodeId = $parentObject->user_id;
        $isValidNode = self::binaryEligible($nodeId);
        //binary calculation percentage
        $binaryPercentage = 0.1;

        //is valid node
        if ($isValidNode) {
            $leftNodeAmount = $parentObject->left_purchase + $parentObject->left_carry;
            $rightNodeAmount = $parentObject->right_purchase + $parentObject->right_carry;
            if ($leftNodeAmount == $rightNodeAmount) {
                $binaryAmount = ($leftNodeAmount * $binaryPercentage);
                $parentObject->left_carry = 0;
                $parentObject->right_carry = 0;
            }
            if ($leftNodeAmount < $rightNodeAmount) {
                $binaryAmount = ($leftNodeAmount * $binaryPercentage);
                $parentObject->left_carry = 0;
                $parentObject->right_carry = ($rightNodeAmount - $leftNodeAmount);
            }
            if ($leftNodeAmount > $rightNodeAmount) {
                $binaryAmount = ($rightNodeAmount * $binaryPercentage);
                $parentObject->right_carry = 0;
                $parentObject->left_carry = ($leftNodeAmount - $rightNodeAmount);
            }
            $parentObject->commission_amount = $binaryAmount;
            $parentObject->save(false);
            if ($binaryAmount != 0) {
                self::createCommissionTransaction($binaryAmount, $parentObject);
            }
        }
        return $parentObject;
    }

    /**
     * 
     * @param type $binaryAmount
     * @param type $parentObject
     * @return int
     */
    public static function createCommissionTransaction($binaryAmount, $parentObject) {
        Yii::trace('createCommissionTransaction Amount:' . $binaryAmount);
        Yii::trace(CVarDumper::dumpAsString($parentObject));

        $nodeId = $parentObject->user_id;
        $postDataArray['used_rp'] = $binaryAmount;
        $postDataArray['status'] = 1;
        $postDataArray['verified'] = Transaction::$_STATUS_VERIFIED; //System Generated Binary Comission is mark automatically.
        $postDataArray['mode'] = Transaction::$_MODE_BINARYCOMMISSION;
        
        $userObject = User::model()->findByAttributes(array('id' => $parentObject->user_id));
        $adminObject = User::model()->findByAttributes(array('id' => 1));
        $transactionObject = Transaction::model()->createTransaction($postDataArray, $adminObject, $postDataArray['status']);
        /* wallet object */
        $toUserWalletObject = Wallet::model()->findByAttributes(array('user_id' => $nodeId, 'type' => 2));

        $postDataArray['userId'] = $nodeId;
        $adminWalletObject = Wallet::model()->findByAttributes(array('user_id' => Yii::app()->params['adminId'] , 'type' => 2));
        
        $postDataArray['status'] = 1;
        $postDataArray['comment'] = 'Binary Commission Transferred.';
        $postDataArray['fromwallettype'] = 2 ; // Admin Cashback wallet
        $postDataArray['towallettype'] = 2 ; // User Cashback wallet

        Yii::trace('Create Money Transfer for Binary Commission ');
        Yii::trace(CVarDumper::dumpAsString($postDataArray));

        $moneyTransferObject = MoneyTransfer::model()->createCompleteMoneyTransfer($postDataArray, $transactionObject, $userObject->id , Yii::app()->params['adminId'] ,$binaryAmount );
        Yii::trace('Money Transfer Created');
        Yii::trace(CVarDumper::dumpAsString($moneyTransferObject));
        
        // Send internal mail.
        BaseClass::sendInternalMail($userObject, 'Binary Commission Transferred.', 'Binary Commission Transferred.');
        Yii::trace('Internal Mail Sent to :' . $userObject->id);
        return 1;
    }

    /**
     * Check binary node both the purchase amount for given node
     * 
     * @param int $nodeId
     * @return boolean | amount
     */
    public static function binaryEligible($nodeId) {
        return true;
//        //find the left node amount
//        $binaryCommissionObjectLeft = BinaryCommissionTest::model()->findByAttributes(array('parent' => $nodeId,'position'=>'left'));   
//        //find left node amount
//        $binaryCommissionObjectRight = BinaryCommissionTest::model()->findByAttributes(array('parent' => $nodeId ,'position'=>'right'));
//        if (!empty($binaryCommissionObjectLeft) && !empty($binaryCommissionObjectRight)) {
//            //send both node purchase amount
//            return true;
//        }
//        return false;
    }

    public static function getCountryName() {
        $countryObject = Country::model()->findAll();
        return $countryObject;
    }

    /**
     * Checks if the current route matches with given routes
     * @param array $routes
     * @return bool
     */
    public static function isActive($routes = array()) {
        $routeCurrent = '';
        if ($this->module !== null) {
            $routeCurrent .= sprintf('%s/', $this->module->id);
        }
        $routeCurrent .= sprintf('%s/%s', $this->id, $this->action->id);
        foreach ($routes as $route) {
            $pattern = sprintf('~%s~', preg_quote($route));
            if (preg_match($pattern, $routeCurrent)) {
                return "active";
            }
        }
        return "";
    }

    /**
     * 
     */

    /**
     * Get packages based on purchase status.
     */
    public static function getPackagesOnPurchaseStatus($criteria, $pstatus, $orderBy = NULL) {
        if (!$orderBy) {
            $orderBy = 'close_date';
        }

        $criteria->condition = "(t.status = :status AND t.start_date <= :curDate AND t.close_date > :curDate AND p.purchase_type = :pstatus)"; // Only Bid AND Buy options.
        $criteria->params = (array(':status' => 1, ':curDate' => date('Y-m-d H:i:s'), ':pstatus' => $pstatus/* ':noDays' => 5/* 24 hours */));
        $criteria->limit = 8;
        $criteria->order = ('t.' . $orderBy . ' ASC');

        //$criteria->order = ('t.created_at DESC');
        $auctionListsObject = Auction::model()->findAll($criteria);
        return $auctionListsObject;
    }

    /**
     * Function to get google contacts using CURL.
     * 
     * @param type $url
     * @param type $post
     * @return type
     */
    public static function curl($url, $post = "") {
        $curl = curl_init();
        $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
        curl_setopt($curl, CURLOPT_URL, $url);
        //The URL to fetch. This can also be set when initializing a session with curl_init().
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        //The number of seconds to wait while trying to connect.
        if ($post != "") {
            curl_setopt($curl, CURLOPT_POST, 5);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        }
        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
        //The contents of the "User-Agent: " header to be used in a HTTP request.
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        //To follow any "Location: " header that the server sends as part of the HTTP header.
        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);
        //To automatically set the Referer: field in requests where it follows a Location: redirect.
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        //The maximum number of seconds to allow cURL functions to execute.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        //To stop cURL from verifying the peer's certificate.
        $contents = curl_exec($curl);
        curl_close($curl);
        return $contents;
    }

    /**
     * count auction which have some bids.
     * @return type
     */
    public static function getBidAuctionCount() {
        // Get all auctions which have some bids.
        
        $sql = "SELECT count(distinct(au.id)) as auction_count
            FROM auction_bids t
            JOIN auction au ON t.auction_id = au.id
            JOIN product p ON p.id = au.product_id
            WHERE p.purchase_type IN(2,3)
            AND au.status = 1
            AND au.start_date <= NOW()
            AND au.close_date > NOW()";
     
        $auctionBidsCount = Yii::app()->db->createCommand($sql)->queryAll();        
        return $auctionBidsCount[0]['auction_count'];
    }
    
    /**
     * Count all active auctions.
     * @return type
     */
    public static function getAuctionsCount() {
        // Auctions count which are active.
        $sql = "SELECT count(t.id) as auction_count
            FROM auction t            
            JOIN product p ON p.id = t.product_id
            WHERE p.purchase_type IN(2,3)
            AND t.status = 1
            AND t.start_date <= NOW()
            AND t.close_date > NOW()";
        
        $openAuctionCount = Yii::app()->db->createCommand($sql)->queryAll(); // Get all auctions which have some bids.        
        return $openAuctionCount[0]['auction_count'];
    }
    
    /**
     * MyCrypt encription
     * 
     * @param type $key
     * @return type
     */
    public static function mgEncrypt($input) { 
        $key = Yii::app()->params['encryptionPassword'];
        $cryptedText = mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128, $key, $input, MCRYPT_MODE_ECB
        );
        $cryptedValue = str_replace("+", "*", base64_encode($cryptedText));
        return trim($cryptedValue);
    }

    /**
     * MyDecrypt 
     * 
     * @param type $key
     * @return type
     */
    public static function mgDecrypt($input) {
        $input = str_replace("*", "+", $input); 
        $key = Yii::app()->params['encryptionPassword'];
        return trim(mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128, $key, base64_decode($input), MCRYPT_MODE_ECB
        ));
    }
    
    public static function userLoginBrowserInfo($getUserObject, $status) {
        // Log user login details
        //print_r($getUserObject); die;
        $postDataArray['user_id'] = $getUserObject->id;
        $ip = $postDataArray['ip'] = $_SERVER['REMOTE_ADDR']; // the IP address to country
        if (!empty($ip)) {
            /*Get user ip address details with geoplugin.net*/
            $countryByIp = @unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
            if ($countryByIp && $countryByIp['geoplugin_countryName'] != '') {
                $postDataArray['country'] = $countryByIp['geoplugin_countryName'];
            } else {
                $postDataArray['country'] = "NA";
            }
        }
        $postDataArray['browser'] = CommonHelper::getBrowser();
        $postDataArray['operating_system'] = CommonHelper::getOS();
        $postDataArray['status'] = $status; // Login status Success.

        UserActivityLog::model()->create($postDataArray);
    }
    
     /**
     * getSocialUserName
     * @param type $userName
     * @return type
     */
    public static function getSocialUserName($userName) {
        if(strpos($userName, " ") !== false){ //removing space
            $userName = str_replace(' ', '', $userName);
        }

        if (strlen($userName) > 8) {
            $resultUser = strtolower(substr($userName, 0, 6));
        } else {
            $resultUser = strtolower($userName);
        }
        return self::isUserExisted($resultUser);
    }
    
     /**
     * Is user existed
     * @param type $resultUser
     * @return object
     */
    public static function isUserExisted($resultUser) {
        $userObject = User::model()->findByAttributes(array('name' => $resultUser));
        $newresultUser = $resultUser;
        if ($userObject) {
            $resultUser = substr($resultUser, 0, 6);
            $str = 'abcdefghijklmnopqrstuvwxyz0123456789&!$@*#&';
            $randomCharOne = $str[rand(0, strlen($str)-1)];
            $randomCharTwo = $str[rand(0, strlen($str)-1)];;
            $newresultUser = trim($resultUser . $randomCharOne.$randomCharTwo);
            self::isUserExisted($newresultUser);
        }
        return $newresultUser;
    }
    
        public static function geneaologyUsers($sponserObject ,$position){
        /* Condition for they have the child or not */
        $geneObject = Genealogy::model()->findByAttributes(array('parent' => $sponserObject->id, 'position' => $position));

        if (count($geneObject) > 0) {
            $parentId = "";
            $userCount = User::model()->count();
            for ($i = 1; $i <= $userCount; $i++) {

                if ($i == 1) {
                    $geneObjectNode = Genealogy::model()->findByAttributes(array('parent' => $geneObject->user_id, 'position' => $position));
                    if (count($geneObjectNode)) {
                        $parentId = $geneObjectNode->user_id;
                    } else {
                        $parentId = $geneObject->user_id;
                        break;
                    }
                } else {
                    $geneObjectNode = Genealogy::model()->findByAttributes(array('parent' => $parentId, 'position' => $position));
                    if (count($geneObjectNode)) {
                        $parentId = "";
                        $parentId .= $geneObjectNode->user_id;
                    } else {
                        $parentId;
                        break;
                    }
                }
            }
        } else {
            $parentId = $sponserObject->id;
        }
        
        return $parentId ;
        
    }
    
    public static function getGatewayId($slug) {
        $gatewayObject = Gateway::model()->findByAttributes(array('slug' => $slug));
        if ($gatewayObject) {
            return $gatewayObject->id;
        }
        return 0;
    }
    
    public static function createUploadFilename($filename){
        $newFileName = preg_replace("/[^a-zA-Z0-9.]/", "_", $filename);
        return $newFileName;
    }
    
     /**
     * Create User wallet
     * 
     * @param object $userId
     * @return boolean
     */
    public static function _createNewUserWallet($userId){
        $requiredWalletArray = Yii::app()->params['walletIdList'];
    
        foreach ($requiredWalletArray as $walletType){ 
            //create new user wallet
            $walletObject = Wallet::model()->findByAttributes(array('user_id' => $userId, 'type' => $walletType));
            if(empty($walletObject)){ //If wallet is not exist, then only creating new one
                Wallet::model()->create($userId, 0 , $walletType);
            }
        }
        return true;
    }
    
     /**
     * 
     * @param type $userId
     * @param type $constantId
     */
    public static function userCountIncrement($userId, $constantId = "") {
        if(empty($constantId)){
           $constantId =  $userId;
        }
        $userObject = Genealogy::model()->findByAttributes(array('user_id' => $userId));
        $userId = $userObject->parent;
        $userPosition = $userObject->position;
        if ($userId) {
            $parentObject = Genealogy::model()->findByAttributes(array('user_id' => $userId));
            if ($userPosition == 'right') {

                $parentObject->right_count = $parentObject->right_count + 1;
                $prefix = "";
                if (!empty($parentObject->right_users))
                    $prefix = ",";
                $parentObject->right_users = $parentObject->right_users . $prefix . $constantId;
            }
            if ($userPosition == 'left') {

                $parentObject->left_count = $parentObject->left_count + 1;
                $prefix = "";
                if (!empty($parentObject->left_users))
                    $prefix = ",";
                $parentObject->left_users = $parentObject->left_users . $prefix . $constantId;
            }

            if(empty($parentObject->reg_check_date)){
                $parentObject->reg_check_date = null;
            }
            if (!$parentObject->update(false)) {
                echo "<pre>";
                print_r($parentObject->getErrors());
                exit;
            }

            if ($userId != 1) {
                
                $parentId = self::userCountIncrement($userId, $constantId);
            }
        }
    }

    
    public static function sendInternalMail($userObject, $subject, $msg) {
        $postDataArray['subject'] = $subject;
        $mailObject = Mail::model()->create($postDataArray);
        if ($mailObject) {
            $postDataArray['mail_id'] = $mailObject->id;
            $postDataArray['from_user_id'] = 1; // Admin
            $postDataArray['to_user_id'] = $userObject->id;
            $postDataArray['department_id'] = self::getDepartmentId(); // Support.
            $postDataArray['status'] = 0; // Unread.                                
            $postDataArray['message'] = $msg;
            $postDataArray['replied_by'] = 0;
            UserHasConversations::model()->create($postDataArray, '');
        }
    }

     /**
     * Get Support department ID.
     * @return type
     */
    public static function getDepartmentId() {
        $deptName = Yii::app()->params['defaultDepartmentName'];
        $deptObject = Department::model()->findByAttributes(array('slug' => 'support'));
        return $deptObject->id;
    }
    
    public static function get404Page() {
        throw new CHttpException(404, 'Page not found');
    }
    
    public static function get404() {
        throw new CHttpException(404, 'Page not found');
    }
    
    public static function newBinaryFlow($fromDate) {
        
        Yii::trace('New Binary Flow Create From Date: ' . $fromDate);
//        $genealogyArr = Genealogy::model()->findAll();
        
        $dataProvider = new CActiveDataProvider('Genealogy');
        $genealogyIterator = new CDataProviderIterator($dataProvider);
        
        foreach ($genealogyIterator as $genealogyObject) {
            if ($genealogyObject->id == 1) continue;
            Yii::trace('Read Genealogy Parent Object in New Binary Flow');
            Yii::trace(CVarDumper::dumpAsString($genealogyObject));
            
            if ($genealogyObject) {
                Yii::trace('Node Genealogy Object');
                Yii::trace(CVarDumper::dumpAsString($genealogyObject));
                
                $leftNodeUserList = $genealogyObject->left_users;
                $rightNodeUserList = $genealogyObject->right_users;
                //if left list and right list empty then skip
                if (empty($leftNodeUserList) && empty($rightNodeUserList)) {
                    Yii::trace('Empty Left and Right');
                    continue;
                }
                
                $leftTotalPurchase = self::getTotalPurchase($leftNodeUserList, $fromDate);
                $righTotalPurchase = self::getTotalPurchase($rightNodeUserList, $fromDate);

                if ($leftTotalPurchase == 0 && $righTotalPurchase == 0) {
                    Yii::trace('Purchase left right 0');
                    continue;
                }

                $leftTotalPurchaseWitCarry = $leftTotalPurchase + $genealogyObject->left_carry;
                $righTotalPurchaseWitCarry = $righTotalPurchase + $genealogyObject->right_carry;
                //$carrayAmount['totalPurchaseAmount'] = ($userTotalPurchase + $leftTotalPurchase + $righTotalPurchase);
                $binaryData = self::getCarryAmount($leftTotalPurchaseWitCarry, $righTotalPurchaseWitCarry, $genealogyObject->user_id);

                $binaryData['righTotalPurchase'] = $righTotalPurchase;
                $binaryData['leftTotalPurchase'] = $leftTotalPurchase;
                $binaryData['fromDate'] = $fromDate;
                $binaryData['user_id'] = $genealogyObject->user_id;
                $binaryData['previous_left_carry'] = $genealogyObject->left_carry;
                $binaryData['previous_right_carry'] = $genealogyObject->right_carry;
                Yii::trace('BinaryGenerationReport Create');
                
                $binarygenerationObject = BinaryGenerationReport::model()->create($binaryData);
                Yii::trace(CVarDumper::dumpAsString($binarygenerationObject));
                /* Update left carry and right carry in genealogy table make sure it should update before update genealogy */
                $genealogyObject->left_carry = $binaryData['left_carry'];
                $genealogyObject->right_carry = $binaryData['right_carry'];
                $genealogyObject->update();
                $binaryAmount = $binaryData['commission_amount'];

                if (!empty($binaryAmount)) {
                    Yii::trace('Create commission' . $binaryAmount);
                    self::createCommissionTransaction($binaryAmount, $genealogyObject);
                }
                
                Yii::trace('Update membership type of User');
                
                $updateMembershipType = Yii::app()->db->createCommand('update `user` u left join `order` o on u.id = o.user_id  and o.status = 1 and o.end_date > :date 
                and o.package_price = ( select max(package_price) from `order` where user_id = u.id and end_date > :date and status = 1 )
                set u.membership_type = IFNULL(o.package_id,0) where u.role_id = 1 and u.membership_type != IFNULL(o.package_id , 0)')->bindValue('date',$fromDate)->query();
                
                Yii::trace(CVarDumper::dumpAsString($updateMembershipType));
                
            }
        }
    }
    
    public static function getTotalPurchase($userList, $fromDate) {

        Yii::trace('Total purchase user list:' . $userList . 'date:' . $fromDate);

        $totalPurchase = 0;

        if (empty($userList)) {
            return $totalPurchase;
        }
        //$fromDate = date("Y-m-d"); //2015-06-14
        $connection = Yii::app()->db;
        $command = $connection->createCommand('select sum(package_price) as totalPurchase FROM `order` WHERE created_at ="' . $fromDate . '" AND user_id IN (' . $userList . ') AND type = "'.Order::$_TYPE_ADDCASH.'" AND status = 1');
        $row = $command->queryAll();
        foreach ($row as $key) {
            
        }

        if (!empty($key['totalPurchase'])) {

            $totalPurchase = $key['totalPurchase'];
        }
        Yii::trace('Total purchase' . $totalPurchase);
        return $totalPurchase;
    }
    
    /**
     * 
     * @param type $leftNodeAmount
     * @param type $rightNodeAmount
     * @param type $nodeId
     * @return type
     */
    public static function getCarryAmount($leftNodeAmount, $rightNodeAmount, $nodeId) {
        $binaryPercentage = 0.1;
        if ($leftNodeAmount == $rightNodeAmount) {
            $binaryAmount = ($leftNodeAmount * $binaryPercentage);
            $carryArray['left_carry'] = 0;
            $carryArray['right_carry'] = 0;
        }
        if ($leftNodeAmount < $rightNodeAmount) {
            $binaryAmount = ($leftNodeAmount * $binaryPercentage);
            $carryArray['left_carry'] = 0;
            $carryArray['right_carry'] = ($rightNodeAmount - $leftNodeAmount);
        }
        if ($leftNodeAmount > $rightNodeAmount) {
            $binaryAmount = ($rightNodeAmount * $binaryPercentage);
            $carryArray['right_carry'] = 0;
            $carryArray['left_carry'] = ($leftNodeAmount - $rightNodeAmount);
        }

        $carryArray['commission_amount'] = $binaryAmount;
        $carryArray['flush'] = 0;

        if ($nodeId != '1') { //1: Super Admin
            $limit = self::cappingLimit($nodeId);
            if ($binaryAmount > $limit) {
                //set commitsion amount
                $carryArray['commission_amount'] = $limit;
                //added flush amount in user account
                $carryArray['flush'] = ($binaryAmount - $limit);
            }
        }
        return $carryArray;
    }
    
    /**
     * 
     * @param type $parentObject
     * @return type
     */
    public static function cappingLimit($userId) {
        $limit = 0;
        Yii::trace('Create capping limit');
        $userObject = User::model()->findByAttributes(array('id' => $userId));

        if (!empty($userObject)) {
            $packageObject = Package::model()->findByAttributes(array('id' => $userObject->membership_type));
            if (!empty($packageObject)) {
                $limit = $packageObject->binary_capping;
            }
        }
        Yii::trace('Capping Limit' . $limit);
        return $limit;
    }
    
        /**
     * transaction
     * 
     * @param string $message
     * @param object $Objbect
     */
    public static function traceLog($message, $Objbect=""){
        Yii::trace($message);
        if($Objbect){
             Yii::trace(CVarDumper::dumpAsString($Objbect));
        }
    }
    
       /**
     * Convert date formate
     * 
     * @param type $originalDate
     * @return type
     */
    public static function convertStandardDateFormat($originalDate) {
        return $newDate = date("d/m/Y", strtotime($originalDate));
    }
    
    /**
     * convert number formate with 2 decimal
     * 
     * @param type $inputNumber
     * @return number
     */
    public static function numberFormat($inputNumber){
        return number_format($inputNumber,2, '.', '');
    }
    
    public static function gridFullPhone($data){
        $phone = "NA";
        if (!empty($data->phone)) {
            $phone = "+" .$data->country_code . "- ". $data->phone;
        }
        echo $phone;
    }
    
    public static function gridProfilePhone($data){
        $phone = "NA";
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->id));
        if ($userProfileObject) {
            if($userProfileObject->phone != 0)
            $phone = "+" .$userProfileObject->country_code . "- ". $userProfileObject->phone;
        }
        echo $phone;
    }
    
    public static function updateTransationStatus($aUniques){
        foreach($aUniques as $key => $comment){                    
            if(!empty($key)){                        
                $transactionObject = Transaction::model()->findByPk($key);                                        
                $transactionObject->admin_comment = $comment ? $comment : $transactionObject->admin_comment ;
                $transactionObject->admin_mark_status = "VERIFIED";                                   
                if (!$transactionObject->update()) {
                    Yii::trace("Some error got in Transaction mark as verified and the transaction id is :- ".$transactionObject->id);
                    throw new CHttpException(404,'Unable to find the code you requested.');
                }
            }
        }
    }  
    
    public static function defaultLandingpage() {
        $landingId = 1;
        $landingPageId = LandingPage::model()->find(array('condition' => 'status=1', 'order' => 'id Desc', 'limit' => '1'));
        if (!empty($landingPageId)) {
            $landingId = $landingPageId->id;
        }
        return $landingId;
    }
    
    public static function calculatePercentage($totalAmt,$percentageAmt) {        
        $cashbackAmt = ($totalAmt/100)*$percentageAmt; 
        return(self::numberFormat($cashbackAmt));        
    }
    
    public static function metaTags($getPageName){

        switch ($getPageName) {
            case "/package/auctions":
                echo '<title>Best Online Reverse Auction Bidding Websites, Top Bitcoin Bidding sites</title>
                      <meta content="Bid online in live auctions from all over the world. We have given fair chance for all bidders to place the lowest bid that they wanted to pay." name="description">
                      <meta content="online bidding, Best Auction Bidiing, Top Bitcoin Bidding sites, Top Bidding sites,Bitcoin Bidding sites, Best Reverse Auction Bidding Websites,Reverse Auction Bidding, Online Auction Bidding" name="keywords">'."\n";
                break;
            case "/about":
                 echo '<title>Online Auctions Sites, Bid Online, Live Online Auctions, Bitcoin Trading</title>
                       <meta content="Mavwealth is leading pay-to-bid site. Whoever has placed the unique lowest bid will be selected as the winner automatically." name="description">
                       <meta content="Online Auctions Sites, Bid Online, Live Online Auctions" name="keywords">'."\n";
                break;
            
            case "/buybids":
                echo '<title>Buy free Bitcoins, where to buy Bitcoins,Free online Auction Sites,Bid to Buy</title>
                      <meta content="Mavwealth Auctions is an Bitcoins Alternative Auction Site. Buy or sell with our online auction bitcoin today. We have given fair chance for all bidders to place the lowest bid that they wanted to pay." name="description">
                      <meta content="Buy free Bitcoins, Bitcoin Trading,  where to buy Bitcoins, Free online Aution Sites,online Auction Sites, Bid to Buy" name="keywords">'."\n";
                break;
            
            case "/howtoplay":
                echo '<title>Easy Binary Income & Options Trading, Auto Trade Binary Options,  Best Binary income Plan</title>
                <meta content="Mavwealth is number one reverse auction website where you can win free bitcoins. Do online bidding, place unique bid to earn free bitcoins. Best bidding" name="description">
                <meta content="Easy Binary Income, Binary Options Trading, Auto Trade Binary Options,  Best Binary income Plan" name="keywords">'."\n";
                break;
            
            case "/winner":
                 echo '<title>Winner - Earn Free Bitcoins Fast, Web Auction, Quick Binary Profits</title>
                <meta content="mavwealth.com made a humble beginning as a user-friendly auction site. The idea behind the inception of the site was to create an auction platform that is different and unavailable." name="description">
                <meta content="earn free bitcoins fast, Web Auction, Quick Binary Profits" name="keywords">'."\n";
                break;
            
            case "/contact":
               echo '<title>Contact us - Best Daily online Deals , Internet deals , Great Deal of The Day, Daily Deal Sites</title>
                <meta content="mavwealth is leading lowest unique auction portal. Our motive to run fair and unique auctions through lowest unique auction method." name="description">
                <meta content="Best Daily online Deals , Internet deals , Great Deal of The Day, Daily Deal Sites" name="keywords">'."\n";
                break;
            
//            case "/library":
//               echo '<title>Library - Mavwealth Reviews | Mavwealth Testimonials | Mavwealth videos |  Mavwealth Reviews | Mavwealth Product Tutorials</title>
//                <meta content="Library - Mavwealth Reviews | Mavwealth Testimonials | Mavwealth videos |  Mavwealth Reviews | Mavwealth Product Tutorials." name="description">
//                <meta content="Library - Mavwealth Reviews | Mavwealth Testimonials | Mavwealth videos |  Mavwealth Reviews | Mavwealth Product Tutorials" name="keywords">'."\n";
//                break;
   
            default:
                    echo '<title>Online Bidding Website| Reverse Auction Website</title>
                    <meta content="Mavwealth is number one reverse auction website where you can win free bitcoins. Do online bidding, place unique bid to earn free bitcoins. Best bidding website providing exciting offers for the lowest unique bid." name="description">
                    <meta content="Online Bidding Website, Reverse Auction Website, bitcoins, auction, reverse auction, free bitcoins, earn bitcoins, best bidding, online reverse auction, bid to win, auction sites, top online auctions" name="keywords">';
           
        }
  
    }
    
    public static function checkAgentStatus(){
        $currnetUserId = Yii::app()->session['userid'] ;
        $agenListObject = AgentList::model()->findByAttributes(array('user_id'=> $currnetUserId , 'status' => 1 )) ;
        return $agenListObject ;
    }
    
    /**
     * 
     * @param type $email
     */
    public static function checkEmailValid($email) { // email id is valid or not 
        if (!empty($email)) {
            if (self::domain_exists($email)) {
                $domainName = explode("@", $email);
                $validationResult['format_valid'] = 1;
                $systemWinnerSettingsObject = SystemSetting::model()->findByAttributes(array('name'=>'DisposableEmails','value'=>1));
                if($systemWinnerSettingsObject){
                    $blockedEmailIdArray = self::blockedEmailDomain();
                    if (in_array($domainName['1'], $blockedEmailIdArray)) { 
                        $validationResult['format_valid'] = 0;
                    }
                }
                if ($validationResult['format_valid'] == 1) { //preventing dummy email domains
                    $status = 0;
                } else {
                    $status = 2;
                }
            } else {
                $status = 2;
            }

            return $status;
        }
    }
    
        /**
     * 
     * @param type $email
     * @param type $record
     * @return type
     */
    public static function domain_exists($email, $record = 'MX') {
    
        $checkArr =  explode('@', $email);
        //list($user, $domain) = explode('@', $email);
        if(count($checkArr) < 2  || empty($checkArr[1]) ){
            return false;
        }
        return checkdnsrr($checkArr[1], $record);
    }
     
    
    /**
     * blocked email ids
     * @return array
     */
    public static function blockedEmailDomain() {
        return array('0815.ru', '0wnd.net', '0wnd.org', '10minutemail.co.za', '10minutemail.com',
            '123-m.com', '1fsdfdsfsdf.tk', '1pad.de', '20minutemail.com', '21cn.com', '2fdgdfgdfgdf.tk', '2prong.com',
            '30minutemail.com', '33mail.com', '3trtretgfrfe.tk', '4gfdsgfdgfd.tk', '4warding.com', '5ghgfhfghfgh.tk',
            '6hjgjhgkilkj.tk', '6paq.com', '7tags.com', '9ox.net', 'a-bc.net', 'agedmail.com', 'ama-trade.de',
            'amilegit.com', 'amiri.net', 'amiriindustries.com', 'anonmails.de', 'anonymbox.com', 'antichef.com',
            'antichef.net', 'antireg.ru', 'antispam.de', 'antispammail.de', 'armyspy.com', 'artman-conception.com',
            'azmeil.tk', 'baxomale.ht.cx', 'beefmilk.com', 'bigstring.com', 'binkmail.com', 'bio-muesli.net', 'bobmail.info',
            'bodhi.lawlita.com', 'bofthew.com', 'bootybay.de', 'boun.cr', 'bouncr.com', 'breakthru.com', 'brefmail.com',
            'bsnow.net', 'bspamfree.org', 'bugmenot.com', 'bund.us', 'burstmail.info', 'buymoreplays.com', 'byom.de',
            'c2.hu', 'card.zp.ua', 'casualdx.com', 'cek.pm', 'centermail.com', 'centermail.net', 'chammy.info', 'childsavetrust.org',
            'chogmail.com', 'choicemail1.com', 'clixser.com', 'cmail.net', 'cmail.org', 'coldemail.info', 'cool.fr.nf',
            'courriel.fr.nf', 'courrieltemporaire.com', 'crapmail.org', 'cust.in', 'cuvox.de', 'd3p.dk', 'dacoolest.com',
            'dandikmail.com', 'dayrep.com', 'dcemail.com', 'deadaddress.com', 'deadspam.com', 'delikkt.de', 'despam.it',
            'despammed.com', 'devnullmail.com', 'dfgh.net', 'digitalsanctuary.com', 'dingbone.com', 'disposableaddress.com',
            'disposableemailaddresses.com', 'disposableinbox.com', 'dispose.it', 'dispostable.com', 'dodgeit.com',
            'dodgit.com', 'donemail.ru', 'dontreg.com', 'dontsendmespam.de', 'drdrb.net', 'dump-email.info', 'dumpandjunk.com',
            'dumpyemail.com', 'e-mail.com', 'e-mail.org', 'e4ward.com', 'easytrashmail.com', 'einmalmail.de', 'einrot.com',
            'eintagsmail.de', 'emailgo.de', 'emailias.com', 'emaillime.com', 'emailsensei.com', 'emailtemporanea.com',
            'emailtemporanea.net', 'emailtemporar.ro', 'emailtemporario.com.br', 'emailthe.net', 'emailtmp.com', 'emailwarden.com',
            'emailx.at.hm', 'emailxfer.com', 'emeil.in', 'emeil.ir', 'emz.net', 'ero-tube.org', 'evopo.com', 'explodemail.com',
            'express.net.ua', 'eyepaste.com', 'fakeinbox.com', 'fakeinformation.com', 'fansworldwide.de', 'fantasymail.de',
            'fightallspam.com', 'filzmail.com', 'fivemail.de', 'fleckens.hu', 'frapmail.com', 'friendlymail.co.uk', 'fuckingduh.com',
            'fudgerub.com', 'fyii.de', 'garliclife.com', 'gehensiemirnichtaufdensack.de', 'get2mail.fr', 'getairmail.com',
            'getmails.eu', 'getonemail.com', 'giantmail.de', 'girlsundertheinfluence.com', 'gishpuppy.com', 'gmial.com',
            'goemailgo.com', 'gotmail.net', 'gotmail.org', 'gotti.otherinbox.com', 'great-host.in', 'greensloth.com', 'grr.la',
            'gsrv.co.uk', 'guerillamail.biz', 'guerillamail.com', 'guerrillamail.biz', 'guerrillamail.com', 'guerrillamail.de',
            'guerrillamail.info', 'guerrillamail.net', 'guerrillamail.org', 'guerrillamailblock.com', 'gustr.com', 'harakirimail.com',
            'hat-geld.de', 'hatespam.org', 'herp.in', 'hidemail.de', 'hidzz.com', 'hmamail.com', 'hopemail.biz', 'ieh-mail.de',
            'ikbenspamvrij.nl', 'imails.info', 'inbax.tk', 'inbox.si', 'inboxalias.com', 'inboxclean.com', 'inboxclean.org',
            'infocom.zp.ua', 'instant-mail.de', 'ip6.li', 'irish2me.com', 'iwi.net', 'jetable.com', 'jetable.fr.nf', 'jetable.net',
            'jetable.org', 'jnxjn.com', 'jourrapide.com', 'jsrsolutions.com', 'kasmail.com', 'kaspop.com', 'killmail.com',
            'killmail.net', 'klassmaster.com', 'klzlk.com', 'koszmail.pl', 'kurzepost.de', 'lawlita.com', 'letthemeatspam.com',
            'lhsdv.com', 'lifebyfood.com', 'link2mail.net', 'litedrop.com', 'lol.ovpn.to', 'lolfreak.net', 'lookugly.com',
            'lortemail.dk', 'lr78.com', 'lroid.com', 'lukop.dk', 'm21.cc', 'mail-filter.com', 'mail-temporaire.fr', 'mail.by',
            'mail.mezimages.net', 'mail.zp.ua', 'mail1a.de', 'mail21.cc', 'mail2rss.org', 'mail333.com', 'mailbidon.com',
            'mailbiz.biz', 'mailblocks.com', 'mailbucket.org', 'mailcat.biz', 'mailcatch.com', 'mailde.de', 'mailde.info',
            'maildrop.cc', 'maileimer.de', 'mailexpire.com', 'mailfa.tk', 'mailforspam.com', 'mailfreeonline.com', 'mailguard.me',
            'mailin8r.com', 'mailinator.com', 'mailinator.net', 'mailinator.org', 'mailinator2.com',
            'mailincubator.com', 'mailismagic.com', 'mailme.lv', 'mailme24.com', 'mailmetrash.com', 'mailmoat.com', 'mailms.com',
            'mailnesia.com', 'mailnull.com', 'mailorg.org', 'mailpick.biz', 'mailrock.biz', 'mailscrap.com', 'mailshell.com',
            'mailsiphon.com', 'mailtemp.info', 'mailtome.de', 'mailtothis.com', 'mailtrash.net', 'mailtv.net', 'mailtv.tv',
            'mailzilla.com', 'makemetheking.com', 'manybrain.com', 'mbx.cc', 'mega.zik.dj', 'meinspamschutz.de', 'meltmail.com',
            'messagebeamer.de', 'mezimages.net', 'ministry-of-silly-walks.de', 'mintemail.com', 'misterpinball.de', 'moncourrier.fr.nf',
            'monemail.fr.nf', 'monmail.fr.nf', 'monumentmail.com', 'mt2009.com', 'mt2014.com', 'mycard.net.ua', 'mycleaninbox.net',
            'mymail-in.net', 'mypacks.net', 'mypartyclip.de', 'myphantomemail.com', 'mysamp.de', 'mytempemail.com', 'mytempmail.com',
            'mytrashmail.com', 'nabuma.com', 'neomailbox.com', 'nepwk.com', 'nervmich.net', 'nervtmich.net', 'netmails.com', 'netmails.net',
            'neverbox.com', 'nice-4u.com', 'nincsmail.hu', 'nnh.com', 'no-spam.ws', 'noblepioneer.com', 'nomail.pw', 'nomail.xl.cx',
            'nomail2me.com', 'nomorespamemails.com', 'nospam.ze.tc', 'nospam4.us', 'nospamfor.us', 'nospammail.net', 'notmailinator.com',
            'nowhere.org', 'nowmymail.com', 'nurfuerspam.de', 'nus.edu.sg', 'objectmail.com', 'obobbo.com', 'odnorazovoe.ru', 'oneoffemail.com',
            'onewaymail.com', 'onlatedotcom.info', 'online.ms', 'opayq.com', 'ordinaryamerican.net', 'otherinbox.com', 'ovpn.to', 'owlpic.com',
            'pancakemail.com', 'pcusers.otherinbox.com', 'pjjkp.com', 'plexolan.de', 'poczta.onet.pl', 'politikerclub.de', 'poofy.org', 'pookmail.com',
            'privacy.net', 'privatdemail.net', 'proxymail.eu', 'prtnx.com', 'putthisinyourspamdatabase.com', 'putthisinyourspamdatabase.com',
            'qq.com', 'quickinbox.com', 'rcpt.at', 'reallymymail.com', 'realtyalerts.ca', 'recode.me', 'recursor.net', 'reliable-mail.com',
            'rhyta.com', 'rmqkr.net', 'royal.net', 'rtrtr.com', 's0ny.net', 'safe-mail.net', 'safersignup.de', 'safetymail.info', 'safetypost.de',
            'saynotospams.com', 'schafmail.de', 'schrott-email.de', 'secretemail.de', 'secure-mail.biz', 'senseless-entertainment.com',
            'services391.com', 'sharklasers.com', 'shieldemail.com', 'shiftmail.com', 'shitmail.me', 'shitware.nl', 'shmeriously.com',
            'shortmail.net', 'sibmail.com', 'sinnlos-mail.de', 'slapsfromlastnight.com', 'slaskpost.se', 'smashmail.de', 'smellfear.com',
            'snakemail.com', 'sneakemail.com', 'sneakmail.de', 'snkmail.com', 'sofimail.com', 'solvemail.info', 'sogetthis.com', 'soodonims.com',
            'spam4.me', 'spamail.de', 'spamarrest.com', 'spambob.net', 'spambog.ru', 'spambox.us', 'spamcannon.com', 'spamcannon.net', 'spamcon.org',
            'spamcorptastic.com', 'spamcowboy.com', 'spamcowboy.net', 'spamcowboy.org', 'spamday.com', 'spamex.com', 'spamfree.eu', 'spamfree24.com',
            'spamfree24.de', 'spamfree24.org', 'spamgoes.in', 'spamgourmet.com', 'spamgourmet.net', 'spamgourmet.org', 'spamherelots.com', 'spamherelots.com',
            'spamhereplease.com', 'spamhereplease.com', 'spamhole.com', 'spamify.com', 'spaml.de', 'spammotel.com', 'spamobox.com', 'spamslicer.com', 'spamspot.com',
            'spamthis.co.uk', 'spamtroll.net', 'speed.1s.fr', 'spoofmail.de', 'stuffmail.de', 'super-auswahl.de', 'supergreatmail.com', 'supermailer.jp',
            'superrito.com', 'superstachel.de', 'suremail.info', 'talkinator.com', 'teewars.org', 'teleworm.com', 'teleworm.us', 'temp-mail.org', 'temp-mail.ru',
            'tempe-mail.com', 'tempemail.co.za', 'tempemail.com', 'tempemail.net', 'tempemail.net', 'tempinbox.co.uk', 'tempinbox.com', 'tempmail.eu',
            'tempmaildemo.com', 'tempmailer.com', 'tempmailer.de', 'tempomail.fr', 'temporaryemail.net', 'temporaryforwarding.com', 'temporaryinbox.com',
            'temporarymailaddress.com', 'tempthe.net', 'thankyou2010.com', 'thc.st', 'thelimestones.com', 'thisisnotmyrealemail.com', 'thismail.net',
            'throwawayemailaddress.com', 'tilien.com', 'tittbit.in', 'tizi.com', 'tmailinator.com', 'toomail.biz', 'topranklist.de', 'tradermail.info',
            'trash-mail.at', 'trash-mail.com', 'trash-mail.de', 'trash2009.com', 'trashdevil.com', 'trashemail.de', 'trashmail.at', 'trashmail.com',
            'trashmail.de', 'trashmail.me', 'trashmail.net', 'trashmail.org', 'trashymail.com', 'trialmail.de', 'trillianpro.com', 'twinmail.de', 'tyldd.com', 'uggsrock.com', 'umail.net', 'uroid.com', 'us.af', 'venompen.com');
    }
    
    public static function create_slug($string){
        $slug = strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $string));
        return $slug;
    }

    public static function getWalletInfo($id){
        $walletObject = WalletType::model()->findByPk($id);
        $walletStatus = "" ;
        if(!empty($walletObject)){
            $walletStatus = $walletObject->transfer_status;
        }
        return $walletStatus ;
    }
    
     /**
     * Blocked country ip and code
     * 
     * @return boolean
     */
    public static function isCountryValid($currentURL) {
        $IP = $_SERVER['REMOTE_ADDR'];
        $countryArr = self::ipInfo($IP, "Location");
        $country = $countryArr['country_code'];
//        $country = 'INA';
        $arr = array('/user/registration', '/user/commonregistration', 'user/commonregistration');

        if ($IP == '182.75.61.130' || $IP == '106.51.126.86' || $IP == '106.51.13.66' || $IP == '106.51.13.96' || $IP == '106.51.13.97' || $IP == '106.51.13.98' || $IP == '106.51.13.99' || $IP == '106.51.13.100' || $IP == '106.51.13.101' || $IP == '106.51.13.102' || $IP == '106.51.13.103' || $IP == '106.51.227.14') {
            return true;
        } else { //return true;
            if ($_SERVER['SERVER_ADDR'] == Yii::app()->params['dev_ip_address']) { //verify country only in 
                return true;
            } else {
                $not_allowed_countries = array("IR", "ISA");
                if (in_array(trim($country), $not_allowed_countries)) {
                    //Access for the all IP
                    if (($_SERVER["REQUEST_URI"] == '/user/getfullname') || (in_array($currentURL, $arr))) {
                        return true;
                    }else{
                      return $currentURL;  
                    }
                }else{
                    return true;
                }
            }
            return true;
        }
    }

    /**
     * function to check country code
     * @param type $ip
     * @param type $purpose
     * @param type $deep_detect
     * @return type
     */
    public static function ipInfo($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city" => @$ipdat->geoplugin_city,
                            "state" => @$ipdat->geoplugin_regionName,
                            "country" => @$ipdat->geoplugin_countryName,
                            "country_code" => @$ipdat->geoplugin_countryCode,
                            "continent" => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }
    
    public static function auctionShareDesc($auctionObject) {
        $desc = '';
        if(is_object($auctionObject)) {
            $desc = 'Come Join the Auction for '. str_replace('-', ' $', $auctionObject->product->name) .'. Auction Ends '. date("d-M-Y h:m", strtotime($auctionObject->close_date)) .' UDT. ';
            $desc .= 'Mavwealth Provides you many more E-Currencies to bid on.';
        }
        return $desc;
    }
    
    public static function isRestricted($userId){
        
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $userId));
        $resultArray = array();
        $resultArray['IsRestricted'] =  0;
        $resultArray['MobileVerified'] = 0;
        $resultArray['DocumentVerified'] = 0;
        $resultArray['RegCheckDate'] = "";
        $resultArray['userId'] = "";
        $resultArray['LeftRightCheck'] =  0;
        $resultArray['leftExists'] =  0;
        $resultArray['rightExists'] =  0;
        $resultArray['leftPaid'] =  0;
        $resultArray['rightPaid'] =  0;
        
        if(!empty($userProfileObject->phone_number_status) && ($userProfileObject->phone_number_status == 1)) {
            $resultArray['MobileVerified'] = 1;
        }
        if(!empty($userProfileObject->document_status) && ($userProfileObject->document_status == 1)) {
           $resultArray['DocumentVerified'] = 1;
        }
        
        static $restrictedUsers =[];
        if(empty($restrictedUsers)) {
            $genealogyListObject = Genealogy::model()->findAll(array('condition' => 'is_restricted = 1'));
            
            foreach($genealogyListObject as $genealogyObj) {
                if(!isset($restrictedUsers[$genealogyObj->reg_check_date])) {    
                    $restrictedUsers[$genealogyObj->reg_check_date] = array($genealogyObj->user_id);
                } else {
                    $restrictedUsers[$genealogyObj->reg_check_date] = array_merge(($restrictedUsers[$genealogyObj->reg_check_date]), array($genealogyObj->user_id));
                }
                if(!empty($genealogyObj->left_users)) {
                    $restrictedUsers[$genealogyObj->reg_check_date] = array_merge($restrictedUsers[$genealogyObj->reg_check_date],explode(',', $genealogyObj->left_users));
                }
                if(!empty($genealogyObj->right_users)) {
                    $restrictedUsers[$genealogyObj->reg_check_date] = array_merge($restrictedUsers[$genealogyObj->reg_check_date],explode(',', $genealogyObj->right_users));
                }
            }
        }
        
        foreach($restrictedUsers as $key => $value){

            if(in_array($userId, $value)) {
                $resultArray['IsRestricted'] =  1;
                $resultArray['RegCheckDate'] =  $key;
                $resultArray['userId'] =  $userId;

                //if user is registered before check date then 
                //he needs to have a new left and right user
                if(strtotime($userProfileObject->user->created_at) < strtotime($key)) {
                    $resultArray['LeftRightCheck'] = 1;

                    $currentUserMembershipType = User::model()->findByPk($userId);
                    //print_r($currentUserMembershipType); die;

                    $lruser = Yii::app()->db->createCommand()
                        ->select('position,max(membership_type) as maxMembershipType')
                        ->from('user u')
                        ->where('sponsor_id=:sponsorId AND created_at > :regCheckDate ', array(':sponsorId'=>$userProfileObject->user->name, ':regCheckDate'=>$key))
                        ->group('position')    
                        ->queryAll();

                    foreach($lruser as $chkUser){
                        if($chkUser['position'] == "left") {
                            $resultArray['leftExists'] = 1;
                            if($chkUser['maxMembershipType'] >= $currentUserMembershipType->membership_type) {
                                $resultArray['leftPaid'] = 1;
                            }
                        }
                        if($chkUser['position'] == "right") {
                            $resultArray['rightExists'] = 1;
                            if($chkUser['maxMembershipType'] >= $currentUserMembershipType->membership_type) {
                                $resultArray['rightPaid'] = 1;
                            }
                        }
                    }
                }

            }
        } 
        return $resultArray; 
    }
    
    public static function strposa($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }
    
    /**
    * Checking email is Yahoo, Gmail and hotmail emails
    */
    public static function isEmails($email){
        $flag = 0 ;
        $subDomain = explode("@", $email);        
        $subDomainName = (explode(".",$subDomain[1]));
        $registerEmails = array('yahoo','hotmail');
        if (in_array($subDomainName[0], $registerEmails)) {
            $flag = 1 ;
        } 
        return $flag ;        
    }
    
    /**
    * Removing special characters and spaces from file name
    */
    public static function create_upload_filename($filename){
        $newFileName = preg_replace("/[^a-zA-Z0-9.]/", "_", $filename);
        return $newFileName;
    }
    
    public static function bannerClassName($size){
        $clr = "";
        if ($size >= 450) {
            $clr = " image-textarea ";
        }
        if ($size <= 125) {
            $smallimg = " left90 ";
        }
        if ($size <= 468) {
            $custom = " custom-text ";
        }
        return $clr;
    }
    
    /**
     * convert number formate with 2 decimal
     * 
     * @param type $inputNumber
     * @return number
     */
    public static function convertNumberFormate($inputNumber){
        return number_format($inputNumber,2, '.', '');
    }
    
    public static function getPercentageAmt($reqfund, $userWalletAmt, $percentage){
        $maxuse = $reqfund * $percentage / 100 ;

        if($reqfund > $maxuse){
          return floor($maxuse) ;  
        }else if($reqfund > $userWalletAmt ){
             return floor($userWalletAmt) ; 
        }else{
          return floor($reqfund) ;  
        } 
       return 2 ; 
    }
    
}
