<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        ADD Employees
        <small><?php echo Yii::app()->session['orgName']; ?></small>
        <small><strong><i>
            <?php if(Yii::app()->user->hasFlash('success')):?>
                <div class="info text-green">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
                </i></strong>
        </small>
    </h1>
    
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Employees</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    
    <form name="add_emp_frm" id="add_emp_frm" method="post" action="">
        <input type="text" value="<?php echo (isset($userObject)&& !empty($userObject))?$userObject->user()->id:""; ?>" name="user_id" class="form-control">
    <div class="row">
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Basic Information</h3>
            </div>
            <div class="box-body">

              <!-- phone mask -->
              <div class="form-group">
                <label>Full Name:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                    <input type="text" value="<?php echo (isset($userObject)&& !empty($userObject))?$userObject->user()->full_name:""; ?>" name="full_name" placeholder="Enter Full Name" id="full_name" class="form-control" required>
                </div>
                <!-- /.input group -->
              </div>
              <!-- phone mask -->
              <div class="form-group">
                <label>Email:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                  </div>
                    <input type="email" value="<?php echo (isset($userObject)&& !empty($userObject))?$userObject->user()->email:""; ?>" name="email" placeholder="Enter Email" id="mail" class="form-control" required>
                </div>
                <!-- /.input group -->
              </div>
              <!-- phone mask -->
              <div class="form-group">
                <label>Phone</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                    <input type="number" value="<?php echo (isset($userObject)&& !empty($userObject))?$userObject->user()->phone_no:""; ?>" class="form-control" placeholder="Phone Number" name="phone" required>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                <label>Alternative Number</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                    <input type="number" class="form-control" value="<?php echo (isset($userObject)&& !empty($userObject))?$userObject->alternative_no:""; ?>" placeholder="Alternative Number" name="alternative" required>
                </div>
                <!-- /.input group -->
              </div>
              <!-- phone mask -->
              <div class="form-group">
                <label>Address</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-sticky-note"></i>
                  </div>
                    <textarea type="text" class="form-control" placeholder="Enter full Address " name="address"><?php echo (isset($userObject)&& !empty($userObject))?$userObject->address:""; ?></textarea>

                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                <label>Date Of Birth:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input type="text" class="form-control pull-right datepicker" value="<?php echo (isset($userObject)&& !empty($userObject))?$userObject->dob:""; ?>" placeholder="Date Of Birth" id="datepicker" name="date_of_birth" >
                </div>
                <!-- /.input group -->
              </div>
              <!-- phone mask -->
              <div class="form-group">
                <label>Gender</label>

                <div class="checkbox">
                  <label for="drop-remove">
                      <input type="radio" name="gender" value="MALE" <?php if(($userObject)&& !empty($userObject)){ if($userObject->user()->gender == 'MALE'){ echo "checked=true";} else { echo "checked=false";}} else { echo "checked=true";} ?> > Male
                    <input type="radio"  name="gender" value="FEMALE" <?php if(($userObject)&& !empty($userObject)){ if($userObject->user()->gender == 'FEMALE'){ echo "checked=true";} else { echo "checked=false";}} ?> > Female
                  </label>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


        </div>
        <!-- /.col (left) -->
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Official Info</h3>
            </div>
            <div class="box-body">
              <!-- Date -->
              <div class="form-group">
                <label>Designation:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-graduation-cap"></i>
                  </div>
                    <select class="form-control select2" name="designation_id" style="width: 100%;" required>
                  <option selected="selected">Select Designation</option>
                    <?php foreach($designationListObject as $designationObject) { ?>
                    <option value="<?php echo $designationObject->id; ?>"
                            <?php if(($userObject)&& !empty($userObject)){ if($userObject->designation_id == $designationObject->id){ echo "selected";}} ?>
                            ><?php echo $designationObject->name; ?></option>
                    <?php } ?>
                </select>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                <label>Department:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-home"></i>
                  </div>
                    <select class="form-control select2" name="department_id" style="width: 100%;" required>
                    <option selected="selected">Select Department</option>
                    <?php foreach($departMentListObject as $departmentObject) { ?>
                    <option value="<?php echo $departmentObject->id; ?>" 
                            <?php if(($userObject)&& !empty($userObject)){ if($userObject->department_id == $departmentObject->id){ echo "selected";}} ?>
                            ><?php echo $departmentObject->name; ?></option>
                    
                    <?php } ?>
                  </select>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
                <div class="form-group">
                <label>Joined On:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input type="text" class="form-control pull-right datepicker"value="<?php echo (isset($userObject)&& !empty($userObject))?$userObject->joined_on:""; ?>"  placeholder="Joined On" id="datepicker" name="joined" required>
                </div>
                <!-- /.input group -->
              </div>
                <div class="form-group">
                <label>Available Leave:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-sign-out"></i>
                  </div>
                    <input type="text" class="form-control pull-right"value="<?php echo (isset($userObject)&& !empty($userObject))?$userObject->user()->available_leave:""; ?>"  placeholder="Enter Available Leave" id="datepicker" name="available_leave" required>
                </div>
                <!-- /.input group -->
              </div>
              
              <div class="form-group">
                <label>Branch:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-sitemap"></i>
                  </div>
                    <select class="form-control select2" name="branch_id" style="width: 100%;" required>
                    <option selected="selected">Select Branch</option>
                    <?php foreach($branchListObject as $branchObject) { ?>
                    <option value="<?php echo $branchObject->id; ?>"
                            <?php if(($userObject)&& !empty($userObject)){ if($userObject->branch_id == $branchObject->id){ echo "selected";}} ?>
                            ><?php echo $branchObject->name; ?></option>
                    <?php } ?>
                </select>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                <label>Blood Group:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-sitemap"></i>
                  </div>
                    <select class="form-control select2" name="blood_group_id" name="blood_group"style="width: 100%;" required>
                  <option selected="selected">Select Blood Group</option>
                    <?php foreach($bloodGroupListObject as $bloodGroupObject) { ?>
                    <option value="<?php echo $bloodGroupObject->id; ?>"
                            <?php if(($userObject)&& !empty($userObject)){ if($userObject->blood_group_id == $bloodGroupObject->id){ echo "selected";}} ?>
                            ><?php echo $bloodGroupObject->name; ?></option>
                    <?php } ?>
                </select>
                </div>
                <!-- /.input group -->
              </div>
              
              <!-- Date range -->
              <div class="form-group">
                <label>Avatar:</label>

                <div class="input-group">
                    <input type="file" name="avatar" id="exampleInputFile">
                  <p class="help-block">Only JPG, PNG file supported</p>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group  pull-right" style="max-width: 100px;">
                  <input type="submit" class="btn btn-block btn-info" value="Submit">
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- /.form group -->

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col (right) -->
      </div>
    </form>
    <!-- /.row -->
</section>
