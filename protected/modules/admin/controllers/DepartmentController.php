<?php

class DepartmentController extends Controller {

    public $layout = 'main';
    
    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'add', 'changestatus'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

     /*
     * Function to fetching list
     */
    
    public function actionIndex() {
        $order = "";
        $pageSize = Yii::app()->params['defaultPageSize'];

        if (empty($_GET['Department_sort'])) {
            $order = 'id DESC';
        }
        $dataProvider = new CActiveDataProvider('Department', array(
            'criteria' => array(
                'order' => $order,
            ), 'pagination' => array('pageSize' => $pageSize)));

        $this->render('index', array('dataProvider' => $dataProvider));
     
    }

     /*
     * Function to add/edit
     */
    public function actionAdd() {
       
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $departmentId = BaseClass::mgDecrypt($_GET['id']);
            $departmentObject = Department::model()->findByPk($departmentId);
        } else {
            $departmentObject = new Department; 
        }
     
        if (!empty($_POST)) {
            if (!empty($_POST['name'])) {
                $existDepartmentObject = Department::model()->findByAttributes(array('name' => $_POST['name']));

                if (count($existDepartmentObject) > 0 && empty($_GET['id'])) {
                    Yii::app()->user->setFlash('error', 'Department already exists');
                    $this->redirect('/admin/department/add');  
                } else {
                  
                    try {
                        $newDepartmentObject = Department::model()->create($_POST, $_FILES, $departmentObject);
                        if(isset($newDepartmentObject) && !empty($newDepartmentObject)){
                          Yii::app()->user->setFlash('success', 'Department succesfully updated');
                          $this->redirect('/admin/department/add');
                        } else {
                          Yii::app()->user->setFlash('error', 'Department submition failed');
                          $this->redirect('/admin/department/add');  
                        }
                       
                    } catch (Exception $ex) {

                        $exception = $ex->getMessage();
                        exit;
                    }
                }
            } else {
                Yii::app()->user->setFlash('error', 'Please fill all required fields.');
                $this->redirect('/admin/department/add'); 
            }
        }

        $this->render('add', array('departmentObject' => $departmentObject));
    }

    /*
     * Function to change status
     */

    public function actionChangeStatus() {
        if (!empty($_GET['id'])) {
            $departmentObject = Department::model()->findByPK($_GET['id']);
            if ($departmentObject->status == 1) {
                $departmentObject->status = 0;
            } else {
                $departmentObject->status = 1;
            }
            $departmentObject->save(false);
            Yii::app()->user->setFlash('success', 'Status Changed Successfully');
            $this->redirect(array('/admin/department'));
        }
    }

}
