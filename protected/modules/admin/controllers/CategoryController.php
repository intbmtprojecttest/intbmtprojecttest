<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CategoryController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'categoryadd', 'categoryedit', 'categorylist', 'deletecategory',
                    'changestatus'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    /*
     * function to add builder category
     */

    public function actionCategoryAdd() {
        $error = "";
        $success = "";
        $categoryObject = new Category;
        if ($_POST) {
            if ($_POST['Category']['name'] != '') {
                $categoryObject->name = $_POST['Category']['name'];
                $categoryObject->created_at = new CDbExpression('NOW()');
                $categoryObject->updated_at = new CDbExpression('NOW()');
                if ($categoryObject->save(false)) {
                    $this->redirect(array('categorylist', 'msg' => 1));
                }
            } else {
                $error .= "Please fill all required(*) marked fields";
            }
        }
        $this->render('/category/category_add', array('error' => $error, 'success' => $success));
    }

    /*
     * Function to edit bilder category
     */

    public function actionCategoryEdit() {
        $error = "";
        $success = "";
        if ($_REQUEST['id']) {
            $categoryObject = Category::model()->findByPk($_REQUEST['id']);
            if ($_POST) {
                if ($_POST['Category']['name'] != '') {
                    $categoryObject->name = $_POST['Category']['name'];
                    $categoryObject->created_at = new CDbExpression('NOW()');
                    $categoryObject->updated_at = new CDbExpression('NOW()');
                    if ($categoryObject->save(false)) {
                        $this->redirect(array('categorylist', 'msg' => 2));
                    }
                } else {
                    $error .= "Please fill all required(*) marked fields";
                }
            }
        }
        $this->render('/category/category_edit', array('error' => $error, 'success' => $success, 'categoryObject' => $categoryObject));
    }

    /*
     * Function to list bilder category
     */

    public function actionCategoryList() {
        $dataProvider = new CActiveDataProvider('Category', array(
            'pagination' => array('pageSize' => 10),
        ));
        $this->render('/category/category_list', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /*
     * Function to fetch Package list
     */

    public function actionChangeStatus() {

        if ($_REQUEST['id']) {
            $categoryObject = Category::model()->findByPk($_REQUEST['id']);
            
            if ($categoryObject->status == 1) {
                $categoryObject->status = 0;
            } else {
                $categoryObject->status = 1;
            }
            $categoryObject->save(false);
            //$this->redirect('/admin/package/list',array('msg'=>'2'));
            $this->redirect(array('/admin/category/categorylist', 'msg' => 4));
        }
    }
    
    /*
     * Function to Delete Bilder Category list
     */

    public function actionDeleteCategory() {
        if ($_REQUEST['id']) {
            $categoryObject = BuildCategory::model()->findByPK($_REQUEST['id']);
            $categoryObject->delete();
            $this->redirect(array('/admin/BuildTemp/categorylist', 'msg' => 3));
        }
    }

}
