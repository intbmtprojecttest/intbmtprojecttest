<?php

class AuctionController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('add', 'edit', 'list', 'autocompletebypid', 'declarewinner' , 'changestatus', 'changepublish',
                    'deleteauction', 'closingauctiondata', 'winnerpayment', 'winnerpaymentdata', 'bulkwinnerpaid'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Adding auction.
     */
    public function actionAdd() {
        $error = "";
        $success = "";
        $msg = 1;
        $auctionObject = new Auction;
        
        $criteria = new CDbCriteria;
        $criteria->condition = " status  = 1 AND purchase_type != 1 ";
        $productObject = Product::model()->findAll($criteria);
//        $productObject = Product::model()->findByAttributes(array('status' => 1 , 'purchase_type' => 2));
        if (!empty($_POST)) {
            $start_date = $_POST['sdate'] ;
            $close_date = $_POST['cdate'] ;

            if ($_POST['product_id'] == '' && $_POST['sdate'] == '' && $_POST['cdate'] == '') {
                $error .= "Please fill required(*) marked fields.";
            } else {

                $auctionObject->start_date = $start_date;
                $auctionObject->close_date = $close_date;
                $auctionObject->product_id = $_POST['product_id'];
                $auctionObject->entry_fees = $_POST['entry_fees'];
                $auctionObject->lowest_bid_price = $_POST['lowest_bid_price'];
                $auctionObject->is_premium = isset($_POST['is_premium']) ? $_POST['is_premium'] : 0;
                $auctionObject->is_publish = isset($_POST['is_publish']) ? $_POST['is_publish'] : 0;
                $auctionObject->is_autoserious = isset($_POST['is_autoserious']) ? $_POST['is_autoserious'] : 0;
                $auctionObject->is_autobid = isset($_POST['is_autobid']) ? $_POST['is_autobid'] : 0;
                $auctionObject->created_at = new CDbExpression('NOW()');
                if ($auctionObject->save(false)) {
                    //$this->update_auction($auctionObject); // Update auction_bid with default_bid
                    Yii::app()->user->setFlash('success', "Auction Successfully Added.");
                    $this->redirect(array('list'));
                } else {
                    $error .= "Error! Auction not created.";
                }
            }
        }

        $this->render('add', array('success' => $success, 'error' => $error, 'auctionObject' => $auctionObject, 'productObject' => $productObject));
    }

    /**
     * Editing Auction.
     */
    public function actionEdit() {
        $error = "";
        $success = "";
        $msg = 1;
        $start_date = "";
        $close_date = "";
        $is_premium = 0;
        $is_publish = 0;
        $is_autoserious = 0;
        $auctionObject = Auction::model()->findByPK(array('id' => BaseClass::mgDecrypt($_GET['id'])));
        $productObject = Product::model()->findAllByAttributes(array('status' => 1));
        if (!empty($_POST)) {
            if (!empty($_POST['is_premium'])) {
                $is_premium = $_POST['is_premium'];
            }
            if (!empty($_POST['is_publish'])) {
                $is_publish = $_POST['is_publish'];
            }
            if (!empty($_POST['is_autoserious'])) {
                $is_autoserious = $_POST['is_autoserious'];
            }
            $start_date = $_POST['sdate'] ;
            $close_date = $_POST['cdate'] ;
            //echo '<pre>';print_r($start_date);exit;
            if ( $_POST['sdate'] == '' && $_POST['cdate'] == '') {
                $error .= "Please fill required(*) marked fields.";
            } else {

                $auctionObject->start_date = $start_date;
                $auctionObject->close_date = $close_date;
                $auctionObject->is_premium = $is_premium;
                $auctionObject->entry_fees = $_POST['entry_fees'];;
                $auctionObject->lowest_bid_price = $_POST['lowest_bid_price'];;
                $auctionObject->is_publish = $is_publish;
                $auctionObject->is_autoserious = $is_autoserious;
                $auctionObject->is_autobid = isset($_POST['is_autobid']) ? $_POST['is_autobid'] : 0;
                $auctionObject->update(false);
                Yii::app()->user->setFlash('success', "Auction Successfully Updated.");
                $this->redirect(array('list'));
            }
        }

        $this->render('edit', array('success' => $success, 'error' => $error, 'auctionObject' => $auctionObject, 'productObject' => $productObject));
    }

    /**
     * Get auction list.
     */
    public function actionList() {
        $todayDate = "";
        $fromDate = "";
        $name = "";
        $status = "";
        $order = "";
        if(empty($_GET['Auction_sort'])) {
            $order = 't.id DESC';
        }
        
        $condition = "p.status = 1 "; //Default condition
        
        $pageSize = Yii::app()->params['defaultPageSize'];
        if(isset($_GET['perpage']) && !empty($_GET['perpage'])){
            $pageSize = $_GET['perpage'];
        }

        if (isset($_GET['name']) || isset($_GET['res_filter']) || isset($_GET['to']) || isset($_GET['from'])) {
           
            $name = isset($_GET['name'])?$_GET['name']:"";
            $status = $_GET['res_filter'];

            if (!empty($_GET['to']) && !empty($_GET['from'])) {
                $todayDate = date("Y-m-d", strtotime($_GET['to']));
                $fromDate = date("Y-m-d", strtotime($_GET['from']));
                $condition .= ' AND t.start_date >= "' . $todayDate . '" AND t.close_date <= "' . $fromDate . '"';
            }
        
            //Checking Product
            if (!empty($name)) { 
                $condition .= ' AND p.name like "%' . $name . '%"';
            }
            
            //Checking Status
            if ($status != "") { 
                $condition .= 'AND t.status = ' . $status;
            }
        }
        
        $criteria=new CDbCriteria;
        $criteria->join = ' JOIN product p ON p.id = t.product_id'; 
        $criteria->addCondition($condition);
        $criteria->order = $order;

        $dataProvider = new CActiveDataProvider('Auction', array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => $pageSize),
        ));

        $this->render('list', array('dataProvider' => $dataProvider, 'name' => $name, 'to' => $todayDate, 'from' => $fromDate, 'status' => $status));
    }

    /**
     * Get bid value.
     */
    protected function getBidValue($data, $row) {
        $auctionId = $data->id;
        $auctionObject = AuctionBids::model()->findByAttributes(array('auction_id' => $auctionId, 'status' => 1));
        if (!empty($auctionObject)) {
            return $auctionObject->bid_value;
        }
    }

    /**
     * Get aution status.
     */
    protected function getBidStatus($auctionId) {
        $auctionObject = Auction::model()->findByAttributes(array('id' => $auctionId, 'status' => 1));

        if (!empty($auctionObject)) {
            return "Open";
        } else {
            return "Closed";
        }
    }

    /**
     * Get aution open list.
     */
    public function actionDeclareWinner() {
        $this->render('declarewinner');
    }
 
      public function actionClosingAuctionData() {

        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        $closeDate = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +30 minutes"));
 
        $dataQuery = Yii::app()->db->createCommand()
            ->select('a.id as auctionId, p.name as productName, a.close_date as endDate, ifnull(u.name,"Not_Declared") as winnerName,
            (case when u.role_id = 1 then "Regular" when u.role_id = 3 then "Preferential" ELSE "--" END) as winnerUserType,
            (case when a.status = 1 then "Active" ELSE "InActive" END) as auctionStatus,
            (select (count(ab.auction_id) * a.entry_fees) from auction_bids ab  join user U1 on ab.user_id = U1.id and U1.role_id = 1  where ab.auction_id  = a.id ) as amountCollected')
            ->from('auction a')
            ->join('product p', 'p.id = a.product_id')
            ->leftjoin('user u', 'u.id = a.winner_user_id');
        $dataQuery->where('p.status = 1 and a.status=1 and a.winner_user_id IS NULL AND a.close_date <= "'.$closeDate.'"');
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('p.name LIKE :productName OR u.name LIKE :userName', array(':productName' => '%'.$searchData.'%', ':userName' => '%'.$searchData.'%'));
        }
        
        if(!empty($_POST['columns']['2']['search']['value'])){
            $todayDate = $_POST['columns']['2']['search']['value'];
            $fromDate = $_POST['columns']['2']['search']['regex'];
            $dataQuery->andWhere('a.close_date >= :todate', array(':todate' => $todayDate));
            $dataQuery->andWhere('a.close_date <= :fromdate', array(':fromdate' => $fromDate));
        }
        
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $profitabilityObject = $dataQuery->queryAll();
        
        $profitabilityCount = count($profitabilityObject);
        
        $profitabilityJSONData = CJSON::encode($profitabilityObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $profitabilityCount . ',
                    "recordsFiltered": ' . $profitabilityCount . ',
                    "data":' . $profitabilityJSONData . '
        }';
        exit;
    }

    /**
     * Get autocomplete package id.
     */
    public function actionAutoCompleteByPid($term) {
        $match = $term;
        $projectObject = Package::model()->findAll(
                'name LIKE :match', array(':match' => "%$match%")
        );
        $list = array();
        foreach ($projectObject as $q) {
            $data['value'] = $q['id'];
            $data['label'] = $q['name'];
            $list[] = $data;
            unset($data);
        }
        echo json_encode($list);
    }

    /**
     * Changing auction status.
     */
    public function actionChangeStatus() {

        if ($_REQUEST['id']) {
            $auctionObject = Auction::model()->findByPK(BaseClass::mgDecrypt($_REQUEST['id']));
            if ($auctionObject->status == 1) {
                $auctionObject->status = 0;
            } else {
                $auctionObject->status = 1;
            }
            $auctionObject->save(false);
            Yii::app()->user->setFlash('success', "Auction updated successfully.");
            $this->redirect(array('list'));
        }
    }

    /**
     * Changing auction publish status.
     */
    public function actionChangePublish() {

        if ($_REQUEST['id']) {
            $auctionObject = Auction::model()->findByPK($_REQUEST['id']);
            if ($auctionObject->is_publish == 1) {
                $auctionObject->is_publish = 0;
            } else {
                $auctionObject->is_publish = 1;
            }
            $auctionObject->save(false);
            Yii::app()->user->setFlash('success', "Publish Changed Succesfully.");
            $this->redirect(array('list'));
        }
    }

    /**
     * Delete Auction.
     */
    public function actionDeleteAuction() {
        if ($_REQUEST['id']) {
            $auctionObject = Auction::model()->findByPK(BaseClass::mgDecrypt($_REQUEST['id']));
            $auctionObject->delete();
            Yii::app()->user->setFlash('success', "Record Deleted Succesfully.");
            $this->redirect(array('list'));
        }
    }

    /**
     * return false;
     * @param type $auctionObject
     * @return type
     */
    public function update_auction($auctionObject = NULL) {
        if (!$auctionObject)
            return false;

        $bidAuctionCount = BaseClass::getBidAuctionCount();
        $auctionCount = BaseClass::getAuctionsCount();
        $bidCount = ceil(((Yii::app()->params['defaultNamePerc'] * $auctionCount) / 100));
        $notBidAuctionCount = $auctionCount - $bidAuctionCount; // Not bidded auctions.

        if ($notBidAuctionCount > $bidCount) {
            $prefUserId = BaseClass::getPreferentialUser();

            // Create a bid with some default values.            
            AuctionBids::model()->create($prefUserId, 1, 0.01, $auctionObject->id, 3);
        }
    }
    
    public function actionWinnerPayment() {
        $this->render('winnerpayment');
    }
    
    public function actionWinnerPaymentData() {
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        $dataQuery = Yii::app()->db->createCommand()
            ->select('a.id as auctionId, u.name as winnerName, p.name as productName, p.price as productPrice, '
                    . 'a.close_date as closeDate, a.account_details as accountDetails, a.winner_status as winnerStatus, '
                    . '( case when a.winner_status = 1 then "Awaiting Payment" when a.winner_status = 2 then "Paid" ELSE "Awaiting Account Details" END ) as winnerStatusLabel')
            ->from('auction a')
            ->join('product p', 'a.product_id = p.id')
            ->join('user u', 'a.winner_user_id = u.id');
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('p.name LIKE :productName', array(':productName' => '%'.$searchData.'%'));
            $dataQuery->orWhere('u.name LIKE :userName', array(':userName' => '%'.$searchData.'%'));
        }

        if(!empty($_POST['columns']['5']['search']['value']) && !empty($_POST['columns']['5']['search']['regex'])){
            $fromDate = $_POST['columns']['5']['search']['value'] . " 00:00:00";
            $toDate = $_POST['columns']['5']['search']['regex'] . " 23:59:59";
            $dataQuery->andWhere('a.close_date >= :fromdate', array(':fromdate' => $fromDate));
            $dataQuery->andWhere('a.close_date <= :todate', array(':todate' => $toDate));
        }

        $winnerStatusCondition = 'a.winner_status = 1';
        if($_POST['columns'][7]['search']['value'] == 'accdetails') {
             $winnerStatusCondition = 'a.winner_status = 0';
        }
        if($_POST['columns'][7]['search']['value'] == 'paid') {
             $winnerStatusCondition = 'a.winner_status = 2';
        }
        if($_POST['columns'][7]['search']['value'] == 'all') {
             $winnerStatusCondition = 'a.winner_status IN (1,0,2)';
        }
        
        $dataQuery->andWhere($winnerStatusCondition);

        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $winnerListObject = $dataQuery->queryAll();
        
       // Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('auction a')
            ->join('product p', 'a.product_id = p.id')
            ->join('user u', 'a.winner_user_id = u.id');
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $countQuery->andWhere('p.name LIKE :productName', array(':productName' => '%'.$searchData.'%'));
            $countQuery->orWhere('u.name LIKE :userName', array(':userName' => '%'.$searchData.'%'));
        }

        if(!empty($_POST['columns']['5']['search']['value']) && !empty($_POST['columns']['5']['search']['regex'])){
            $fromDate = $_POST['columns']['5']['search']['value'] . " 00:00:00";
            $toDate = $_POST['columns']['5']['search']['regex'] . " 23:59:59";
            $countQuery->andWhere('a.close_date >= :fromdate', array(':fromdate' => $fromDate));
            $countQuery->andWhere('a.close_date <= :todate', array(':todate' => $toDate));
        }

        $countQuery->andWhere($winnerStatusCondition);
            
        $totalRecordsArray = $countQuery->queryRow();
            
        $winnerCount = $totalRecordsArray['totalRecords'];

        $winnerJSONData = CJSON::encode($winnerListObject);
        
        echo '{"draw": '.$draw.',
                    "recordsTotal": '.$winnerCount.',
                    "recordsFiltered": '.$winnerCount.',
                    "data":' . $winnerJSONData . '
        }';
        exit;
        
    }
    
    /**
     * bulkWinnerPaid action will update winner status.
     */
    public function actionbulkWinnerPaid(){
        if (isset($_POST['requestids']) && !empty($_POST['requestids'])) {
            
            $arrayRequestedId = $_POST['requestids'];
            
            $criteria = new CDbCriteria;
            $criteria->addInCondition('id',$arrayRequestedId);
            $auctionUpdateStatus = Auction::model()->updateAll(array('winner_status'=>Auction::$_WINNER_AMOUNT_PAID), $criteria);
            
            if($auctionUpdateStatus) {
                Yii::app()->user->setFlash('success', "Paid Successfully.");
                $this->redirect('/admin/auction/winnerpayment');
            }
        }
        Yii::app()->user->setFlash('error', "Please select auction.");
        $this->redirect('/admin/auction/winnerpayment');
    }
}
