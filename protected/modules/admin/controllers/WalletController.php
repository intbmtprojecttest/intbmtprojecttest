<?php

class WalletController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';
    
    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /* public function init() {
      BaseClass::isAdmin();
      } */

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'list', 'getfundbyamount', 'rpwallet', 'commisionwallet', 
                 'fundwallet', 'cashbackwallet','walletsummary', 'walletsettings', 'walletsummarycsv', 'ecurrencysettings'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionList() {
        if (isset(Yii::app()->session['userid'])) {
            $dataProvider = new CActiveDataProvider('Wallet', array(
                'pagination' => array('pageSize' => 10),
            ));
            $this->render('list', array('dataProvider' => $dataProvider));
        } else {
            $this->redirect(Yii::app()->getHomeUrl());
        }
    }

    public function actionGetFundByAmount() {
        if ($_POST) {
            $userId = $_POST['userId'];
            $type = $_POST['walletId'];
            $walletObject = Wallet::model()->findByAttributes(array('user_id' => $userId, 'type' => $type));
            if (!empty($walletObject)) {
                echo number_format($walletObject->fund, 2);
                exit;
            }
            echo 0;
            exit;
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Wallet;

        // Uncomment the following line if AJAX validation is neededusergetfundbyamount
        // $this->performAjaxValidation($model);

        if (isset($_POST['Wallet'])) {
            $model->attributes = $_POST['Wallet'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Wallet'])) {
            $model->attributes = $_POST['Wallet'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Wallet');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Wallet('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Wallet']))
            $model->attributes = $_GET['Wallet'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Wallet the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Wallet::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Wallet $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'wallet-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /*
     * this will fetch rp wallet
     */

    public function actionRpWallet() {
        $loggedInUserId = Yii::app()->session['userid'];
        $todayDate = Yii::app()->params['startDate'];
        $pageSize = Yii::app()->params['defaultPageSize'];
        $fromDate = date('Y-m-d');
        if (!empty($_POST)) {
            $todayDate = date('Y-m-d', strtotime($_POST['from']));
            $fromDate = date('Y-m-d', strtotime($_POST['to']));
        }

        $walletobject = Wallet::model()->findByAttributes(array('user_id' => $loggedInUserId, 'type' => 2));
        if ($walletobject) {
            $walletId = $walletobject->id;
        } else {
            $walletId = 0;
        }
        $dataProvider = new CActiveDataProvider('MoneyTransfer', array(
            'criteria' => array(
                'condition' => ('(wallet_id="' . $walletId . '" OR to_wallet_id="' . $walletId . '")  AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND (to_user_id = ' . $loggedInUserId . ' OR from_user_id = "' . $loggedInUserId . '")'), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));
//        echo "<pre>"; print_r($dataProvider);exit;
        $this->render('/report/rpwallet', array('dataProvider' => $dataProvider));
    }

    /*
     * this will fetch commision wallet
     */

    public function actionCommisionWallet() {
        $todayDate = Yii::app()->params['startDate'];
        $pageSize = Yii::app()->params['defaultPageSize'];
        $fromDate = date('Y-m-d');
        if (!empty($_POST)) {
            $todayDate = date('Y-m-d', strtotime($_POST['from']));
            $fromDate = date('Y-m-d', strtotime($_POST['to']));
        }

        $loggedInUserId = Yii::app()->session['userid'];
        $walletobject = Wallet::model()->findByAttributes(array('user_id' => $loggedInUserId, 'type' => 3));
        if ($walletobject) {
            $walletId = $walletobject->id;
        } else {
            $walletId = 0;
        }
        $dataProvider = new CActiveDataProvider('MoneyTransfer', array(
            'criteria' => array(
                'condition' => ('(wallet_id="' . $walletId . '" OR to_wallet_id="' . $walletId . '") AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND (to_user_id = ' . $loggedInUserId . ' OR from_user_id = "' . $loggedInUserId . '")'), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));
        $this->render('/report/commissionwallet', array('dataProvider' => $dataProvider));
    }

    /*
     * this will fetch fund wallet
     */

    public function actionFundWallet() {
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $condition = "(from_fund_type = 1 OR to_fund_type = 1)"; //Default condition
        
          if (!empty($_GET['from']) && !empty($_GET['to'])) {
               $todayDate = date('Y-m-d', strtotime($_GET['from']));
               $fromDate = date('Y-m-d', strtotime($_GET['to']));
               $condition .= ' AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"';
            }
       
         $dataProvider = new CActiveDataProvider('MoneyTransfer', array(
                'criteria' => array(
                    'condition' => ($condition), 'order' => 'id DESC',
                ), 'pagination' => array('pageSize' => $pageSize),
            ));

        $this->render('/report/fundwallet', array('dataProvider' => $dataProvider, 'pageSize' => $pageSize));
    }

    /**
     * Affiliate wallet.
     */
    public function actionCashBackWallet() {
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $condition = "(from_fund_type = 2 OR to_fund_type = 2)"; //Default condition
        
          if (!empty($_GET['from']) && !empty($_GET['to'])) {
               $todayDate = date('Y-m-d', strtotime($_GET['from']));
               $fromDate = date('Y-m-d', strtotime($_GET['to']));
               $condition .= ' AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"';
            }
     
       $dataProvider = new CActiveDataProvider('MoneyTransfer', array(
                'criteria' => array(
                    'condition' => ($condition), 'order' => 'id DESC',
                ), 'pagination' => array('pageSize' => $pageSize),
            ));

        $this->render('cashback_wallet', array('dataProvider' => $dataProvider, 'pageSize' => $pageSize));
    }
    
    public function actionWalletSummary() {
        $condition = '';
        $error = '';
        $walletType = '';
        $userId = "";
        $dataProvider ="";
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
     
        $order = "";
        if(empty($_GET['MoneyTransfer_sort'])){
            $order = 'id DESC';
        }
        
        if(!empty($_GET)){
            if(!empty($_GET['username']) && !empty($_GET['walletType'])) {
                $condition = 'status = 1';
                $userObject = User::model()->findByAttributes(array('name' => $_GET['username']));
                if(!empty($userObject)) {
                    $userId = $userObject->id;
                    $walletType = $_GET['walletType'];
                    $dataProvider = new CActiveDataProvider('MoneyTransfer', array(
                    'criteria' => array(
                    'condition' => (' t.status = 1 AND ((t.to_user_id = ' . $userId .' AND t.to_fund_type = '. $walletType . ') OR (t.from_user_id = ' . $userId . ' AND t.from_fund_type = '. $walletType .'))'), 'order' => $order,
                    ), 'pagination' => array('pageSize' => $pageSize),));
                
                } else {
                     $error = "Incorrect username";
                }
                
            } else {
                if(empty($_GET['username']) && empty($_GET['walletType'])) {
                  $error = "Please enter username and wallet";
                } elseif(empty($_GET['username'])){
                  $error = "Please enter username";
                }elseif(empty($_GET['walletType'])){
                  $error = "Please select wallet";
                } else{
                  $error = "Please enter username or wallet";
                }
            }
        }

        $this->render('/wallet/walletsummary', array('dataProvider' => $dataProvider, 
            'walletType' => $walletType, 'pageSize' => $pageSize, 'userId' => $userId, 
            'error' => $error));
        
    }
    
    public function actionWalletSummaryCSV(){
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="tranction.csv"');        
        $dataTransfer = "TransactionID,Date,Particulars,Deposit(+),Withdraw(-),Balance,Mode,Comment\n";  
        
        $condition = '';
        $error = '';
        $walletType = '';
        $userId = '';

            if(!empty($_GET['username']) && !empty($_GET['walletType'])) {
                $condition = 'status = 1';
                $userObject = User::model()->findByAttributes(array('name' => $_GET['username']));
                if(!empty($userObject)) {
                    $userId = $userObject->id;
                    $condition .= ' AND (to_user_id = ' . $userId . ' OR from_user_id = ' . $userId.')';
                    
                    $walletType = $_GET['walletType'];
                    $condition .= " AND (to_fund_type = '" . $walletType . "' OR from_fund_type = '" . $walletType . "') ";
                    
                }
            } 
        
        $criteria = new CDbCriteria;
        $criteria->condition = ($condition);
        $criteria->order = 'id DESC';
        $moneyTransferObjects = MoneyTransfer::model()->findAll($criteria);
      //echo '<pre>';print_r($moneyTransferObjects);exit;
        if (count($moneyTransferObjects) > 0) {
            foreach ($moneyTransferObjects as $moneyTransferObjectsList) {   
                
                $dataTransfer .=
                    ($moneyTransferObjectsList->transaction->transaction_id).",".  
                    ($moneyTransferObjectsList->created_at).",".
                    ((isset($userId) && $userId == $moneyTransferObjectsList->from_user_id) ? 'To:'.$moneyTransferObjectsList->touser->name : 'From:'.$moneyTransferObjectsList->fromuser->name).",". 
                    ((isset($userId) && $userId == $moneyTransferObjectsList->to_user_id) ? $moneyTransferObjectsList->fund : '-').",".                    
                    ((isset($userId) && $userId == $moneyTransferObjectsList->from_user_id) ? $moneyTransferObjectsList->fund : '-').",".                    
                    ((isset($userId) && $userId == $moneyTransferObjectsList->from_user_id) ? $moneyTransferObjectsList->from_user_balance : $moneyTransferObjectsList->to_user_balance).",".                    
                    ($moneyTransferObjectsList->transaction->mode).",".                    
                    (isset($moneyTransferObjectsList->comment) ?  $moneyTransferObjectsList->comment : 'NA').","."\n";
              
            }
        }
        echo $dataTransfer;
        exit();
        
    }
    
    public function actionWalletSettings(){
        if($_POST) {
            
            $walletTypeObject = WalletType::model()->findByAttributes(array('id' => $_POST['id']));
            if(!empty($walletTypeObject)){
                $walletTypeObject->name = $_POST['name'];
                $walletTypeObject->withdrawal_charges = $_POST['withdrawal_charges'];
                $walletTypeObject->withdraw_status = $_POST['withdraw_status'];
                $walletTypeObject->transfer_charges = $_POST['transfer_charges'];
                $walletTypeObject->transfer_status = $_POST['transfer_status'];
                $walletTypeObject->purchase_percentage = $_POST['purchase_percentage'];
                if($walletTypeObject->update(false)) {
                    echo 'success';exit;
                } else {
                    echo 'failed';exit;
                }  
                
            }
        }
        $walletTypeObject = WalletType::model()->findAll();
        $this->render('/wallet/walletsettings', array('walletTypeObject' => $walletTypeObject));
    }
    
    public function actionEcurrencySettings() {
        $condition = " id != 8 " ;
        
        if (!empty($_POST)) {
            $gatewayObject = Gateway::model()->findByAttributes(array('id' => $_POST['id']));
            if(!empty($gatewayObject)){
                $gatewayObject->mode = $_POST['mode'];
                $gatewayObject->title = $_POST['title'];
                $gatewayObject->description = $_POST['description'];
                $gatewayObject->min_deposit = $_POST['min_deposit'];
                $gatewayObject->min_withdrawal = $_POST['min_withdrawal'];
                $gatewayObject->priority = $_POST['priority'];
                $gatewayObject->withdrawal_status = $_POST['withdrawal_status'];
                $gatewayObject->payment_status = $_POST['payment_status'];
                if($gatewayObject->update(false)) {
                    echo 'success';exit;
                } else {
                    echo 'failed';exit;
                } 
            }
        }
        
        if(isset($_GET['mode']) && $_GET['mode'] != '' ){ 
            $condition .= " and mode ='".$_GET['mode']."'" ;
        }
        if(isset($_GET['withdrawal_status']) && $_GET['withdrawal_status'] != '' ){ 
            $condition .= " and withdrawal_status ='".$_GET['withdrawal_status']."'" ;
        }
        if(isset($_GET['payment_status']) && $_GET['payment_status'] != '' ){ 
            $condition .= " and payment_status ='".$_GET['payment_status']."'" ;
        }
        
        $condition .= " order by payment_status desc" ;  
        
        $gatewayObject = Gateway::model()->findAll(array('condition' => $condition ));
        $this->render('/wallet/ecurrencysettings', array(
            'gatewayObject' => $gatewayObject,
        ));
    }

}
