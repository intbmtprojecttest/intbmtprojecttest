<?php

class WithdrawalController extends Controller
{
    public function init() {
        BaseClass::isAdmin();
    }

    public $layout = 'main';

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'getwalletdata', 'list','history','request','payment', 'edit', 'sendmail'), 
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){ 
      $this->render('request')  ;
    }

    public function actionGetWalletData() {            
        if ($_POST) {
            $userId = Yii::app()->session['userid'] ;
            $type = $_POST['walletId'];
            $walletObject = Wallet::model()->findByAttributes(array('user_id'=>$userId,'type'=>$type));
            if(!empty($walletObject)){
              echo  number_format($walletObject->fund,2);              
              exit;
            }else
            echo 0;exit;
        }
    }

    public function GetWithdrawalAction($data){          
        if($data->status == 0){
            $action = '<form action="" method="post"><input type="hidden" value="' . BaseClass::mgEncrypt($data->id) . '" name="request_id"><input type="submit" value="Cancel" name="cancel" class="btn orange  margin-right15" onclick="{return confirmCancel();}" ></form>';
        }else{
            $action = "N/A";
        }
        echo $action;
    }    

    public function GetWithdrawalStatus($data){
        echo BaseClass::getWithDrowStatus($data->status);
    }

    public function GetWithdrawalComment($data){ 
        $comment = $data->comment ? $data->comment:'' ;
        echo "<textarea rows='4' style='display:none' class='form-control' cols='50' id='comment_{$data->id}'  name='comment[".$data->id."]'>".$comment."</textarea>";
        echo '<a onclick="showText('.$data->id.')">Comment</a>';
    }

    public function GetWithdrawalCheckbox($data){
        echo "<input type='checkbox' class='allcheckbox' name='requestids[".$data->id."]' value='".$data->id."'>" ;
    }

    public function actionHistory(){
        $success ="";
        $model = new WithDrawal;
        $pageSize = Yii::app()->params['defaultPageSize'];
        $userId = Yii::app()->session['userid'] ;
        $condition ="";
        if (!empty($_GET['from']) && !empty($_GET['to'])) {
            $todayDate = $_GET['from'];
            $fromDate = $_GET['to'];                
            $condition .= 't.created_at >= "' . $todayDate . '" AND t.created_at <= "' . $fromDate . '" ';          
        }
        
        if (!empty($_GET['res_filter'])) {
            if(!empty($condition)){
                $condition .= " AND ";   
            }
            $condition .= "t.status = ".$_GET['res_filter'];                
        }    

        $order = "";
        if(empty($_GET['WithDrawal_sort'])) {
            $order = 't.id DESC';
        }

        $criteria=new CDbCriteria;
        $criteria->with = array( 'user');
        $criteria->condition = $condition;
        $criteria->order = $order;

        $dataProvider = new CActiveDataProvider($model, array(
        'criteria' => $criteria,
        'pagination' => array('pageSize' => $pageSize),
        'sort'=>array(
        'attributes'=>array(
                'username'=>array('asc'=>'user.name', 'desc'=>'user.name DESC'),
                '*',
            ),
        )));

        $this->render('history', array('dataProvider' => $dataProvider, 'success' => $success));
    }

    public function actionRequest(){
        $label = "Pending Request";
        $success ="";
        $model = new WithDrawal;
        $pageSize = Yii::app()->params['defaultPageSize'];
        $userId = Yii::app()->session['userid'] ;            

        $order = 'id DESC';
        
        if(!empty($_GET['WithDrawal_sort'])) {
            $order = 't.id DESC';
        }
        //Update Withdraw request with comment.
        if(!empty($_POST['requestids'])){                
            $arrayRequestedId = $_POST['requestids'];
            $arrayComment = $_POST['comment'];
            $aUniques = array_intersect_key($arrayComment,$arrayRequestedId);
            $status = 0;
            if(isset($_POST['proceed'])){$status = 4 ; }
            if(isset($_POST['reject'])){ $status = 2 ; }                
            // for the withdrawal request update.

            $withDrawalSuccessIds = BaseClass::updateWithDrawalStatus($aUniques,$status);
            $this->redirect('request');

        }

        $dataProvider = new CActiveDataProvider($model, array(
        'pagination' => array('pageSize' => $pageSize),    
        'criteria' => array(
            'condition' => ( ' status = 0 '), 'order' => $order,
        )));

        $this->render('list', array('dataProvider' => $dataProvider, 'success' => $success ,'label'=>$label,));
    }

    public function actionPayment(){
        $label = "Pending Payment";
        $success ="";
        $model = new WithDrawal;
        $pageSize = Yii::app()->params['defaultPageSize'];
        $userId = Yii::app()->session['userid'] ;            

       if(!empty($_POST['requestids'])){                
            $arrayRequestedId = $_POST['requestids'];
            $arrayComment = $_POST['comment'];
            $aUniques = array_intersect_key($arrayComment,$arrayRequestedId);
            $status = 0;
            if(isset($_POST['reject'])){ $status = 2 ; }
            if(isset($_POST['success'])){ $status = 1 ;}
            // for the withdrawal request update.

            $withDrawalSuccessIds = BaseClass::updateWithDrawalStatus($aUniques,$status);
            
            /*mail to user*/
            if(!empty($withDrawalSuccessIds)){
                foreach($withDrawalSuccessIds as $withDrawalSuccess){
                    $withDrawObject = WithDrawal::model()->findByPk($withDrawalSuccess);
                    if($withDrawObject->status == 1){
                    $userObject = $withDrawObject->user();
                    
                    $config['to'] = $userObject->email;
                    $config['subject'] = 'Withdrawal request completed successfully';
                    $config['body'] =  $this->renderPartial('/mailTemplate/success-withdraw', array('userObjectArr'=>$userObject, 'withDrawObjectArr' => $withDrawObject),true);
                    CommonHelper::sendMail($config);
                    }
                }
            }

        }

        $dataProvider = new CActiveDataProvider($model, array(
        'pagination' => array('pageSize' => $pageSize),    
        'criteria' => array(
            'condition' => ( ' status = 4 '), 'order' => 'updated_at DESC',
        )));

        $this->render('list', array('dataProvider' => $dataProvider, 'success' => $success ,'label'=>$label));

    }
    
    public function actionEdit(){
        $success = "";
        $error = "";
        if(isset($_GET['id'])) {
            $withdrawalObject = WithDrawal::model()->findByAttributes(array('id' => BaseClass::mgDecrypt($_GET['id'])));
        } else {
            $this->redirect(array('/admin/withdrawal/request'));
        }
        $countryObject = Country::model()->findAll();
        $jsonDecodeData = json_decode($withdrawalObject->bank_wire_details);
        
        if($_POST) {
            $withdrawalObject->account_number = $_POST['account_number'];
            $withdrawalObject->comment = $_POST['comment'];
            
            if($withdrawalObject->gateway()->id == 12 || $withdrawalObject->gateway()->id == 13 || $withdrawalObject->gateway()->id == 14){
                $postDataBankWire['beneficiary_name'] = $_POST['beneficiary_name'];
                $postDataBankWire['beneficiary_account_no'] = $_POST['beneficiary_account_no'];
                $postDataBankWire['beneficiary_country'] = $_POST['beneficiary_country'];
                $postDataBankWire['beneficiary_city'] = $_POST['beneficiary_city'];
                $postDataBankWire['beneficiary_bank_name'] = $_POST['beneficiary_bank_name'];
                $postDataBankWire['beneficiary_bank_branch'] = $_POST['beneficiary_bank_branch'];
                $postDataBankWire['beneficiary_bank_no'] = $_POST['beneficiary_bank_no'];
                $postDataBankWire['beneficiary_routing_code'] = $_POST['beneficiary_routing_code'];
                $postDataBankWire['beneficiary_email'] = isset($_POST['beneficiary_email'])?$_POST['beneficiary_email']:"";
                $postDataBankWire['beneficiary_cor_bank'] = isset($_POST['beneficiary_cor_bank'])?$_POST['beneficiary_cor_bank']:"";
                $postDataBankWire['beneficiary_cor_swift_adrs'] = isset($_POST['beneficiary_cor_swift_adrs'])?$_POST['beneficiary_cor_swift_adrs']:"";
                $postData['bank_wire_details'] = CJSON::encode($postDataBankWire); 
                $withdrawalObject->bank_wire_details = $postData['bank_wire_details'];
            }
            
            if ($withdrawalObject->update()) {
                Yii::app()->user->setFlash('requestMsg','Details Updated Succussfully.');
                if($withdrawalObject->status == 0) {
                    $this->redirect(array('/admin/withdrawal/request'));
                } else if($withdrawalObject->status == 4) {
                    $this->redirect(array('/admin/withdrawal/payment'));
                }
             }
        }
        
        $this->render('edit', array(
            'withdrawalObject' => $withdrawalObject,
            'countryObject' => $countryObject,
            'jsonDecodeData' => $jsonDecodeData,
        ));
    }
    
    public function actionSendMail(){
        if(isset($_POST['id'])) {
            $mailStatus = '';
            $userId = $_POST['user_id'];
            $msg = $_POST['email_body'];
            
            $postDataArray['subject'] = $_POST['email_subject'];
            
            $mailObject = Mail::model()->create($postDataArray);
            if ($mailObject) {
                $postDataArray['mail_id'] = $mailObject->id;
                $postDataArray['from_user_id'] = 1; // Admin
                $postDataArray['to_user_id'] = $userId;
                $postDataArray['department_id'] = 1; // Billing.
                $postDataArray['status'] = 0; // Unread.                                
                $postDataArray['message'] = $msg;
                $postDataArray['replied_by'] = 0;
                $mailStatus = UserHasConversations::model()->create($postDataArray, '');
                if(!empty($mailStatus)) {
                    Yii::app()->user->setFlash('requestMsg', "Mail Sent Successfully.");
                    if($_POST['status'] == 0) {
                        $this->redirect(array('/admin/withdrawal/request'));
                    } else if($_POST['status'] == 4) {
                        $this->redirect(array('/admin/withdrawal/payment'));
                    }
                }
            }
        }
        
        $withdrawalObject = WithDrawal::model()->findByAttributes(array('id' => BaseClass::mgDecrypt($_GET['id'])));
        $this->renderPartial('sendmailform', array('withdrawalObject' => $withdrawalObject));
    }
    
    public function GetBankWireDetails($data) {
        $bankWireDeatails = "";
        if (!empty($data->bank_wire_details)) {
            $jsonDecodeData = json_decode($data->bank_wire_details);
            $bankWireDeatails .= "<b>Beneficiary Details</b> <br> Beneficiary Name: " . $jsonDecodeData->beneficiary_name . "<br>";
            $bankWireDeatails .= "Beneficiary IBAN/Account Number: " . $jsonDecodeData->beneficiary_account_no . "<br>";
            $bankWireDeatails .= "Beneficiary Branch Country: " . $jsonDecodeData->beneficiary_country . "<br>";
            $bankWireDeatails .= "Beneficiary Branch City: " . $jsonDecodeData->beneficiary_city . "<br>";
            $bankWireDeatails .= "Beneficiary Bank Name: " . $jsonDecodeData->beneficiary_bank_name . "<br>";
            $bankWireDeatails .= "Beneficiary Bank Branch: " . $jsonDecodeData->beneficiary_bank_branch . "<br>";
            $bankWireDeatails .= "Beneficiary Bank SWIFT/BIC: " . $jsonDecodeData->beneficiary_bank_no . "<br>";
            $bankWireDeatails .= "Routing Code: " . $jsonDecodeData->beneficiary_routing_code . "<br>";
            if (!empty($jsonDecodeData->beneficiary_email)) {
                $bankWireDeatails .= "Beneficiary E-mail address: " . $jsonDecodeData->beneficiary_email . "<br>";
            }
            if (!empty($jsonDecodeData->beneficiary_cor_bank) || !empty($jsonDecodeData->beneficiary_cor_swift_adrs)) {
                $bankWireDeatails .= "<b>Beneficiary Correspondent Bank Details</b> <br>";
            }
            if (!empty($jsonDecodeData->beneficiary_cor_bank)) {
                $bankWireDeatails .= "Correspondent Bank: " . $jsonDecodeData->beneficiary_cor_bank . "<br>";
            }
            if (!empty($jsonDecodeData->beneficiary_cor_swift_adrs)) {
                $bankWireDeatails .= "SWIFT Address: " . $jsonDecodeData->beneficiary_cor_swift_adrs . "<br>";
            }
        } else {
            $bankWireDeatails = "";
        }
        echo $bankWireDeatails;
    }

    public function checkRestricted($data) {
        $isRestricted = BaseClass::isRestricted($data->user_id);
        if($isRestricted['IsRestricted']) {
            echo 'YES';
        } else {
            echo 'NO';
        }
    }
    
    public function userCountry($data) {
        $profileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->user_id));
        echo $profileObject->country()->name ? $profileObject->country()->name : "N/A";
    }
}