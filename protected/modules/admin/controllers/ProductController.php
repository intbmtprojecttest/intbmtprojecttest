<?php

class ProductController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'add', 'edit', 'deleteproduct', 'editimages', 'deleteimages', 'list',
                    'vendorlist', 'changestatus', 'deletepackage', 'getPackageUpdatedTime','thumbupload', 'getprouctavailability'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionIndex()
    {
            $this->render('index');
    }
    
    /*
     * Function to add package in database
     */

    public function actionAdd() {
        $error = "";
        $success = "";
        $thumbnails = "";
      
        $path = Yii::app()->params['imagePath']['package'];
        if ($_POST) {
            if ($_POST['name'] == '' && $_POST['price'] == '') {
                $error .= "Please fill required(*) marked fields.";
            } else {
                if(!isset($_FILES['image']['name']) || $_FILES['image']['name'] != ''){
                    $newThumbnails = BaseClass::createUploadFilename($_FILES['image']['name']);
                    $thumbnails = time() . $newThumbnails;
                    $thumbnailsArray = explode(".", $newThumbnails);
                    $ext1 = end($thumbnailsArray);
                    if ($ext1 == '' || $ext1 != "jpg" && $ext1 != "png" && $ext1 != "jpeg") {
                        $error = "Please upload mentioned file type.";
                    } else if ($_FILES['image']["size"] > 5242880) {
                        $error = "File can not be greater than 5MB.";
                    }else{
                        BaseClass::uploadFile($_FILES['image']['tmp_name'], $path, $thumbnails);
                    }
                }
                
                if($error == ''){
                    $product = new Product;
                    $product->attributes = $_POST;
                    $product->name = $_POST['name'];
                    $product->price = $_POST['price'];
                    $product->image = $thumbnails;
                    $product->created_by = 1;
                    $product->purchase_type = $_POST['purchase_type'];
                    $product->description = isset($_POST['description'])?$_POST['description']:'';
                    $product->status = $_POST['status'] ;
                    $product->is_cover = $_POST['is_cover'] ;
                    $product->created_at = date('Y-m-d');
                    $product->save(false);
                    Yii::app()->user->setFlash('success', "Product Successfully Added.");
                    $this->redirect(array('list'));
                }
            }
        }
        $this->render('add', array('success' => $success, 'error' => $error));
    }
    
    
    /*
     * Function to Update package
     */

    public function actionedit() {
        $error = "";
        $success = "";
        $path = Yii::app()->params['imagePath']['package'];
        
        $productObject = Product::model()->findByPK(array('id' => BaseClass::mgDecrypt($_GET['id'])));
        if ($_POST) {
            $message = "Updated";
            if(!empty($_POST['is_copy'])){
                $productObject = new Product;
                $productObject->created_at = date('Y-m-d');
                $message = "Added";
            }
            if ($_POST['name'] == '' && $_POST['price'] == '' && $_POST['description'] == '') {
                $error .= "Please fill required(*) marked fields.";
                $this->redirect('list?error=' . $error);
            } else {
                if(!empty($_FILES['image']['name'])){
                    $newThumbnails = BaseClass::createUploadFilename($_FILES['image']['name']);
                    $thumbnails = time() . $newThumbnails;
                    $thumbnailsArray = explode(".", $newThumbnails);
                    $ext1 = end($thumbnailsArray);
                    BaseClass::uploadFile($_FILES['image']['tmp_name'], $path, $thumbnails);
                    $productObject->image = $thumbnails;
                }
                $productObject->name = $_POST['name'];
                $productObject->price = $_POST['price'];
                $productObject->created_by = 1;
                $productObject->purchase_type = $_POST['purchase_type'];
                $productObject->description = $_POST['description'];
                $productObject->status = $_POST['status'];
                $productObject->is_cover = $_POST['is_cover'];
                $productObject->save(false);

                Yii::app()->user->setFlash('success', "Product Successfully ".$message);
                $this->redirect(array('list'));
            }
        }
        $this->render('edit', array( 'error' => $error, 'productObject' => $productObject));
    }

    /*
     * Function to list of product
     */

    public function actionList() {
        $status = 1;
        $condition = "";
        $name = "";
        $pageSize = Yii::app()->params['defaultPageSize'];
      
        if(isset($_GET['res_filter'])){
            $status = $_GET['res_filter']; 
        }
        // Default  Condition.
        if($status != "all"){
            $condition = 'status ='.$status;  
        } else {
            $condition = 'status IN(1,0)';
        }
         
        if (!empty($_GET['name'])) {
            $name = $_GET['name'];
            $condition .= " AND name like '%" . $name . "%' ";
        }
       // echo '<pre>';print_r($condition);exit;
        $dataProvider = new CActiveDataProvider('Product', array(
                'criteria' => array(
                    'condition' => ($condition), 'order' => 'id DESC',
                ), 'pagination' => array('pageSize' => $pageSize),
            ));

        $this->render('list', array(
            'dataProvider' => $dataProvider, 'msg' => 0,
            'name' => $name,'status' => $status
        ));
    }

    public function  getPurchaseStatus($data) {
        $productObject = Product::model()->findByPK($data->id);
        if(!empty($productObject)){
            if($productObject->purchase_type==1){
                echo "Buy";
            }
            else if($productObject->purchase_type==2){
                echo "Bid";
            }
            else if($productObject->purchase_type==3){
                echo "Buy and Bid";
            }
            else{
                echo "NA";
            }
        }
    }
    
    public function actionChangeStatus() {
        if ($_REQUEST['id']) {
            $productId = BaseClass::mgDecrypt($_REQUEST['id']);
            $productObject = Product::model()->findByPK($productId);
            if ($productObject->status == 1) {
                $productObject->status = 0;
                $auctionObejct = Auction::model()->updateAll(array( 'is_publish' => 0 ), 'product_id = '.$productId.' AND status = 1' );
            } else {
                $productObject->status = 1;
            }
            $productObject->update(false);
            Yii::app()->user->setFlash('success', "Product updated successfully.");
            $this->redirect(array('/admin/product/list'));
            
        }
    }

    /*
     * Function to Delete Package 
     */
    public function actionDeleteProduct() { 
        if ($_REQUEST['id']) {
            $udpateId = BaseClass::mgDecrypt($_REQUEST['id']) ;
            $productObject = Product::model()->findByPK($udpateId);
            $productObject->status = 2 ;
            $productObject->update();
            $auctionObejct = Auction::model()->updateAll(array( 'is_publish' => 0 ), 'product_id = '.$udpateId.' AND status = 1' );
            Yii::app()->user->setFlash('success', "Record Deleted Succesfully.");
            $this->redirect(array('/admin/product/list'));
        }
    }
    
    public function actionGetProuctAvailability() {
        if ($_POST['productName'] != '') {
            $productName = $_POST['productName'];
            /* For add product */
            if($_POST['type'] == 'add'){
                $getUserObject = Product::model()->findByAttributes(array('name' => $productName));  
            }else{
                $getUserObject = Product::model()->count(array('condition' =>  'name = "'.$productName.'" and id != '.$_POST['id']));  
            }
            
            if ($getUserObject) {
                    echo 1 ;
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            
        }
    }
}