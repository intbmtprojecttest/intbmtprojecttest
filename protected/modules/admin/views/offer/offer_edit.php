<?php
$this->breadcrumbs = array(
    'Operation' => '/admin/offer',
    'Offer Edit',
);

?>
<div class="col-md-7 col-sm-7">
    <?php if (isset($error) && !empty($error)) { ?><div class="error"><?php echo $error; ?></div><?php } ?>
    <?php 
        if (isset($success) && !empty($success)) { ?>
            <div class="success"><?php echo $success; ?> &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo $this->createUrl('offer/index');?>">Click here to view. </a></div> 
    <?php } ?>

    <form action="" method="post" action="/offer/add" class="form-horizontal" onsubmit="return validateForm();" enctype="multipart/form-data">

        <fieldset>
            <legend>Edit Offer</legend>

            <div class="form-group">
                <label for="offer_title" class="col-lg-4 control-label">Offer Title <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" maxlength="50" id="offer_title" name="offer_title" value="<?php echo isset($offerObject->offer_title) ? $offerObject->offer_title : '';?>">
                    <span id="offer_title_error" class="clrred"></span>                    
                </div>
            </div>

            <div class="form-group">
                <label for="bidpoints" class="col-lg-4 control-label">Bid Points <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="bid_points" name="bid_points" maxlength="5" onkeyup="return isValidAmount(this);" value="<?php echo isset($offerObject->bid_points) ? $offerObject->bid_points : '';?>">
                    <span id="bid_points_error" class="clrred"></span></div>
            </div>

            <div class="form-group">
                <label for="bidpointsprec" class="col-lg-4 control-label">Offered Bid Points(%) <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="offered_bid_points" name="offered_bid_points" maxlength="2"  onkeyup="return isValidAmount(this);" value="<?php echo isset($offerObject->offered_bid_points) ? $offerObject->offered_bid_points : '';?>">
                    <span id="offered_bid_points_error" class="clrred"></span></div>
            </div>

            <div class="form-group">
                <label for="offer-image" class="col-lg-4 control-label">Offer Image</span></label>
                <div class="col-lg-8">
                    <input type="file" class="form-control" name="offer_image" id="offer_image" accept=".png, .jpg, .gif, .jpeg">                    
                    <span id="offer_image_error" class="clrred"></span></div>
            </div>
            <input type="hidden" value="<?php echo isset($offerObject->id) ? $offerObject->id : '';?>" id="offer_id" name="offer_id" />

        </fieldset>

        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Submit" class="btn red">
            </div>
        </div>

    </form>

</div> 


<script>
    function isValidAmount(object) {
        var fieldVal = parseFloat($("#" + object.id).val());
        if (!$.isNumeric(fieldVal)) {
            $("#" + object.id).val("");
            return false;
        }

        if ((fieldVal.toString().split(".")[1].length) >= 1) {
            $("#" + object.id).val("");
            return false;
        }
        return false;
    }

    function validateForm() {
        var title = $("#offer_title").val();
        var bidpoints = $("#bid_points").val();
        var offered_bidpoints = $("#offered_bid_points").val();
        //var offered_image = $("#offer_image").val();

        $("#offer_title_error").html("");
        $("#bid_points_error").html("");
        $("#offered_bid_points_error").html("");

        if (title === '') {
            $("#offer_title_error").html("Please enter title.");
            $("#offer_title").focus();
            return false;
        }

        if (bidpoints === '' || bidpoints <= 0) {
            $("#bid_points_error").html("Please enter valid bid points.");
            $("#bid_points").focus();
            return false;
        }

        if (offered_bidpoints === '' || offered_bidpoints <= 0) {
            $("#offered_bid_points_error").html("Please enter valid offerd bid points.");
            $("#offered_bid_points").focus();
            return false;
        }

        /*if (offered_image === '') {
            $("#offer_image_error").html("Please upload offer image.");
            $("#offer_image").focus();
            return false;
        }*/

        return true;
    }
</script>
