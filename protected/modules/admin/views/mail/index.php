<ul class="page-breadcrumb breadcrumb">
    <li>
        <div class="breadcrumbs">
            <a href="/admin/default/dashboard">Dashboard</a> »
            <a href="/admin/mail">Mail</a> »
            <span>Inbox</span>
        </div>
    </li>
</ul>
<?php 
if(!empty($_GET['successMsg']) && $_GET['successMsg']=='1'){
    echo "<div class='success'>Message sent successfully.</div>";
}else{
 $_GET['successMsg'] = $successMsg;   
}
?>
    <div class="col-md-12 user-status mailBtns">
        <div class="col-md-3 padding-right0"> 
            <?php echo CHtml::link(Yii::t('translation', 'Compose') . ' <i class="fa fa-edit"></i>', '/admin/mail/compose', array("class" => "btn btn-xs green")); ?>
            <?php echo CHtml::link(Yii::t('translation', 'Inbox') . ' <i class="fa fa-envelope"></i>', '/admin/mail',  array("class" => "btn btn-xs green")); ?> 
            <?php echo CHtml::link(Yii::t('translation', 'Sent') . ' <i class="fa fa-share"></i>', '/admin/mail/sent', array("class" => "btn btn-xs green")); ?> 
        </div>
        <?php if(Yii::app()->session['userid']){ if (isset($userDepartment)){ ?>
                <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/mail" class="form-inline">
                   <div class="expiration confirmMenu">
                    <div class="col-md-3  ">
                    <select class="customeSelect howDidYou form-control input-medium select2me confirmBtn" id="ui-id-5" name="res_filter">
                        <option value="">All</option>
                        <?php foreach ($userDepartment as $department) { ?>
                            <option value="<?php echo $department->department_id; ?>" <?php if (!empty($_GET['res_filter']) && $_GET['res_filter'] == $department->department_id) { ?> selected="selected"<?php } ?>><?php echo ucwords($department->department()->name); ?></option>
                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-md-3 mobile-top-10">
                            <input type="text" name="search_user_name" id="search_user_name" value="<?php if (!empty($_GET['search_user_name'])) { echo $_GET['search_user_name']; } ?>" placeholder="Enter User Name" class="form-control">
                    </div> 
                    <div class="col-md-2 ">
                        <div class="dataTables_length" id="search_length">
                            <label>Display</label>
                            <select id="per_page" name="per_page" aria-controls="" class="customeSelect  form-control select2me " onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                                    <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                                    <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                                    <?php } ?>
                            </select>   
                        </div>
                    </div> 
                    <div class="col-md-1 no_pad_left-right">
                    <input type="submit" class="btn btn-primary confirmOk" value="OK" name="submit" id="submit"/>
                    </div>
                   </div>
                    <div class="col-md-12 margin-top-10 padding-mail">
                        <a class="btn btn-xs blue" onclick="markRed('read')">Mark Read<i class="fa fa-check"></i></a>
                        <a class="btn btn-xs red" onclick="markRed('unread')">Mark UnRead<i class="fa fa-times"></i></a>
                    </div>
                </form>
        <?php } } ?>
    </div>
  <form name="markrednunred" id="markrednunred" method="post" action="/admin/mail/mark?res_filter=<?php echo isset($_GET['res_filter'])?$_GET['res_filter']:''; ?>&per_page=<?php echo isset($_GET['per_page'])?$_GET['per_page']:''; ?>&submit=<?php echo isset($_GET['submit'])?$_GET['submit']:''; ?>">
    <input type="hidden" value="" name="actiontype" id="actiontype"/>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'room-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}",
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'rowCssClassExpression' => '($data->status == 1) ? "" : "info"',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                        'name' => '',
                        'header' => '<span style="white-space: nowrap;"><input type="checkbox" onclick="selectAll(this)" id="selectall"></span>',
                        'value'=>array($this,'GetMailCheckbox'),
                ),
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">Sl.No</span>',
                ),
                array(
                    'name' => 'from_user_id',
                    'header' => '<span style="white-space: nowrap;">Sender &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->fromuser->name',
                ),
                array(
                    'name' => 'to_user_id',
                    'header' => '<span style="white-space: nowrap;">Receiver &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->touser->name',
                ),
                array(
                    'name' => 'department_id',
                    'header' => '<span style="white-space: nowrap;">Department &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->department->name) ?  $data->department->name : ""',
                ),
                array(
                    'name' => 'id',
                    'htmlOptions' => array('width' => '30%'),
                    'header' => '<span style=" color:#1F92FF;white-space: nowrap;">Subject&nbsp;</span>',
                    'value' => '$data->to_mail->subject',
                ),
                array(
                    'name' => 'updated_at',
                    'htmlOptions' => array('width' => '15%'),
                    'header' => '<span style=" color:#1F92FF;white-space: nowrap;">Date & Time&nbsp;</span>',
                    'value' => array($this, 'convertDate'),
                ),
                array(
                    'name' => 'status',
                    'htmlOptions' => array('width' => '8%'),
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Read\') : Yii::t(\'translation\', \'Unread\')',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap; text-align:left; display:block">Action</span>',
                    'template' => '{Reply}{View}',
                    'htmlOptions' => array('width' => '10%'),
                    'buttons' => array(
                        'Reply' => array(
                            'label' => '<i class="fa fa-reply"></i>',
                            'options' => array('class' => 'action-icons', 'title' => 'Reply'),
                            'url' => 'Yii::app()->createUrl("admin/mail/reply", array("id"=>BaseClass::mgEncrypt($data->to_mail->id),"department_id" => BaseClass::mgEncrypt($data->department_id)))',
                        ),
                        'View' => array(
                            'label' => '<i class="fa fa-eye"></i>',
                            'options' => array('class' => 'action-icons', 'title' => 'View'),
                            'url' => 'Yii::app()->createUrl("admin/mail/view", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>
</form>
