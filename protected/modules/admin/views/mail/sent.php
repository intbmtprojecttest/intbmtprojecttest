<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Mail' => '/admin/mail',
    'Sent'
);
?>
<?php 
if(!empty($_GET['successMsg']) && $_GET['successMsg']=='1'){
    echo "<div class='success'>Your message sent successfully.</div>";
}
?>

    <div class="col-md-12 user-status mailBtns">
        <div class="padding0 col-md-3">  
             <?php echo CHtml::link(Yii::t('translation', 'Compose') . ' <i class="fa fa-envelope"></i>', '/admin/mail/compose', array("class" => "btn btn-xs green")); ?>
            <?php echo CHtml::link(Yii::t('translation', 'Inbox') . ' <i class="fa fa-edit"></i>', '/admin/mail', array("class" => "btn btn-xs green")); ?>
            <?php echo CHtml::link(Yii::t('translation', 'Sent'). ' <i class="fa fa-share"></i>', '/admin/mail/sent', array("class" => "btn btn-xs green")); ?>
        </div>
        <?php if (isset($emailObject)): ?>
            <div class="col-md-6">   
                <div class="expiration margin-topDefault confirmMenu">
                    <form id="regervation_filter_frm" name="regervation_filter_frm" method="post" action="/admin/mail" class="form-inline">
                        <select class="customeSelect howDidYou form-control input-medium select2me confirmBtn" id="ui-id-5" name="admin_email">
                            <option value="">Select Mail By User</option>

                            <?php foreach ($emailObject as $email) { ?>
                                <option value="<?php echo $email->id; ?>" <?php if (!empty($_POST) && $_POST['admin_email'] == $email->id) { ?> selected="selected"<?php } ?>><?php echo $email->full_name; ?></option>
                            <?php } ?>

                        </select>
                        <input type="submit" class="btn btn-primary confirmOk" value="OK" name="submit" id="submit"/>
                    </form>
                </div>
            </div>
        <?php endif; ?>
    </div>


<div class="row">
    <div class="col-md-12">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'room-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'rowCssClassExpression' => '($data->status == 1) ? "" : "info"',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">Sl.No</span>',
                ),
                array(
                    'name' => 'from_user_id',
                    'header' => '<span style="white-space: nowrap;">Sender &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->fromuser->name',
                ),
                array(
                    'name' => 'from_user_id',
                    'header' => '<span style="white-space: nowrap;">Receiver &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->touser->name',
                ),
                array(
                    'name' => 'department_id',
                    'header' => '<span style="white-space: nowrap;">Department &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->department->name) ?  $data->department->name : ""',
                ),
                array(
                    'name' => 'subject',
                    'htmlOptions' => array('width' => '30%'),
                    'header' => '<span style=" color:#1F92FF;white-space: nowrap;">Subject&nbsp;</span>',
                    'value' => '$data->to_mail()->subject',
                ),
                array(
                    'name' => 'updated_at',
                    'htmlOptions' => array('width' => '15%'),
                    'header' => '<span style=" color:#1F92FF;white-space: nowrap;">Date & Time&nbsp;</span>',
                    'value' => array($this, 'convertDate'),
                ),
                 
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{View}',
                    'header' => '<span style=" color:#1F92FF;white-space: nowrap;">Action</span>',
                    'htmlOptions' => array('width' => '5%'),
                    'buttons' => array(
                        'View' => array(
                            'label' => '<i class="fa fa-eye"></i>',
                            'options' => array('class' => 'action-icons', 'title' => 'View'),
                            'url' => 'Yii::app()->createUrl("admin/mail/view", array("id"=>BaseClass::mgEncrypt($data->id),"department_id" => BaseClass::mgEncrypt($data->department_id),"sent"=>"yes"))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>