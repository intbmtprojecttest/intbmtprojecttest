<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Member Access' => '/admin/UserHasAccess/members',
    'Permission'
);

if (!empty($error)) {
    echo "<p class=' error-2'><i class='fa fa-times-circle icon-error'></i><span class='span-error-2'>" . $error . "</span></p>";
}
if (!empty($success)) {
    echo "<p class='alert alert-success'>" . $success . "</span></p>";
}
?>
<form class="form-horizontal" role="form" id="form_admin_reservation" enctype="multipart/form-data" action="/admin/UserHasAccess/memberaccess?id=<?php echo (!empty($_GET) && $_GET['id'] != '') ? $_GET['id'] : ""; ?>" method="post" onsubmit="return validateForm()">
    <input type="hidden" id="admin_id" class="form-control" name="admin_id" readonly="readonly" value="<?php echo (!empty($_GET) && $_GET['id'] != '') ? $_GET['id'] : "0"; ?>">
    <div class="col-md-12 form-group">
        <label class="col-md-2">User Name: </label>
        <div class="col-md-6">
            <p><strong><?php echo $emailObject->name; ?></strong></p>
            <span id="first_name_error" style="color:red"></span>
        </div>
    </div>
    <div class="col-md-12 form-group">
        <!--<label class="col-md-2">Choose Permission: </label>-->
        <div class="col-md-2 checkbox">
            <input type="checkbox" name="access[]" value="dashboard" <?php if (in_array('dashboard', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Dashboard</strong><br/><br/>

            <input type="checkbox" name="access[]" value="support" <?php if (in_array('support', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Support</strong><br/><br/>
            <input type="checkbox" name="access[]" value="support_recharge_wallet" <?php if (in_array('support_recharge_wallet', $accessArr)) { ?> checked="checked" <?php } ?>>Recharge Wallet<br/><br/>
            <input type="checkbox" name="access[]" value="support_transaction_search" <?php if (in_array('support_transaction_search', $accessArr)) { ?> checked="checked" <?php } ?>>Transaction Search<br/><br/>
            <input type="checkbox" name="access[]" value="support_inactive_users" <?php if (in_array('support_inactive_users', $accessArr)) { ?> checked="checked" <?php } ?>>Inactive Users<br/><br/>
            <input type="checkbox" name="access[]" value="change_password" <?php if (in_array('change_password', $accessArr)) { ?> checked="checked" <?php } ?>>Change Password<br/><br/>
            <input type="checkbox" name="access[]" value="change_email" <?php if (in_array('change_email', $accessArr)) { ?> checked="checked" <?php } ?>>Change Email<br/><br/>
            <input type="checkbox" name="access[]" value="upline_downline" <?php if (in_array('upline_downline', $accessArr)) { ?> checked="checked" <?php } ?>>Upline/Downline<br/><br/>
            <input type="checkbox" name="access[]" value="document" <?php if (in_array('document', $accessArr)) { ?> checked="checked" <?php } ?>>Document Approval<br/><br/>
            <input type="checkbox" name="access[]" value="testimonial" <?php if (in_array('testimonial', $accessArr)) { ?> checked="checked" <?php } ?>>Testimonial Approval<br/><br/>
            <input type="checkbox" name="access[]" value="profileimage" <?php if (in_array('profileimage', $accessArr)) { ?> checked="checked" <?php } ?>>Profile Image Approval<br/><br/>

        </div>

        <div class="col-md-2 checkbox">    
            <input type="checkbox" name="access[]" value="reports" <?php if (in_array('reports', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Reports</strong><br/><br/>
            <input type="checkbox" name="access[]" value="userreport" <?php if (in_array('userreport', $accessArr)) { ?> checked="checked" <?php } ?>>Registration<br/><br/>
            <input type="checkbox" name="access[]" value="reportaddress" <?php if (in_array('reportaddress', $accessArr)) { ?> checked="checked" <?php } ?>>Member Address<br/><br/>
            <input type="checkbox" name="access[]" value="reportsponsor" <?php if (in_array('reportsponsor', $accessArr)) { ?> checked="checked" <?php } ?>>Company Sponsor Report<br/><br/>
            <input type="checkbox" name="access[]" value="reportcontact" <?php if (in_array('reportcontact', $accessArr)) { ?> checked="checked" <?php } ?>>Contact Report<br/><br/>
            <input type="checkbox" name="access[]" value="reportreferral" <?php if (in_array('reportreferral', $accessArr)) { ?> checked="checked" <?php } ?>>Referral Report<br/><br/>
            <input type="checkbox" name="access[]" value="binaryreport" <?php if (in_array('binaryreport', $accessArr)) { ?> checked="checked" <?php } ?>>Binary Report<br/><br/>
            <input type="checkbox" name="access[]" value="userloginactivities" <?php if (in_array('userloginactivities', $accessArr)) { ?> checked="checked" <?php } ?>>User Login Activities<br/><br/>
            <input type="checkbox" name="access[]" value="agentcontact" <?php if (in_array('agentcontact', $accessArr)) { ?> checked="checked" <?php } ?>>Agent Contact<br/><br/>
            <input type="checkbox" name="access[]" value="agentrequest" <?php if (in_array('agentrequest', $accessArr)) { ?> checked="checked" <?php } ?>>Agent Request<br/><br/>

            <input type="checkbox" name="access[]" value="promocode" <?php if (in_array('promocode', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Promo Code</strong><br/><br/>
            <input type="checkbox" name="access[]" value="landingpage" <?php if (in_array('landingpage', $accessArr)) { ?> checked="checked" <?php } ?>><strong>landing Page</strong><br/><br/>
            <input type="checkbox" name="access[]" value="landingpageadd" <?php if (in_array('landingpageadd', $accessArr)) { ?> checked="checked" <?php } ?>>Landing Page Add<br/><br/>

        </div>

        <div class="col-md-2 checkbox">

            <input type="checkbox" name="access[]" value="withdrawal" <?php if (in_array('withdrawal', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Withdrawal</strong><br/><br/>
            <input type="checkbox" name="access[]" value="request" <?php if (in_array('request', $accessArr)) { ?> checked="checked" <?php } ?>>Pending Request<br/><br/>
            <input type="checkbox" name="access[]" value="payment" <?php if (in_array('payment', $accessArr)) { ?> checked="checked" <?php } ?>>Pending Payment<br/><br/>
            <input type="checkbox" name="access[]" value="history" <?php if (in_array('history', $accessArr)) { ?> checked="checked" <?php } ?>>History<br/><br/>

            <input type="checkbox" name="access[]" value="mail" <?php if (in_array('mail', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Mail</strong><br/><br/>

            <input type="checkbox" name="access[]" value="user" <?php if (in_array('user', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Operation</strong><br/><br/>
            <input type="checkbox" name="access[]" value="usermg" <?php if (in_array('usermg', $accessArr)) { ?> checked="checked" <?php } ?>>Member Management<br/><br/>
            <input type="checkbox" name="access[]" value="wallet" <?php if (in_array('wallet', $accessArr)) { ?> checked="checked" <?php } ?>>Wallet<br/><br/>
            <input type="checkbox" name="access[]" value="genealogy" <?php if (in_array('genealogy', $accessArr)) { ?> checked="checked" <?php } ?>>Genealogy<br/><br/>
            <input type="checkbox" name="access[]" value="binarycommission" <?php if (in_array('binarycommission', $accessArr)) { ?> checked="checked" <?php } ?>>Generate Binary Commission<br/><br/>
            <input type="checkbox" name="access[]" value="broadcast" <?php if (in_array('broadcast', $accessArr)) { ?> checked="checked" <?php } ?>>Broadcast Message<br/><br/>
            <input type="checkbox" name="access[]" value="powerline" <?php if (in_array('powerline', $accessArr)) { ?> checked="checked" <?php } ?>>Powerline<br/><br/>
            <input type="checkbox" name="access[]" value="manageagent" <?php if (in_array('manageagent', $accessArr)) { ?> checked="checked" <?php } ?>>Manage Agent<br/><br/>
            <input type="checkbox" name="access[]" value="manageusergroup" <?php if (in_array('manageusergroup', $accessArr)) { ?> checked="checked" <?php } ?>>Manage User Group<br/><br/>
        </div>

        <div class="col-md-2 checkbox">    

            <input type="checkbox" name="access[]" value="settings" <?php if (in_array('settings', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Settings</strong><br/><br/>
            <input type="checkbox" name="access[]" value="packagesettings" <?php if (in_array('packagesettings', $accessArr)) { ?> checked="checked" <?php } ?>>Package Settings<br/><br/>
            <input type="checkbox" name="access[]" value="walletsettings" <?php if (in_array('walletsettings', $accessArr)) { ?> checked="checked" <?php } ?>>Wallet Settings<br/><br/>
            <input type="checkbox" name="access[]" value="ecurrencysettings" <?php if (in_array('ecurrencysettings', $accessArr)) { ?> checked="checked" <?php } ?>>Ecurrency Settings<br/><br/>
            <input type="checkbox" name="access[]" value="systemlist" <?php if (in_array('systemlist', $accessArr)) { ?> checked="checked" <?php } ?>>System Settings<br/><br/>

            <input type="checkbox" name="access[]" value="memberaccess" <?php if (in_array('memberaccess', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Admin Access</strong><br/><br/>

            <input type="checkbox" name="access[]" value="summary" <?php if (in_array('summary', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Summary</strong><br/><br/>
            <input type="checkbox" name="access[]" value="cashwallet" <?php if (in_array('cashwallet', $accessArr)) { ?> checked="checked" <?php } ?>>Bid wallet<br/><br/>
            <input type="checkbox" name="access[]" value="cashwallet" <?php if (in_array('cashwallet', $accessArr)) { ?> checked="checked" <?php } ?>>CashBack Wallet<br/><br/>

            <input type="checkbox" name="access[]" value="product" <?php if (in_array('product', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Product</strong><br/><br/>
            <input type="checkbox" name="access[]" value="product_list" <?php if (in_array('product_list', $accessArr)) { ?> checked="checked" <?php } ?>>Product List<br/><br/>
            <input type="checkbox" name="access[]" value="product_add" <?php if (in_array('product_add', $accessArr)) { ?> checked="checked" <?php } ?>>Add Product<br/><br/>
        </div>

        <div class="col-md-2 checkbox">    
            <input type="checkbox" name="access[]" value="financereports" <?php if (in_array('financereports', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Finance Reports</strong><br/><br/>
            <input type="checkbox" name="access[]" value="reporttransaction" <?php if (in_array('reporttransaction', $accessArr)) { ?> checked="checked" <?php } ?>>Transaction Report<br/><br/>
            <input type="checkbox" name="access[]" value="packagepurchased" <?php if (in_array('packagepurchased', $accessArr)) { ?> checked="checked" <?php } ?>>Order List<br/><br/>
            <input type="checkbox" name="access[]" value="generatebinaryreport" <?php if (in_array('generatebinaryreport', $accessArr)) { ?> checked="checked" <?php } ?>>Generated Binary<br/><br/>
            <input type="checkbox" name="access[]" value="downlinereport" <?php if (in_array('downlinereport', $accessArr)) { ?> checked="checked" <?php } ?>>Down Line Purchase Report<br/><br/>
            <input type="checkbox" name="access[]" value="offlinepayment" <?php if (in_array('offlinepayment', $accessArr)) { ?> checked="checked" <?php } ?>>Offline Payment Report<br/><br/>
            <input type="checkbox" name="access[]" value="walletsummary" <?php if (in_array('walletsummary', $accessArr)) { ?> checked="checked" <?php } ?>>Wallet Summary<br/><br/>
            <input type="checkbox" name="access[]" value="profitability" <?php if (in_array('profitability', $accessArr)) { ?> checked="checked" <?php } ?>>Profitability Report<br/><br/>

            <input type="checkbox" name="access[]" value="auction" <?php if (in_array('auction', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Auction</strong><br/><br/>
            <input type="checkbox" name="access[]" value="add" <?php if (in_array('add', $accessArr)) { ?> checked="checked" <?php } ?>>Auction Add<br/><br/>
            <input type="checkbox" name="access[]" value="list" <?php if (in_array('list', $accessArr)) { ?> checked="checked" <?php } ?>>Auction List<br/><br/>
            <input type="checkbox" name="access[]" value="declarewinner" <?php if (in_array('declarewinner', $accessArr)) { ?> checked="checked" <?php } ?>>Declare Winner<br/><br/>
            <input type="checkbox" name="access[]" value="winnerpayment" <?php if (in_array('winnerpayment', $accessArr)) { ?> checked="checked" <?php } ?>>Winner Payment<br/><br/>
        </div>

        <div class="col-md-2 checkbox">
            <input type="checkbox" name="access[]" value="cms" <?php if (in_array('cms', $accessArr)) { ?> checked="checked" <?php } ?>><strong>CMS</strong><br/><br/>
            <input type="checkbox" name="access[]" value="news" <?php if (in_array('news', $accessArr)) { ?> checked="checked" <?php } ?>>News<br/><br/>
            <input type="checkbox" name="access[]" value="faq" <?php if (in_array('faq', $accessArr)) { ?> checked="checked" <?php } ?>>FAQ<br/><br/>
            <input type="checkbox" name="access[]" value="popup" <?php if (in_array('popup', $accessArr)) { ?> checked="checked" <?php } ?>>Popup<br/><br/>
            <input type="checkbox" name="access[]" value="homebanner" <?php if (in_array('homebanner', $accessArr)) { ?> checked="checked" <?php } ?>>Home Banner<br/><br/>

            <input type="checkbox" name="access[]" value="library" <?php if (in_array('library', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Resource Library</strong><br/><br/>
            <input type="checkbox" name="access[]" value="library_category" <?php if (in_array('library_category', $accessArr)) { ?> checked="checked" <?php } ?>>Manage Category<br/><br/>
            <input type="checkbox" name="access[]" value="library_media" <?php if (in_array('library_media', $accessArr)) { ?> checked="checked" <?php } ?>>Manage Media<br/><br/>

            <input type="checkbox" name="access[]" value="bids" <?php if (in_array('bids', $accessArr)) { ?> checked="checked" <?php } ?>><strong>Bids</strong><br/><br/>
            <input type="checkbox" name="access[]" value="bidsummary" <?php if (in_array('bidsummary', $accessArr)) { ?> checked="checked" <?php } ?>>Bid Summary<br/><br/>
            <input type="checkbox" name="access[]" value="bidsdetail" <?php if (in_array('bidsdetail', $accessArr)) { ?> checked="checked" <?php } ?>>Bids Detail Report<br/><br/>
        </div>

        <div class="col-md-12 form-group">
            <label class="col-md-2"></label>
            <div class="col-md-12">
                <input type="submit" class="btn orange" name="submit" id="submit" size="60" maxlength="75" class="textBox" value="Submit" />
            </div>
        </div> 
    </div> 
</form>