

<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Member Access' => '/admin/UserHasAccess/members',
    'Edit User',
);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"> </script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" type="text/css" rel="stylesheet">
    
<div class="col-md-7 col-sm-7">
    <?php if (Yii::app()->user->hasFlash('error')){ ?>
        <p class="error-2" id="error_msg"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php echo Yii::app()->user->getFlash('error'); ?></span></p>
    <?php } ?>

    <?php if (Yii::app()->user->hasFlash('success')){ ?>
        <p class="success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php echo Yii::app()->user->getFlash('success'); ?></span></p>
    <?php } ?>
        
    <form action="/admin/UserHasAccess/edit?id=<?php echo $_REQUEST['id']; ?>" method="post" class="form-horizontal" onsubmit="return validateMemberAccess();">
     <input type="hidden" id="nameExistedErrorFlag" name="nameExistedErrorFlag" class="input-text full-width" value="0"/>
     <input type="hidden" id="emailExistedErrorFlag" name="emailExistedErrorFlag" class="input-text full-width" value="0"/>
        <div class=" user-has-access portlet box toe-blue top10">
            <div class="portlet-title">
                    <div class="caption">
                     Edit User
                    </div>
                </div>
            <div class="portlet-body form margin-top-10">
            <div class="form-group">
                <label for="country" class="col-lg-4 control-label">Department</label>
                <div class="col-lg-8 multiSelect">
                    <select name="department[]" class="selectpicker full-width" multiple title="Choose one of the following...">
                        <option value="">Select Department</option>
                            <?php
                            if(isset($departmentObject) && !empty($departmentObject)){
                            foreach ($departmentObject as $department) { ?>
                                <option value="<?php echo $department->id; ?>"
                                  <?php 
                                   if(isset($usersDepartmentObject) && !empty($usersDepartmentObject)){
                                    foreach ($usersDepartmentObject as $usersDepartment) { 
                                        if($usersDepartment->department_id == $department->id) echo "selected";
                                    } }
                                  ?>>
                                    <?php echo ucwords($department->name); ?>
                                </option>
                            <?php } } ?>
                     </select>

                    <span id="country_id_error" class="clrred"></span></div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Name<span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" id="name" class="form-control" name="name" value="<?php echo (!empty($userObject)) ? $userObject->name : ""; ?>" maxlength="15">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Email<span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" id="email" class="form-control" name="email" value="<?php echo (!empty($userObject)) ? $userObject->email : ""; ?>" >
                    <div style="color:red;" id="email_error"></div>
                </div>
            </div>

               <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Password</label>
                <div class="col-lg-8">
                    <input type="password" id="zip_code" class="form-control" name="password" id="password">
                    <div style="color:red;" id="error_password"> </div> 
                </div>
            </div>
                        
                    <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname"></label>
                <div class="col-lg-8">
                   <input type="submit" class="btn green pull-right" name="submit" id="submit"  value="Submit">
                </div>
            </div>              
            
       


</div>
</div>
    </form>
</div>



<script>
    $('.selectpicker').selectpicker();
</script>