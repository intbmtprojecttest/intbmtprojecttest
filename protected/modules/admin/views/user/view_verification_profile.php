<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Operation' => '/admin/user/index',
    'View Profile',
);
$userObject = $userProfileObject->user();
?>

<!-- Profile Info block starts -->
<div class="col-md-6 col-sm-6">
    <?php if (isset($_GET['errorMsg1']) && !empty($_GET['errorMsg1'])) { ?><p class="error-2" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php echo $_GET['errorMsg1']; ?></span></p><?php } ?>

    <?php if (isset($_GET['successMsg1']) && !empty($_GET['successMsg1'])) { ?><p class="success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php echo $_GET['successMsg1']; ?></span></p><?php } ?>
    <div class="portlet box toe-blue ">
        <div class="portlet-title">
            <div class="caption">
                View Profile
            </div>
        </div>
        <div class="portlet-body form">
            <form action="#" method="post" class="form-horizontal">
                <fieldset>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Sponsor ID</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" value="<?php echo (isset($userProfileObject)) ? $userObject->sponsor->name : ""; ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Name</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" value="<?php echo (isset($userProfileObject)) ? $userObject->name : ""; ?>" readonly="readonly">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Position</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" value="<?php echo (isset($userProfileObject)) ? $userObject->position : ""; ?>" readonly="readonly">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Full Name<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" value="<?php echo (isset($userProfileObject)) ? $userObject->full_name : ""; ?>" readonly="readonly">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Email<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" value="<?php echo (!empty($userProfileObject)) ? $userObject->email : ""; ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="country" class="col-lg-4 control-label">Country <span class="require">*</span></label>
                            <div class="col-lg-7">
                                    <input type="text" class="form-control"  value="<?php echo isset($userProfileObject->country()->name)?$userProfileObject->country()->name:""; ?>" readonly="readonly">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Phone<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" value="<?php echo (!empty($userProfileObject)) ? $userProfileObject->country_code : ""; ?>" readonly="readonly" style="width:20%;float:left;" class="form-control" name="UserProfile[country_code]" id="country_code">&nbsp;&nbsp;
                                <input type="text" id="phone" class="form-control" value="<?php echo (!empty($userProfileObject)) ? $userProfileObject->phone : ""; ?>" style="width:75%;float:right;" maxlength="30" readonly="readonly">
                                
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Date of Birth<span class="require">*</span></label>
                            <div class="col-lg-7">                  
                                <input type="text" class="datepicker form-control"  value="<?php echo (!empty($userProfileObject)) ? $userProfileObject->date_of_birth : ""; ?>" readonly="readonly">  
                                
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Skype ID</label>
                            <div class="col-lg-7">
                                <input type="text" placeholder="Ex: example12" class="form-control" value="<?php echo (!empty($userProfileObject)) ? $userProfileObject->skype_id : ""; ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Facebook ID</label>
                            <div class="col-lg-7">
                                <input type="text" placeholder="Ex: http://facebook.com" class="form-control" value="<?php echo (!empty($userProfileObject)) ? $userProfileObject->facebook_id : ""; ?>" readonly="readonly">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Twitter ID</label>
                            <div class="col-lg-7">
                                <input type="text"  placeholder="Ex: http://twitter.com" class="form-control" value="<?php echo (!empty($userProfileObject)) ? $userProfileObject->twitter_id : ""; ?>" readonly="readonly">
                                
                            </div>
                        </div>
        
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Password<span class="require"></span></label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" value="<?php echo (!empty($userObject->password)) ? $userObject->password : ""; ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Master Pin<span class="require"></span></label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" value="<?php echo (!empty($userObject->master_pin)) ? $userObject->master_pin : ""; ?>" readonly="readonly">
                            </div>
                            <input type="hidden" id="UID1" name="uid" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>"/>
                        </div>
                    </div>
                </fieldset>
                <input type="hidden" id="emailExistedErrorFlag" name="emailExistedErrorFlag" value="0"/>

            </form>
        </div>
    </div>
</div>

<!-- Profile Info block ends -->

<!-- Address block starts-->
<div class="col-sm-6 col-md-6">

    <div class="error" id="error_msg" style="display: none;"></div>
    <?php if (isset($_GET['errorMsg']) && !empty($_GET['errorMsg'])) { ?><p class="error-2" id="error_msg"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php echo $_GET['errorMsg']; ?></span></p><?php } ?>

    <?php if (isset($_GET['successMsg']) && !empty($_GET['successMsg'])) { ?><p class="success-2" id="error_msg"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php echo $_GET['successMsg']; ?></span></p><?php } ?>
    <div class="portlet box  toe-blue ">
        <div class="portlet-title">
            <div class="caption">
                Address
            </div>
        </div>
        <div class="portlet-body form">
            <form action="#" method="post" class="form-horizontal">

                <fieldset>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="firstname">Address <span class="require">*</span></label>
                            <div class="col-lg-7">
                                <textarea class="form-control" readonly="readonly"><?php echo (!empty($userProfileObject->address)) ? trim($userProfileObject->address) : ""; ?></textarea>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="Street">Street<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="street" class="form-control" value="<?php echo (!empty($userProfileObject->street)) ? trim($userProfileObject->street) : ""; ?>" readonly="readonly">
                                
                            </div>
                        </div>
                        <div class="form-group" id="stateList" >
                            <label class="col-lg-4 control-label" for="State">State <span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="state_id" class="form-control" value="<?php echo (!empty($userProfileObject->state_name)) ? trim($userProfileObject->state_name) : ""; ?>" readonly="readonly">
                                
                            </div>
                        </div>

                        <div class="form-group" id="cityList">
                            <label class="col-lg-4 control-label" for="email">City <span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="city_id" class="form-control" value="<?php echo (!empty($userProfileObject->city_name)) ? $userProfileObject->city_name : ""; ?>" readonly="readonly">
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="Zipcode">Zip Code<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="zip_code" class="form-control" value="<?php echo (!empty($userProfileObject)) ? $userProfileObject->zip_code : ""; ?>" readonly="readonly">
                                
                            </div>
                        </div>
                    </div>
                </fieldset>
                <input type="hidden" id="UID" name="uid" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>"/>


            </form>
        </div>
    </div>
</div>

