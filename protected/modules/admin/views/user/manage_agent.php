<?php
Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Operation' => '/admin/user',
    'Manage Agent'
);
?>
<input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">

<div class="user-status margin-bottom-10 filter-toggle">
    <form class="form-horizontal" id="form_admin_reservation" action="#" method="post" onsubmit="return getAgentList();">
        <input type="hidden" name="userIdList" id="userIdList" value="" />
        <div class="col-md-4 form-group">
            <label class="col-md-5 control-label">User Name: *</label>
            <div class="col-md-7">
                <input type="text" class="form-control dvalid" onchange="getFullName(this.value);" name="name" id="username" />
                <span id="search_fullname"></span>
                <span id="user_name_valid"></span>
                <span id="user_name_invalid" class="alert-danger"></span>
                <span style="color:red"  id="user_name_error"></span>
            </div>
        </div>

        <div class="col-md-4 form-group">
            <label class="col-md-5 control-label">Country Name:</label>
            <div class="col-md-7">
                <select name="country_id" id="country_id" class="form-control">
                    <?php foreach ($countryObject as $country) { 
                        echo '<option value="'.$country->id.'">'.$country->name.'</option>';
                    } ?>
                </select>
            </div>
        </div>

        <div class="col-md-4 form-group">
            <div class="col-md-5">
                <button type="submit" class="btn orange pull-right textBox">Add Agent</button>
            </div>
        </div> 
    </form>
</div>

<form id="new_user_filter_frm" name="user_filter_frm" method="post" action="/admin/user" >
    <div class="row">
        <div class="col-md-12 responsiveTable">
            <?php
            if (Yii::app()->user->hasFlash('error')):
                echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
            endif;
            if (Yii::app()->user->hasFlash('success')):
                echo '<div class="alert alert-success">' . Yii::app()->user->getFlash('success') . '</div>';
            endif;
            ?>
            <table id="allMemberList" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Country Name</th>
                        <th>Phone</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
            
            <script>
            $(document).ready(function () {
                    oTable = $('#allMemberList').DataTable({
//                        "sDom":'lfptip',
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        "pagingType": "full_numbers",
                        "order": [[5, "desc"]],
                        "aoColumnDefs": [ {
                            'bSortable' : false,
                            'aTargets' : [ 7 ]
                        } ],
                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                        "buttons": [ 'csv'],
                        "ajax": {
                            "url": "manageagentdata",
                            "type": "POST"
                        },
                        "columns": [
                            {"data": "name"},
                            {"data": "full_name"},
                            {"data": "email"},
                            {"data": "countryName"},
                            {"data": "phoneNuber"},
                            {"data": "created_at"},
                            {"data": "userStatus"},
                            {
                                data: "id",
                                render: function (data, type, row) { 
                                        return '<a href="changeagentstatus?id='+data+'"><i class="fa fa-retweet"></i></a>';
                                },
                                className: "dt-body-center"
                            },

                        ],
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)' // no row selection on last column
                        },
                    });
                });
            
            $("#btnSearch").click(function() {
                if($("#toDate").val()>$("#fromDate").val()){
                    alert("To Date should be greater then From Date!!!");
                    return false;
                }
                oTable.columns(5).search($("#toDate").val().trim(),$("#fromDate").val().trim());
                oTable.draw();
            });
            
            function getAgentList(){
                $("#user_name_error").html("");
                var username = $("#username").val();
                var countryId = $("#country_id").val();

                if(username == "" || username.trim() == ""){ 
                    $("#user_name_error").html("Username should not be blank");    
                    return false;
                }
                
                $.ajax({
                    type: "post",
                    url: "/admin/user/checkingagent",
                    data: {'userName': username , 'countryId' : countryId},
                    success: function (responseData) {
                        $("#user_name_invalid").html('');
                        var responseData = jQuery.parseJSON(responseData);
                        if (responseData.validId != "") {
                            $("#userIdList").val(responseData.validId);
                            $.ajax({
                                type: "post",
                                url: "/admin/user/agentadd",
                                data: {'validUser': responseData.validId , 'country_id' : $("#country_id").val()},
                                success: function (responseData) {
                                    $("#user_name_invalid").html('');
                                    if (responseData == 1) {
                                        alert('Agent Added Successfully.');
                                    }else {
                                        alert('Some Thing went wrong.')
                                    }
                                    location.reload();
                                }
                            });
                            
                        }else if (responseData.invalid != "") {
                            $("#user_name_invalid").html('Invalid User.');
                            $("#user_name_error").html("");
                        }else if (responseData.already != "") {
                            $("#user_name_invalid").html('User already an agent.' );
                            $("#user_name_error").html("");
                        }
                    }
                });
                
                return false;
            }
        </script>
        </div>
    </div>
</form>
<script src="/js/datatables.min.js"></script>
<script src="/js/dataTables.buttons.min.js"></script>
<script src="/js/jszip.min.js"></script>
<script src="/js/buttons.html5.min.js"></script>
<script src="/js/buttons.print.min.js"></script>