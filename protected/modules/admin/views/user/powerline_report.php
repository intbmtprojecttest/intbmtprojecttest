<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Operation' => '/admin/user',
    'Powerline Report'
);
?>
<style>
    .table td, .table td a{word-break: break-all;}
    .no-pad {
        padding-left: 0;
        padding-right: 0;
    }
    .dataTables_length {
        margin-top: 9px;
        margin-left: 35px;
    }

</style>
<a class="btn green margin-bottom-10" href="/admin/user/powerline">Create Powerline</a>
<div class="col-md-12 order-list-div margin-bottom-15">  
    <?php if (isset($success)) { ?><?php echo $success; ?><?php } ?>
    <div class="expiration confirmMenu">

        <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/user/powerlinereport">
            <div class="col-md-4 col-sm-6 top7">
                <div class="input-group input-large date-picker input-daterange">
                    <input type="text" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from']) && $_GET['from'] != '') ? $_GET['from'] : date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 15, date("Y"))); ?>">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to']) && $_GET['to'] != '') ? $_GET['to'] : DATE('Y-m-d'); ?>">
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="dataTables_length" id="search_length">
                    <label>&nbsp;</label>
                    <label>
                        <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                                <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                                    <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                                <?php } ?>
                        </select>&nbsp; 
                        Records per page</label>
                    <input type="submit" class="btn btn-success confirmOk pull-right" value="OK" name="submit" id="submit"/>     
                </div>
            </div> 
        </form>
    </div>
</div> 

<div class="row">
    <div class="col-md-12">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            //'summaryText' => 'Showing {start} to {end} of {count} entries',
            //'template' => '<div class="table-responsive">{items}</div> {summary} {pager}',
            'template' => "{pager}\n{items}\n{summary}\n{pager}", //THIS DOES WHAT YOU WANT    
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                    'name' => 'transaction_id',
                    'header' => '<span style="white-space: nowrap;">Txn Id &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->transaction_id)?$data->transaction_id:""',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Date  &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">To User&nbsp; &nbsp;  &nbsp;</span>',
                    'value' => '$data->user()->name',
                ),
                array(
                    'name' => 'actual_amount',
                    'header' => '<span style="white-space: nowrap;">Amount &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->actual_amount)?$data->actual_amount:""',
                ),
                
            ),
        ));
        ?>
    </div>
</div>