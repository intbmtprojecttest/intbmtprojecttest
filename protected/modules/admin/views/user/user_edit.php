<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Operation' => '/admin/user',
    'Edit User',
);
$redirectListUrl = $_SERVER['HTTP_REFERER'];

?>
<?php if ($error) { ?><div class="error" id="error_msg"><?php echo $error; ?></div><?php } ?>
<div class="col-md-6 col-sm-5 portlet box toe-blue">
    <div class="portlet-title">
                    <div class="caption">
                    Edit User
                    </div>
                </div>
    
    <div class="portlet-body form padding15">
    <form action="/admin/user/edit?id=<?php echo $_REQUEST['id']; ?>" method="post" class="form-horizontal" onsubmit="return validateUserAddFrm();">

        <fieldset>
        
            <div class="form-group">
                <label class="col-lg-4 control-label" for="name">Name<span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" id="name" class="form-control" name="UserProfile[name]" value="<?php echo (!empty($userObject)) ? $userObject->name : ""; ?>" readonly="readonly" maxlength="8">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label" for="fullname">Full Name<span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" id="full_name" class="form-control" name="UserProfile[full_name]" value="<?php echo (!empty($userObject)) ? $userObject->full_name : ""; ?>" maxlength="15">
                    <div style="color:red;" id="full_name_error"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label" for="email">Email<span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" id="email" class="form-control" name="UserProfile[email]" value="<?php echo (!empty($userObject)) ? $userObject->email : ""; ?>" >
                    <div style="color:red;" id="email_error"></div>
                </div>
            </div>

            <div class="form-group">
                <label for="country" class="col-lg-4 control-label">Country <span class="require">*</span></label>
                <div class="col-lg-8">
                    <select name="UserProfile[country_id]" id="country_id" onchange="getCountryCode(this.value)" class="form-control">
                        <option value="">Select Country</option>
                        <?php foreach ($countryObject as $country) { ?>
                            <option value="<?php echo $country->id; ?>" <?php
                            if (!empty($userObject)) {
                                if (!empty($profileObject) && $country->id == $profileObject->country_id) {
                                    ?>selected="selected<?php
                                        }
                                    }
                                    ?>"><?php echo $country->name; ?></option>
                                <?php } ?>
                    </select>
                    <div style="color:red;" id="country_id_error"></div> 
                </div>

            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label" for="phone">Phone</label>
                <div class="col-lg-8">
                    <input type="text" id="country_code" name="UserProfile[country_code]" value="<?php echo (!empty($profileObject->country_code)) ? $profileObject->country_code : ""; ?>" readonly="readonly" style="width:31%;float:left;" class="form-control">&nbsp;&nbsp;
                    <input type="text" id="phone" class="form-control" name="UserProfile[phone]" value="<?php echo (!empty($profileObject->phone)) ? $profileObject->phone : ""; ?>"  style="width:65%;float:right;">
                    <div  style="color:red;" id="phone_error"> </div> 
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label" for="fbid">Facebook ID</label>
                <div class="col-lg-8">
                    <input type="text" id="facebook_id" class="form-control" name="UserProfile[facebook_id]" value="<?php echo (!empty($profileObject->facebook_id)) ? $profileObject->facebook_id : ""; ?>">
                    <span class="example">Ex: http://facebook.com</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label" for="address">Full / Postal Address </label>
                <div class="col-lg-8">
                    <textarea id="address" name="UserProfile[address]" class="form-control" ><?php echo (!empty($profileObject)) ? $profileObject->address : ""; ?></textarea>
                    <div  style="color:red;" id="error_msg_address"> </div> 
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label" for="zipcode">Zip Code</label>
                <div class="col-lg-8">
                    <input type="text" id="zip_code" class="form-control" name="UserProfile[zip_code]" value="<?php echo (!empty($profileObject)) ? $profileObject->zip_code : ""; ?>">
                    <div style="color:red;" id="error_msg_zip"> </div> 
                </div>
            </div>
        </fieldset>
        <?php 
        
        $urlRedirect = explode("?",$redirectListUrl);
        if(count($urlRedirect)==1) { ?>
        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Submit" class="btn mav-blue-btn">

            </div>
        </div>
        <?php } ?>
    </form>
        </div>
</div>



<!--<div class="col-md-6 col-sm-7">
    <div class=" portlet box toe-blue">
 <div class="portlet-title">
                    <div class="caption">
                Update Password
                    </div>
                </div>
 <div class="portlet-body form padding15">
    <form action="/admin/user/updatepassword?id=<?php echo $_REQUEST['id']; ?>" method="post" class="form-horizontal">

        <fieldset>

            <div class="form-group">
                <label for="position" class="col-lg-4 control-label">Encryption Type</label>
                <div class="col-lg-8 radioProfile">        
                    <input type="radio" name="update_type" id="update_type" value="encrypt" checked="checked"/>
                    <label for="update_type">Encrypt</label>

                    <input type="radio" name="update_type" id="update_type1" value="decrypt"  />
                    <label for="update_type1">Decrypt</label>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>
                <div class="col-md-8">
                    <input type="text" id="user_password" class="form-control" name="UserProfile[password]" value="<?php echo (!empty($userObject)) ? $userObject->password : ""; ?>">
                    <div id="user_password_error" class="form_error"></div>
                </div>
            </div>
        </fieldset>
        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Update Password" class="btn mav-blue-btn">

            </div>
        </div>
    </form>
</div>
</div>
    <div class=" portlet box toe-blue">
 <div class="portlet-title">
                    <div class="caption">
             Update Master Pin
                    </div>
                </div>
     <div class="portlet-body form padding15">
    <form action="/admin/user/updatemasterpin?id=<?php echo $_REQUEST['id']; ?>" method="post" class="form-horizontal">

        <fieldset>

            <div class="form-group">
                <label for="position" class="col-lg-4 control-label">Encryption Type</label>
                <div class="col-lg-8 radioProfile">        
                    <input type="radio" name="update_type" id="update_type" value="encrypt" checked="checked"/>
                    <label for="update_type">Encrypt</label>

                    <input type="radio" name="update_type" id="update_type1" value="decrypt"  />
                    <label for="update_type1">Decrypt</label>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-4 control-label" for="masterpin">Master Pin</label>
                <div class="col-md-8">
                    <input type="text" id="user_master_pin" class="form-control" name="UserProfile[master_pin]" value="<?php echo (!empty($userObject)) ? $userObject->master_pin : ""; ?>">
                    <div id="user_master_pin_error" class="form_error"></div>
                </div>
            </div>

        </fieldset>
        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Update Master Pin" class="btn mav-blue-btn">

            </div>
        </div>
    </form>
</div>
</div>
</div>-->


