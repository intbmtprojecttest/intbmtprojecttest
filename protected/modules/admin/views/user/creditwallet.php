<?php 

if(empty($_GET)) {
    $this->breadcrumbs = array(
        'Dashboard' => '/admin/default/dashboard',
        'Operation' => '/admin/user/index',
        'Recharge Wallet'
    ); 
} else {
    $this->breadcrumbs = array(
        (isset($_GET['mode']) && $_GET['mode'] == Transaction::$_MODE_ADDFUND)?'Support':'Operation',
        'Recharge Wallet'
    );
}

$mailObject = array();
if (!empty($error)) {
    echo "<p class='error'>" . $error . "</p>";
}

$redirectUrl = "/admin/user/wallet";

if(isset($_GET['mode']) && $_GET['mode'] == Transaction::$_MODE_ADDFUND) {
    $redirectUrl = "";
}

if (Yii::app()->user->hasFlash('success')): 
    echo '<div class="alert alert-success">'.Yii::app()->user->getFlash('success') .'</div>';
endif;

?>
 <div class="col-md-8 portlet creditWallet box toe-blue">
        <div class="portlet-title">
            <div class="caption">
                Recharge
            </div>
        </div>
     <div class="portlet-body form padding15 f-left">
    <form class="form-horizontal" role="form" id="form_admin_reservation" enctype="multipart/form-data" action="<?php (!empty($_GET['id'])) ? "/admin/user/creditwallet?id=".$_GET['id']."&type=".$_GET['type'] : "/admin/user/creditwallet" ?>" method="post" onsubmit="return validateCredit()">
        
        <input type="hidden" name="cancelled" id="cancelled" value=""/>
        <input type="hidden" name="mode" id="mode" value="<?php echo (isset($_GET['mode']) && $_GET['mode']=Transaction::$_MODE_ADDFUND)?Transaction::$_MODE_ADDFUND:""; ?>"/>
        
        <input type="hidden" name="userId" id="search_user_id" value="<?php echo (!empty($userObject)) ? $userObject->id : ""; ?>"/>
        <?php $walletList = BaseClass::getWalletList();
        if (empty($_GET) || isset($_GET['mode']) && $_GET['mode']= Transaction::$_MODE_ADDFUND) {?>
            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">User Name: </label>
                <div class="col-md-7">
                    <input type="text" class="form-control dvalid" name="name"  onchange="getFullName(this.value);" id="search_username" />
                    <span id="search_fullname">&nbsp;</span>
                    <span style="color:red"  id="search_user_error"></span>
                </div>
            </div>
            
            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">Wallet Type: *</label>
                <div class="col-md-7">
                    <select name="towallettype" id="wallet_type" class="form-control dvalid" onchange="getExistingFund(<?php echo Yii::app()->session['userid']; ?>,this.value,'subadmin');">
                        <?php foreach ($walletList as $key => $value) { ?>
                            <option value="<?php echo $value->id; ?>" <?php echo (!empty($_GET['type']) && BaseClass::mgDecrypt($_GET['type']) == $value->id) ? "selected" : "" ?> ><?php echo $value->name; ?></option>
                        <?php } ?>
                    </select>
                    <span style="color:red"  id="wallet_error"></span>
                </div>
            </div>
            <input type="hidden" id="transaction_data_amt" value="<?php echo (!empty($walletObj))? $walletObj->fund : '' ?>">
        
        <?php } else { ?>
            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">User Name: *</label>
                <div class="col-md-7">
                    <p><?php echo (!empty($userObject)) ? $userObject->name : ""; ?></p>
                    <span style="color:red"  id="first_name_error"></span>
                </div>
            </div>
        
            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">Wallet Type: *</label>
                <div class="col-md-7">
                    <select name="towallettype" id="wallet_type" class="form-control dvalid" onclick="getWalletInfo(this.value)">
                        <?php foreach ($walletList as $key => $value) { ?>
                            <option value="<?php echo $value->id; ?>" <?php echo (!empty($_GET['type']) && BaseClass::mgDecrypt($_GET['type']) == $value->id) ? "selected" : "" ?> ><?php echo $value->name; ?></option>
                        <?php } ?>
                    </select>
                    <span style="color:red"  id="wallet_error"></span>
                </div>
            </div>
            <input type="hidden" id="transaction_data_amt" value="<?php echo (!empty($walletObj) && ($walletObj->type == BaseClass::mgDecrypt($_GET['type'])))? $walletObj->fund : '' ?>">

        <?php } ?>
        

        <div class="col-md-12 form-group">
            <label class="col-md-3 control-label">Add Fund: *</label>
            <div class="col-md-7">
                <input type="text" class="form-control dvalid" name="paid_amount" id="fund" maxlength="5" onkeypress="javascript:return isNumberDecimal(event)"  size="60" maxlength="75" value="<?php echo (!empty($walletObject)) ? $walletObject->touser->email : ""; ?>" />
                <span style="color:red"  id="fund_error"></span>
            </div>
        </div>
        
        <div class="payment_capture">
            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">Gateway Id: *</label>
                <div class="col-md-7">
                    <select name="gateway_id" id="gateway_id" class="form-control" onchange="getPaymentType(this.value)">
                        <?php foreach ($gatewayObject as $gateway) { ?>
                            <option value="<?php echo $gateway->id; ?>" ><?php echo $gateway->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-md-12 form-group payref">
                <label class="col-md-3 control-label">Payment Reference: </label>
                <div class="col-md-7">
                    <input type="text" class="form-control dvalid" name="payment_transaction" id="payment_transaction" maxlength="20" value="<?php echo (!empty($walletObject)) ? $walletObject->touser->email : ""; ?>" />
                    <span style="color:red"  id="payment_transaction_error"></span>
                </div>
            </div>
        </div>
        
        <div class="col-md-12 form-group">
            <label class="col-md-3 control-label">Comments: </label>
            <div class="col-md-7">
                <textarea class="form-control" name="comment" rows="10" cols="10"></textarea>
                <span style="color:red"  id="fund_error"></span>
            </div>
        </div>
        <div class="col-md-12 form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-7">
                <input type="submit" class="btn mav-blue-btn" name="submit" id="submit" size="60" maxlength="75" class="textBox" value="Submit" />
                <input type="button" class="btn mav-blue-btn pull-right" name="cancel" onclick="location.href='<?php echo $redirectUrl; ?>'" value="Cancel" />
                <div id="loading2" style="display:none;" class="loader">Don't click back button or refresh page...your transaction is in process</div>
            </div>
        </div> 
    </div>
</form>
 </div>
<input type="hidden" value="<?php if (!empty($_GET['id'])) { echo $_GET['id']; } ?>" id="getId">
<input type="hidden" value="<?php if (!empty($_GET['type'])) { echo BaseClass::mgDecrypt($_GET['type']); }else{ echo 1; } ?>" id="type">
<input type="hidden" value="<?php echo ($userObject) ? $userObject->role_id : "" ; ?>" id="role_id" class="role_id">

<script>
    jQuery(document).ready(function () {
        var getType = $("#type").val();
        var roleId = $("#role_id").val();
        $(".payment_capture").show();
        if(getType == 1 || getType == 2 || roleId == 2 ){
           $(".payment_capture").hide();
       }
    });
    
    function getWalletInfo(id) {
        $(".payment_capture").show();
        var roleId = $("#role_id").val();
        if(id == 1 || id == 2 || id == 3 || roleId == 2){
           $(".payment_capture").hide();
       }
    }
    
    function getPaymentType(id) {
       $(".payref").show();
        if(id == 8 ){
           $(".payref").hide();
       }
    }
</script>