<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Reports' => '/admin/report/',
    'Agent Request'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

?>
<script src="/js/datatables.min.js"></script>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
<div class="user-status col-md-12 margin-bottom-10 filter-toggle">
    <form id="profitability_filter_frm"  method="post" action="" >
        <div class="col-md-4">    
            <span id="date_error" style="color:red"></span>
            <div class="input-group input-large date-picker input-daterange pull-left">
                <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                <span class="input-group-addon"> to </span>
                <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
            </div>
        </div>
        
        <div class="col-md-1 col-sm-6 form-inline">
            <input type="button" class="btn btn-success margintop3 mobile-clear" value="Filter" name="contact" id="contact"/>
        </div>
    </form>
</div>
    <div class="row">
        <div class="col-md-12 responsiveTable">
            <table id="agentcontact" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                       <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Country Name</th>
                        <th>Phone</th>
                        <th>Type</th>
                        <th>Subject</th>
                        <th>facebook</th>
                        <th>Skype</th>
                        <th>Req.Ip</th>
                        <th>Req.Country</th>
                        <th>Create Date</th>
                        
                    </tr>
                </thead>
            </table>
            
<script>
$(document).ready(function () {
    oTable = $('#agentcontact').DataTable({
        "responsive": true,
        "iDisplayLength": 50,
        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
        "processing": true,
        "serverSide": true,
        "paging": true,
        "bSort": true,
        "pagingType": "full_numbers",
        "order": [[10, "desc"]],
        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
        "buttons": [
                'copy', 'csv', 'excel', 'print'
            ],

        "ajax": {
            "url": "agentrequestdata",
            "type": "POST"
        },
        "columns": [
            {"data": "name"},
            {"data": "email"},
            {"data": "cname"},
            {"data": "phoneNuber"},
            {"data": "type"},
            {"data": "subject"},
            {"data": "facebook_id"},
            {"data": "skyp_id"},
            {"data": "actual_ip"},
            {"data": "actual_country"},
            {"data": "updated_at"},
        ],
    });
});

$("#contact").click(function() {
    $("#date_error").html("");
    if($("#toDate").val()>$("#fromDate").val()){
        $("#date_error").html("To Date should be greater then From Date!!!");
        $("#fromDate").focus();
        return false;
    }
oTable.columns(2).search($("#toDate").val().trim(),$("#fromDate").val().trim());
oTable.draw();
});
</script>
        </div>
    </div>
<link href="/css/buttons.dataTables.min.css" rel="stylesheet">
<script src="/js/dataTables.buttons.min.js"></script>
<script src="/js/jszip.min.js"></script>
<script src="/js/buttons.html5.min.js"></script>
<script src="/js/buttons.print.min.js"></script>