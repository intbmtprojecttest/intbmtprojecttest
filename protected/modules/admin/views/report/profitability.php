<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Finanace Reports' => '/admin/report/transaction',
    'Profitability'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

?>
<link href="/css/buttons.dataTables.min.css" rel="stylesheet">

<!--<span class="success" id="bidSuccessMsg" style="display: none">Bid Created Successfully.</span>
<span class="error" id="bidErrorMsg" style="display: none">Failed to create bid.</span>-->

<script src="/js/datatables.min.js"></script>
<!--<script src="/js/dataTables.select.min.js"></script>-->
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">

<div class="user-status col-md-12 filter-toggle">
    <form id="profitability_filter_frm" name="profitability_filter_frm" method="post" action="/admin/report/profitability" >
        <div class="col-md-4">    
            <span id="date_error" style="color:red"></span>
            <div class="input-group input-large date-picker input-daterange pull-left">
                <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                <span class="input-group-addon"> to </span>
                <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
            </div>
        </div>
        <div class="col-md-4 col-sm-4 bulkSelect">
        <div class="form-group">
            <label class="col-md-2 col-sm-2 padding0 top7 margin-left-15"> Status </label>
            <div class="col-md-4 col-sm-4">
                <select class=" form-control " id="actionStatus" name="actionStatus">
                    <option value="inactive">Close</option>
                    <option value="active">Open</option>
                </select>
            </div>
        </div>
    </div>
        <div class="col-md-1 col-sm-6 form-inline">
            <input type="button" class="btn btn-success margintop3 margin-left-15" value="Filter" name="profitability" id="profitability"/>
        </div>
    </form>
</div>
    <div class="row">
        <div class="col-md-12 responsiveTable">
            <?php
            if (Yii::app()->user->hasFlash('error')):
                echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
            endif;
            if (Yii::app()->user->hasFlash('success')):
                echo '<div class="alert alert-success">' . Yii::app()->user->getFlash('success') . '</div>';
            endif;
            ?>
      
            <table id="profitabilityreport" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Auction Id</th>
                        <th>Product Name</th>
                        <th>End Date</th>
                        <th>Amount Collected</th>
                        <th>Winner Name</th>
                        <th>User Type</th>
                        <th>Auction Status</th>
                    </tr>
                </thead>
            </table>

            <script src="/js/dataTables.buttons.min.js"></script>
            <script src="/js/jszip.min.js"></script>
            <script src="/js/buttons.html5.min.js"></script>
            <script src="/js/buttons.print.min.js"></script>
            
            <script>
                $(document).ready(function () {
                    oTable = $('#profitabilityreport').DataTable({
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        "pagingType": "full_numbers",
                        "order": [[2, "desc"]],
                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                        "buttons": [
                                'copy', 'csv', 'excel', 'print'
                            ],
                        
                        "ajax": {
                            "url": "profitabilitydata",
                            "type": "POST"
                        },
                        "columns": [
                            {"data": "auctionId"},
                            {"data": "productName"},
                            {"data": "endDate"},
                            {"data": "amountCollected"},
                            {"data": "winnerName"},
                            {"data": "winnerUserType"},
                            {"data": "auctionStatus"},
                        ],
                    });
                });
                
                $("#profitability").click(function() {
                    $("#date_error").html("");
                    if($("#toDate").val()>$("#fromDate").val()){
                        $("#date_error").html("To Date should be greater then From Date!!!");
                        $("#fromDate").focus();
                        return false;
                    }
                oTable.columns(6).search($("#actionStatus").val().trim());
                oTable.columns(2).search($("#toDate").val().trim(),$("#fromDate").val().trim());
                
                oTable.draw();
                });
                                
            </script>
        </div>
    </div>