<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Reports' => '/admin/report',
    'Member Address'
);
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">
<div class="col-md-12 user-status filter-toggle">
    <div class="expiration margin-topDefault confirmMenu">
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/address">
    <div class="input-group input-medium date-picker input-daterange f-left">
        <input type="text" name="from" placeholder="From Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST) && $_REQUEST['from'] !='') ?  $_REQUEST['from'] :  DATE('Y-m-d');?>">
        <span class="input-group-addon">
        to </span>
        <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST) && $_REQUEST['to'] !='') ?  $_REQUEST['to'] :  DATE('Y-m-d');?>">
    </div>
    <input type="submit" class="btn btn-primary margin-left-15 margintop3" value="OK" name="submit" id="submit"/>
    </form>
</div>
</div>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                 array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                ),
                array(
                    'name' => 'name',
                    'header' => '<span style="white-space: nowrap;">Name &nbsp;</span>',
                    'value' => 'isset($data->user->name)?$data->user->name:""',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Full Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->user->full_name)?$data->user->full_name:""',
                ),
                
                array(
                    'name' => 'address',
                    'header' => '<span style="white-space: nowrap;">Address  &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->address',
                ),
                
                array(
                    'name' => 'country_id',
                    'header' => '<span style="white-space: nowrap;">Country &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->country->name)?$data->country->name:""',
                ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Phone &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this ,'gridPhone'),
                ),
                array(
                    'name' => 'status',
                    'value' => '($data->user->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
            ),
        ));
        ?>
    </div>
</div>
