<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Finanace Reports' => '/admin/report/transaction',
    'Offline Payment'
);
$from = "";
if (isset($_GET['from']) && isset($_GET['to'])) {
    $from = "?" . $_SERVER['QUERY_STRING'];
}

if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/requestpayment?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/requestpayment?per_page=' . "'";
}
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">
<div class="col-md-12 order-list-div margin-bottom-15 filter-toggle">  
    <?php if (isset($success)) { ?><?php echo $success; ?><?php } ?>
    <div class="expiration confirmMenu">
        <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/requestpayment">
            <div class="col-md-3">
                <div class="input-group input-medium date-picker input-daterange">
                    <input type="text" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from']) && $_GET['from'] != '') ? $_GET['from'] : ""; ?>">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to']) && $_GET['to'] != '') ? $_GET['to'] : ""; ?>">
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 spl-label">Gateway</label>
                        <div class="col-md-8">
                             <select class="form-control" id="ui-id-5" name="res_filter">
                    <option value="" selected="selected">All</option>
                    <?php
                    if (isset($gatewayObject)) {
                        foreach ($gatewayObject as $gateway) {
                            ?>
                            <option value="<?php echo $gateway->id ?>" <?php
                            if (isset($_GET['res_filter'])) { if ($_GET['res_filter'] == $gateway->id) echo 'selected'; }?>><?php echo $gateway->name; ?></option>
                            <?php }
                        } ?>                      
                </select>
                        </div>
                    </div>
                </div>
                
                
               
            </div>
            <div class="col-md-3">
                <div class="dataTables_length" id="search_length">
                    <label> Display
                        <select id="per_page" name="per_page" aria-controls="" class="" onchange="window.location = <?php echo $baseUrl; ?> + this.value">
                            <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                            <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                            <?php } ?>
                        </select>  
                        Records per page</label>
                </div>
            </div> 
            <div class="col-md-1">
                <input type="submit" class="btn btn-success confirmOk pull-right margintop3" value="OK" name="submit" id="submit"/>     
            </div>
        </form>
    </div> 
</div>
<a class="btn buttons-csv pull-right" href="/admin/report/requestpaymentcsv<?php echo $from; ?>"> CSV Export </a>

<div class="row">
    <div class="col-md-12 blue-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}", //THIS DOES WHAT YOU WANT
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">No.</span>',
                ),               
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Name &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->user->name)?$data->user->name:""',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Email &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->user->email)?$data->user->email:""',
                ),
                array(
                    'name' => 'gateway_id',
                    'header' => '<span style="white-space: nowrap;">Gateway  &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->gateway->name)?$data->gateway->name:""',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Country &nbsp; &nbsp;</span>',
                    'value' => function($data){
                        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->user_id));
                        echo isset($userProfileObject->country->name)?$userProfileObject->country->name:"";
                    }
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Phone Number &nbsp; &nbsp; &nbsp;</span>',
                    'value' => function($data){
                        $phone = "";
                        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->user_id));
                        if(!empty($userProfileObject->phone)){
                           $phone .= isset($userProfileObject->country_code)?"+".$userProfileObject->country_code:"";
                           $phone .= " ".$userProfileObject->phone;
                        } echo $phone;
                    }
                ),
                array(
                    'name' => 'amount',
                    'header' => '<span style="white-space: nowrap;">Amount &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->amount) ? $data->amount : ""',
                ),
                array(
                    'name' => 'actual_ip',
                    'header' => '<span style="white-space: nowrap;">Req.Ip &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->actual_ip) ? $data->actual_ip : ""',
                ),
                array(
                    'name' => 'actual_country',
                    'header' => '<span style="white-space: nowrap;">Req.Country &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->actual_country) ? $data->actual_country : ""',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created Date &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
            ),
        ));
        ?>
    </div>
</div>