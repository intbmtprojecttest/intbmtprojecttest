<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<table align="center" width="600" cellspacing="0" cellpadding="0"  style="font-family: 'Roboto', sans-serif;border-bottom:solid 5px #00adff" >
    <tr>
        <td bgcolor="#fafafa" height="80" style="line-height:0px">
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                    <tr>
                        <td width="100%" valign="middle" style="line-height:0px"> <img width="" border="0" src="<?php echo Yii::app()->getBaseUrl(true); ?>/images/banner.png" alt="logo"></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td  >
            <table style="border-bottom:solid 1px #00aeef;border-top:solid 1px #00aeef;    padding-right: 0px;
                   padding-left: 0px;"  cellspacing="0" cellpadding="0"  align="center" >
                <tbody>
                    <tr>
                        <td style="line-height:50px;font-size:16px;text-align:right">User Name<span style="padding-left:25px">:</span></td>
                        <td style="line-height:50px;font-size:16px;padding-left:25px"><?php echo $userObjectArr->full_name; ?></td>
                    </tr>
                    <tr>
                        <td style="line-height:50px;font-size:16px;text-align:right">Amount Withdrawn<span style="padding-left:25px">:</span></td>
                        <td style="line-height:50px;font-size:16px;padding-left:25px">$<?php echo $withDrawObjectArr->received_amount; ?></td>
                    </tr>
                    <tr>
                        <td style="line-height:50px;font-size:16px;text-align:right">Withdrawn Using<span style="padding-left:25px">:</span></td>
                        <td style="line-height:50px;font-size:16px;padding-left:25px"><img width="" border="0" style="max-width:100%; max-height:100%" src="<?php echo Yii::app()->getBaseUrl(true); ?>/images/payments/<?php echo $withDrawObjectArr->transaction->gateway->slug; ?>.png" alt="logo"></td>
                    </tr>
                    <tr>
                        <td style="line-height:50px;font-size:16px;text-align:right">Transaction Reference<span style="padding-left:25px">:</span></td>
                        <td style="line-height:50px;font-size:16px;padding-left:25px"><?php echo $withDrawObjectArr->transaction->transaction_id; ?></td>
                    </tr>
                    <tr>
					  <td style="line-height:50px; font-weight: bold; font-size:16px;text-align:right" colspan="2"> " Nobody ever lost money taking a profit "  --<span class="color:#00aeef">Bernard Baruch</span> </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr style="background:#FFFFFF">
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-top:30px;margin-bottom:30px;">
                <tr>
                    <td width="300" style="width:300px;" align="left"> 

                        <p style="margin-bottom:0px;font-size:18px;color:#7e7e7e">Thanks & Regards</p>
                        <h2 style="margin-bottom:0px;margin-top:0px;font-size:25px;color:#00aeef;font-weight:400">Mavwealth Team</h2>
                    </td>
                    <td  valign="middle" width="300" style="width:300px;" align="right">
                        <img width="" border="0" src="<?php echo Yii::app()->getBaseUrl(true); ?>/images/mavwealth_logo.png" alt="logo">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>