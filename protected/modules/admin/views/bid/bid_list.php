<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Bids' => '/admin/bid/list',
    'Bid Summary'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

?>

<link href="/css/buttons.dataTables.min.css" rel="stylesheet">

<script src="/js/datatables.min.js"></script>
<!--<script src="/js/dataTables.select.min.js"></script>-->
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
<div class="user-status col-md-12 filter-toggle">
<form id="auctionBid_filter_frm" name="auctionBid_filter_frm" method="post" action="" >
    <div class="col-md-4"> 
        <span id="date_error" style="color:red"></span>
        <div class="input-group input-large date-picker input-daterange pull-left">
            <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
            <span class="input-group-addon"> to </span>
            <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
        </div>
    </div>
    
    <div class="col-md-4 col-sm-4 bulkSelect">
        <div class="form-group">
            <label class="col-md-2 col-sm-2 padding0 top7 mobile-left-15"> Status </label>
            <div class="col-md-4 col-sm-4">
                <select class=" form-control " id="actionStatus" name="actionStatus">
                    <option value="active">Active</option>
                    <option value="inactive">InActive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-1 col-sm-6 form-inline">
        <input type="button" class="btn btn-success margintop3 mobile-left-15" value="Filter" name="bid_status_check" id="bid_status_check"/>
    </div>
</form>
</div>
    <div class="row ">
        <div class="col-md-12 responsiveTable margin-top-15">
            <table id="bidsummary" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Auction Id</th>
                        <th>Product Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Bid Fee</th>
                        <th>User Bids</th>
                        <th>Winning Bid</th>
                        <th>Winner Name</th>
                        <th>User Type</th>
                        <th>Auction Status</th>
                        <th>Created Date</th>

                    </tr>
                </thead>
            </table>
            
            <script src="/js/dataTables.buttons.min.js"></script>
            <script src="/js/jszip.min.js"></script>
            <script src="/js/buttons.html5.min.js"></script>
            <script src="/js/buttons.print.min.js"></script>

            <script>
                $(document).ready(function () {
                    oTable = $('#bidsummary').DataTable({
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        "pagingType": "full_numbers",
                        "order": [[10, "desc"]],
                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                        "buttons": [
                            'copy', 'csv', 'excel', 'print'
                        ],
                        "aoColumnDefs": [ {
                            'bSortable' : false,
                            'aTargets' : [ 0 ]
                        } ],
                        "ajax": {
                            "url": "bidsummary",
                            "type": "POST"
                        },
                        "columns": [
                            {"data": "auctionId"},
                            {"data": "productName"},
                            {"data": "startDate"},
                            {"data": "endDate"},
                            {"data": "bidFee"},
                            {"data": "userBids"},
                            {"data": "winningBid"},
                            {"data": "winnerName"},
                            {"data": "winnerUserType"},
                            {"data": "auctionStatus"},
                            {"data": "updatedDate"},
                        ],
                    });
                });
                
                $("#bid_status_check").click(function() {
                $("#date_error").html("");
                if($("#toDate").val()>$("#fromDate").val()){
                    $("#date_error").html("To Date should be greater then From Date!!!");
                    $("#fromDate").focus();
                    return false;
                }
                oTable.columns(8).search($("#actionStatus").val().trim());
                oTable.columns(9).search($("#toDate").val().trim(),$("#fromDate").val().trim());
                
                oTable.draw();
                });
            </script>
        </div>
    </div>