<?php
$this->breadcrumbs = array(
    'Bid' => array('bid/ist'),
    'Bid Edit',
);
 
?>
<?php 
if(!empty($auctionObject)){
 $startDate=explode(" ",$auctionObject->start_date);
 $closeDate=explode(" ",$auctionObject->close_date);
 $starttime = ltrim($startDate[1],'0');
 $closetime = ltrim($closeDate[1],'0');
}
?>
<div class="col-md-7 col-sm-7">
    <?php if($error){?><div class="error" id="error_msg"><?php echo $error;?></div><?php }?>
    <?php if($success){?><div class="success" id="error_msg"><?php echo $success;?></div><?php }?>
   
    <form action="/admin/bid/edit?id=<?php echo $auctionObject->id;?>" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validation();">
     
         <fieldset>
      
<div class="form-body">
          
             <div class="form-group">
                <label for="country" class="col-lg-4 control-label">Package<span class="require">*</span></label>
                <div class="col-lg-7">
                    <select name="package_id" id="package_id">
                    <option value="">Select Package</option>
                    <?php foreach ($packageObject as $package) { ?>
                            <option value="<?php echo $package->id; ?>" <?php if(!empty($auctionObject->package_id) && $auctionObject->package_id==$package->id){?> selected="selected" <?php } ?>> <?php echo $package->name; ?> </option>";         
                    <?php } ?>
                    </select>
                    <span style="color:red;" id="package_id_error"></span>
                </div>
            </div>
    
           <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Start Date & Time<span class="require">*</span></label>
                <div class="col-lg-4">
                    <input type="text" id="sdate" class="datepicker form-control" name="sdate" value="<?php echo (!empty($auctionObject))?$startDate[0] : "";?>">&nbsp;
<!--                <input type="text" id="stime" class="form-control" name="stime" value="<?php //echo (!empty($auctionObject))?$startDate[1] : "";?>">-->
                    <select id="stime" class="form-control" name="stime">
                        <option value="">Select start time</option>
                         <?php
                         $timeAry=array("00","30");
                         for($i=1;$i<=24;$i++) { foreach($timeAry as $time) {?>
                        <option value="<?php echo $i.":".$time.":"."00"; ?>" <?php if(!empty($auctionObject) && $starttime==$i.":".$time.":"."00"){?> selected="selected" <?php } ?>><?php echo $i.":".$time; ?></option>
                         <?php } }?>
                    </select>
                    <span style="color:red;" id="start_error"></span>
                </div>
            </div>
    
            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Close Date & Time<span class="require">*</span></label>
                <div class="col-lg-4">
                    <input type="text" id="cdate" class="datepicker form-control" name="cdate" value="<?php echo (!empty($auctionObject))?$closeDate[0] : "";?>">&nbsp;
<!--                    <input type="text" id="ctime" class="form-control" name="ctime" value="<?php //echo (!empty($auctionObject))?$closeDate[1] : "";?>">-->
                    <select id="ctime" class="form-control" name="ctime">
                        <option value="">Select close time</option>
                         <?php
                         $timeAry=array("00","30");
                         for($i=1;$i<=24;$i++) { foreach($timeAry as $time) {?>
                        <option value="<?php echo $i.":".$time.":"."00"; ?>" <?php if(!empty($auctionObject) && $closetime==$i.":".$time.":"."00"){?> selected="selected" <?php } ?>><?php echo $i.":".$time; ?></option>
                         <?php } }?>
                    </select>
                    <span style="color:red;" id="close_error"></span>
                </div>
            </div>
    
           <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Entry fee<span class="require">*</span></label>
                <div class="col-lg-7">
                    <input type="text" id="entry_fees" class="form-control" name="entry_fees" value="<?php echo (!empty($auctionObject))?number_format($auctionObject->entry_fees,2) : "";?>">
                    <span style="color:red;" id="entry_fees_error"></span>
                </div>
            </div>
    
           <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Lowest bid price<span class="require">*</span></label>
                <div class="col-lg-7">
                    <input type="text" id="lowest_bid_price" class="form-control" name="lowest_bid_price" value="<?php echo (!empty($auctionObject))?number_format($auctionObject->lowest_bid_price,2) : "";?>">
                    <span style="color:red;" id="lowest_bid_price_error"></span>
                </div>
            </div>
            
             <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Is Premium</label>
                <div class="col-lg-7">
                    <input type="checkbox" id="is_premium" class="form-control" name="is_premium" value="1" <?php echo (!empty($auctionObject) && $auctionObject->is_premium==1)? "checked": "";?>>
                </div>
            </div>
    
            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Is Publish</label>
                <div class="col-lg-7">
                    <input type="checkbox" id="is_publish" class="form-control" name="is_publish" value="1" <?php echo (!empty($auctionObject) && $auctionObject->is_publish==1)? "checked": "";?>>
                </div>
            </div>
</div>
        </fieldset>

    <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Update" class="btn red">
                 
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    function validation()
    {
        var regex = /^\d*(.\d{2})?$/;
        var entryamount = document.getElementById('entry_fees');
        var lowestamount = document.getElementById('lowest_bid_price');
    
        $("#package_id_error").html("");
        if ($("#package_id").val() == "") {
            $("#package_id_error").html("Select package type");
            $("#package_id").focus();
            return false;
        }
        
         $("#start_error").html("");
        if ($("#sdate").val() == "") {
            $("#start_error").html("Enter start date");
            $("#sdate").focus();
            return false;
        }
        $("#start_error").html("");
        if ($("#stime").val() == "") {
            $("#start_error").html("Enter start time");
            $("#stime").focus();
            return false;
        }
         $("#close_error").html("");
        if ($("#cdate").val() == "") {
            $("#close_error").html("Enter close date");
            $("#cdate").focus();
            return false;
        }
        $("#close_error").html("");
        if ($("#ctime").val() == "") {
            $("#close_error").html("Enter close time");
            $("#ctime").focus();
            return false;
        }
        
        $("#entry_fees_error").html("");
        if ($("#entry_fees").val() == "") {
            $("#entry_fees_error").html("Enter Entry Fees");
            $("#entry_fees").focus();
            return false;
        } else {
            if (isNaN($("#entry_fees").val())) {
                $("#entry_fees_error").html("Enter valid Entry Fees.");
                $("#entry_fees").focus();
                return false;
            }
        }
        if (!regex.test(entryamount.value)) {
            $("#entry_fees_error").html("Enter Valid Entry Fees");
            $("#entry_fees").focus();
            return false;
        }
         $("#lowest_bid_price_error").html("");
        if ($("#lowest_bid_price").val() == "") {
            $("#lowest_bid_price_error").html("Enter Lowest Bid Price");
            $("#lowest_bid_price").focus();
            return false;
        } else {
            if (isNaN($("#lowest_bid_price").val())) {
                $("#lowest_bid_price_error").html("Enter valid Lowest Bid Price.");
                $("#lowest_bid_price").focus();
                return false;
            }
        }
        if (!regex.test(lowestamount.value)) {
            $("#lowest_bid_price_error").html("Enter Valid Lowest Bid Price");
            $("#lowest_bid_price").focus();
            return false;
        }
    }
</script>
     
     
