<?php
$this->breadcrumbs = array(
    'Bid Add',
);
?>
<div class="col-md-7 col-sm-7">
    <?php if ($error) { ?><div class="error" id="error_msg"><?php echo $error; ?></div><?php } ?>
    <?php if ($success) { ?><div class="success" id="error_msg"><?php echo $success; ?></div><?php } ?>
<div class="portlet box orange   ">
    <div class="portlet-title">
							<div class="caption">
								Add Auction
							</div>
    </div>
        <div class="portlet-body form">
            <form action="/admin/bid/add" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validation();">

        <fieldset>
      
<div class="form-body">
          
             <div class="form-group">
                <label for="country" class="col-lg-4 control-label">Package<span class="require">*</span></label>
                <div class="col-lg-7">
                    <select name="package_id" id="package_id">
                    <option value="">Select Package</option>
                    <?php foreach ($packageObject as $package) { ?>
                            <option value="<?php echo $package->id; ?>"> <?php echo $package->name; ?> </option>";         
                    <?php } ?>
                    </select>
                    <span style="color:red;" id="package_id_error"></span>
                </div>
            </div>
    
           <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Start Date & Time<span class="require">*</span></label>
                <div class="col-lg-4">
                    <input type="text" id="sdate" class="datepicker form-control" name="sdate">&nbsp;
<!--                <input type="text" id="stime" class="form-control" name="stime">-->
                    <select id="stime" class="form-control" name="stime">
                        <option value="">Select start time</option>
                         <?php
                         $timeAry=array("00","30");
                         for($i=1;$i<=24;$i++) { foreach($timeAry as $time) {?>
                        <option value="<?php echo $i.":".$time.":"."00"; ?>"><?php echo $i.":".$time; ?></option>
                         <?php } }?>
                    </select>
                    <span style="color:red;" id="start_error"></span>
                </div>
            </div>
    
            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Close Date & Time<span class="require">*</span></label>
                <div class="col-lg-4">
                    <input type="text" id="cdate" class="datepicker form-control" name="cdate">&nbsp;
<!--                <input type="text" id="ctime" class="form-control" name="ctime">-->
                    <select id="ctime" class="form-control" name="ctime">
                        <option value="">Select close time</option>
                         <?php
                         $timeAry=array("00","30");
                         for($i=1;$i<=24;$i++) { foreach($timeAry as $time) {?>
                        <option value="<?php echo $i.":".$time.":"."00"; ?>"><?php echo $i.":".$time; ?></option>
                         <?php } }?>
                    </select>
                    <span style="color:red;" id="close_error"></span>
                </div>
            </div>
    
           <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Entry fee<span class="require">*</span></label>
                <div class="col-lg-7">
                    <input type="text" id="entry_fees" class="form-control" name="entry_fees">
                    <span style="color:red;" id="entry_fees_error"></span>
                </div>
            </div>
    
           <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Lowest bid price<span class="require">*</span></label>
                <div class="col-lg-7">
                    <input type="text" id="lowest_bid_price" class="form-control" name="lowest_bid_price">
                    <span style="color:red;" id="lowest_bid_price_error"></span>
                </div>
            </div>
    
             <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Is Premium</label>
                <div class="col-lg-7">
                    <input type="checkbox" id="is_premium" class="form-control" name="is_premium" value="1">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Is Publish</label>
                <div class="col-lg-7">
                    <input type="checkbox" id="is_publish" class="form-control" name="is_publish" value="1">
                </div>
            </div>
</div>
        </fieldset>
            <div class="form-actions right">                     
                <input type="submit" name="submit" value="Submit" class="btn orange">
                 
            </div>
        
    </form>
</div>
</div>
</div>

<script type="text/javascript">
    function validation()
    {
        var regex = /^\d*(.\d{2})?$/;
        var entryamount = document.getElementById('entry_fees');
        var lowestamount = document.getElementById('lowest_bid_price');
        var starttimeval = document.getElementById('stime');
    
        $("#package_id_error").html("");
        if ($("#package_id").val() == "") {
            $("#package_id_error").html("Select package type");
            $("#package_id").focus();
            return false;
        }
        $("#start_error").html("");
        if ($("#sdate").val() == "") {
            $("#start_error").html("Enter start date");
            $("#sdate").focus();
            return false;
        }
        $("#start_error").html("");
        if ($("#stime").val() == "") {
            $("#start_error").html("Please select start time");
            $("#stime").focus();
            return false;
      }        
        $("#close_error").html("");
        if ($("#cdate").val() == "") {
            $("#close_error").html("Enter close date");
            $("#cdate").focus();
            return false;
        }
        $("#close_error").html("");
        if ($("#ctime").val() == "") {
            $("#close_error").html("Please select close time");
            $("#ctime").focus();
            return false;
        }
        
        $("#entry_fees_error").html("");
        if ($("#entry_fees").val() == "") {
            $("#entry_fees_error").html("Enter Entry Fees");
            $("#entry_fees").focus();
            return false;
        } else {
            if (isNaN($("#entry_fees").val())) {
                $("#entry_fees_error").html("Enter valid Entry Fees.");
                $("#entry_fees").focus();
                return false;
            }
        }
        if (!regex.test(entryamount.value)) {
            $("#entry_fees_error").html("Enter Valid Entry Fees");
            $("#entry_fees").focus();
            return false;
        }
        $("#lowest_bid_price_error").html("");
        if ($("#lowest_bid_price").val() == "") {
            $("#lowest_bid_price_error").html("Enter Lowest Bid Price");
            $("#lowest_bid_price").focus();
            return false;
        } else {
            if (isNaN($("#lowest_bid_price").val())) {
                $("#lowest_bid_price_error").html("Enter valid Lowest Bid Price.");
                $("#lowest_bid_price").focus();
                return false;
            }
        }
        if (!regex.test(lowestamount.value)) {
            $("#lowest_bid_price_error").html("Enter Valid Lowest Bid Price");
            $("#lowest_bid_price").focus();
            return false;
        }
    }
</script>
<script>
    $(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
</script>

