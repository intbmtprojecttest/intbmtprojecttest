<?php 
$this->breadcrumbs = array(
    'With Draw' => array(''),
    'History' => array('/list'),
);
?>
<p>Withdrawal Request payout will be done on 15th and 30th ( or last day of the month) however Withdrawal requests can be done any time before that.</p>
<div class="col-md-8 col-sm-8">
    <?php  if ($error) { ?><p class="error-2" id="error_msg"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php  echo $error; ?></span></p><?php  } ?>
    <?php  if ($success) { ?><p class="success-2" id="error_msg"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php  echo $success; ?></span></p><?php  } ?>
    <div class="portlet box toe-blue ">
        <div class="portlet-title">
            <div class="caption">
                Withdrawal Request
            </div>
        </div>
        <div class="portlet-body form">
            <form action="" method="post" class="form-horizontal" onsubmit="return withdrawal();">
                <fieldset>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="wallet">Wallet Type</label>
                            <div class="col-lg-7">
                                <select name="wallet_id" id="wallet_id" class="form-control" onchange="getWalletData(this.value);">
                                    <option value="">Select Wallet</option>
                                    <?php  foreach ($walletDetailsObject as $walletDetails) { ?>
                                        <option value="<?php  echo $walletDetails->id; ?>" ><?php  echo $walletDetails->name; ?></option>
                                    <?php  } ?>
                                </select>
                                <span class="form_error" id="wallet_id_error"></span>
                            </div>
                        </div>
                        
                        <div id="amount_data_show"></div>
                        <div id="amount_admin_charges"></div>
                        

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="amount">Withdrawal Amount<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="withdrawal_amount" class="form-control" name="withdrawal_amount" maxlength="5">
                                <span class="form_error" id="withdrawal_amount_error"></span>
                            </div>
                        </div>
                        <div id="amount_max_user_can_transfer"></div>
                        <div class="form-group">
                            <label for="country" class="col-lg-4 control-label">Request Ecurrency <span class="require">*</span></label>
                            <div class="col-lg-7">
                                <select name="request_ecurrancy" id="request_ecurrancy" class="form-control" onchange="getGatewayName(this.value);">
                                    <option value="0">Select Request Ecurrency</option>
                                    <?php  foreach ($gatewayObject as $gateway) { ?>
                                        <option value="<?php  echo $gateway->id."--".$gateway->name; ?>" ><?php  echo $gateway->name; ?></option>
                                    <?php  } ?>
                                </select>
                                <span id="request_ecurrancy_error" class="form_error"></span>
                            </div>                            
                        </div>    
                        

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="account_number"><span id="accountDetail">Account Number</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="account_number" class="form-control" name="account_number" maxlength="20">
                                 <span id="account_number_error" class="form_error"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Master Pin<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="master_pin" class="form-control" name="master_pin">
                                <div id="master_pin_error" class="form_error"></div>
                            </div>
                            
                        </div>
                    </div>
                </fieldset>
                <div class="form-actions right">                     
                    <input type="submit" name="submit" value="Submit Request" class="btn orange">
                </div>
                
                <input type="hidden" id="userErrorFlag" value="0">
                <input type="hidden" id="userWalletAmount" name="userWalletAmount" value=""/>
                <input type="hidden" id="totalGet" name="totalGet" value=""/>
                <input type="hidden" id="adminCharges" name="adminCharges" value=""/>
                
            </form>
        </div>
    </div>
</div>