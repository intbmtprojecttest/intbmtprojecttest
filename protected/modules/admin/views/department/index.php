
<ul class="page-breadcrumb breadcrumb">
    <li>
        <div class="breadcrumbs">
            <a href="/admin/default/dashboard">Dashboard</a> »
            <a href="/admin/UserHasAccess/members">Member Access</a> »
            <span>Department</span>
        </div>
    </li>
</ul>
<div class="col-md-12">
    <div class="row">
        <div class="expiration margin-topDefault confirmMenu">
            <a class="btn  green margin-right-20" style="float:left" href="/admin/department/add">New Department +</a> 
        </div>    
    </div>

</div>
<div class="col-md-12">
    <div class="row"> 
        
    <?php if (Yii::app()->user->hasFlash('error')){ ?>
        <p class="error-2" id="error_msg"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php echo Yii::app()->user->getFlash('error'); ?></span></p>
    <?php } ?>

    <?php if (Yii::app()->user->hasFlash('success')){ ?>
        <p class="success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php echo Yii::app()->user->getFlash('success'); ?></span></p>
    <?php } ?>
    
    </div>
</div>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '<div class="table-responsive">{items}</div> {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',

                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Sl. No &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$row+1',
                ),
                array(
                    'name' => 'name',
                    'header' => '<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->name',
                ),
                array(
                    'name' => 'created_by',
                    'header' => '<span style="white-space: nowrap;">Created By &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->user->name)?$data->user->name:""',
                ),
                array(
                    'name' => 'status',
                    'header' => '<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->status==0)? "Inactive" : "Active"',
                ),
                 array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Change}{Edit}',
                    'htmlOptions' => array('width' => '10%'),
                    'buttons' => array(
                        'Change' => array(
                            'label' => '<i class="fa fa-retweet"></i>',
                            'options' => array('class' => 'action-icons', 'title' => 'Change Status'),
                            'url' => 'Yii::app()->createUrl("admin/department/changestatus", array("id"=>$data->id))',
                        ),
                        'Edit' => array(
                            'label' => '<i class="fa fa-pencil-square-o"><i>',
                            'options' => array('class' => 'action-icons', 'title' => 'Edit'),
                            'url' => 'Yii::app()->createUrl("admin/department/add", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>
