<?php 
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Member Access' => '/admin/UserHasAccess/members',
    isset($_GET['id'])?'Edit Department':'Add Department',
);

$type = "ADD";
if (isset($_GET['id'])) {
    $type = "EDIT";
    $actionUrl = "/admin/department/add?id=" . $_GET['id'];
} else {
    $type = "ADD";
    $actionUrl = "/admin/department/add";
}
?>
<div class="col-md-11 col-sm-11">
 
<?php if (Yii::app()->user->hasFlash('error')){ ?>
    <p class="error-2" id="error_msg"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php echo Yii::app()->user->getFlash('error'); ?></span></p>
<?php } ?>
    
<?php if (Yii::app()->user->hasFlash('success')){ ?>
    <p class="success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php echo Yii::app()->user->getFlash('success'); ?></span></p>
<?php } ?>
 
    <div class="col-md-7 portlet box toe-blue   ">
        <div class="portlet-title">
            <div class="caption">
                <?php echo $type; ?> Department
            </div>
        </div>
        <div class="portlet-body form add-dpt">
            <form action="<?php echo $actionUrl; ?>" method="post" class="form-horizontal" onsubmit="return validateDepartment();">

            

                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Department Name<span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" id="name" class="form-control" name="name" value="<?php echo isset($departmentObject->name) ? $departmentObject->name : ""; ?>">
                                <span style="color:red;" id="name_error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions right db-action">                     
                        <input type="submit" name="submit" value="Submit" class="btn mav-blue-btn">

                    </div>
       

            </form>
        </div>
    </div>
</div>