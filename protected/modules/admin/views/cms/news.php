<?php 
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'CMS' => '/admin/cms/news',
    'News'
);

if (isset($_GET['id'])) {
    $classAddEdit = "active"; //Add tab making as Edit
    $type = "EDIT";
    $actionUrl = "/admin/cms/news?id=" . $_GET['id'];
} else {
    $classList = "active"; 
    $actionUrl = "/admin/cms/news";
}

$bulkaction = "";
$statusId = "";
if (isset($_POST['bulkaction'])) { $bulkaction = $_POST['bulkaction']; }
if (isset($_GET['status'])) { $statusId = $_GET['status']; }

if (Yii::app()->user->hasFlash('error')): 
    echo '<p class="alert alert-danger" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
endif; 
if (Yii::app()->user->hasFlash('success')): 
     echo '<p class="alert alert-success" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">'.Yii::app()->user->getFlash('success') .'</span></p>';
endif;
?>
    
    <div class="expiration confirmMenu row">

        <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/cms/news">
<!--            <div class="col-md-4 margin-topDefault ">
                <div class="input-group input-large date-picker input-daterange" style="z-index:9;">
                    <input type="text" id="txtFromDate" name="from" placeholder="From Date" class="datepicker form-control to_date datefrom" value="<?php echo (!empty($_GET['from']) && $_GET['from'] != '') ? $_GET['from'] : ""; ?>">
                    <span id="error_orderfromdate" style="color:red"></span>
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" id="txtToDate" name="to" data-provide="datepicker1" placeholder="To Date" class="datepicker1 form-control from_date dateto" value="<?php echo (!empty($_GET['to']) && $_GET['to'] != '') ? $_GET['to'] : ""; ?>">
                    <span id="error_ordertodate" style="color:red"></span>
                </div>
            </div>-->
            <div class="col-md-3 form-inline margin-topDefault margin-topDefault cms-news">
                    <select class="customeSelect howDidYou form-control  select2me confirmBtn pull-left right20" id="ui-id-5" name="status" >
                        <option value="1" <?php if ($statusId == '1') { ?> selected="selected"<?php } ?>>Active</option>
                        <option value="0" <?php if ($statusId == '0') { ?> selected="selected"<?php } ?>>Inactive</option>
                    </select>

                <input type="submit" class="btn btn-primary confirmOk margintop3" value="OK" name="submit" id="submit"/>
            </div>
        </form>
    </div>

<div class="row">
    <div class="col-md-12 margin-top-20">
        
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo isset($classList) ? $classList : ""; ?>"><a onclick="showAddEditTab();" href="#list" id="link-banner-list" aria-controls="profile" role="tab" data-toggle="tab">LIST</a></li>
            <li role="presentation" class="<?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>"><a href="#addedit" id="link-banner-addedit" aria-controls="home" role="tab" data-toggle="tab"><?php echo isset($type) ? $type : "ADD"; ?></a></li>
        </ul>
        
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>" id="addedit">
            <div class="col-md-7 col-sm-7">
                <form enctype="multipart/form-data" action="<?php echo $actionUrl; ?>" method="post" onsubmit="return newsValidation();">
                    <div class="portlet box toe-blue">
                        <div class="portlet-title">
                            <div class="caption">
                            <h4><?php echo isset($type) ? $type : "Add"; ?> News</h4>
                            </div>
                        </div>

                            <div class="portlet-body form padding15">
                             
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Title *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="title" id="news_title" placeholder="News Title" value="<?php echo isset($newsObject->title) ? $newsObject->title : ""; ?>">
                                        <span id="error_news_title" style="color:red;"></span>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">News *</label>
                                    <div class="col-sm-8">
                                        <textarea name="news" id="summernote_1" class="news_message"><?php echo isset($newsObject->news) ? $newsObject->news : ""; ?></textarea>
                                        <span id="error_news_message" style="color:red;"></span>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Published Date *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="onlydate form-control published_date" name="publish_date" id="news_publish_date" placeholder="Published Date" value="<?php echo isset($newsObject->publish_date) ? date('Y-m-d',strtotime($newsObject->publish_date)) : ""; ?>">
                                        <span id="error_news_publish_date" style="color:red;"></span>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Expiry Date </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="onlydate form-control published_date" name="expiry_date" id="news_expiry_date" placeholder="Expiry Date" value="<?php echo isset($newsObject->expiry_date) ? date('Y-m-d',strtotime($newsObject->expiry_date)) : ""; ?>">
                                        <span id="error_news_expiry_date" style="color:red;"></span>
                                    </div>
                                </div>
                                <?php
                                if (!empty($newsObject) && $newsObject->image != "") {
                                ?>
                                    <div class="form-group" id="news_image_div">
                                       <label for="firstname" class="col-lg-3 control-label">Current Image</label>
                                        <div class="col-lg-8">
                                            <img src="<?php echo Yii::app()->params['imagePath']['news'] . $newsObject->image ; ?>" style="max-height: 200px;max-width: 200px;" />                   
                                        </div>
                                       <input type="hidden" name="remove_image" id="remove_image" value="">
                                       <a onclick="removeNewsImage();" class="image-pointer" title="Remove Image"><i class="fa fa-times"></i></a>
                                    </div> 
                                <?php
                                }  ?>
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Image</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control" name="image" id="cms_news_image">
                                        <span id="cms_news_image_error" style="color:red;"></span>
                                    </div>
                                </div>
                             <input type="submit" name="addedit" id="addedit" class="btn green">
                            </div>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php echo isset($classList) ? $classList : ""; ?>" id="list">
        
            <form action="<?php echo $actionUrl; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return validateNewsSearch()">
        <div class="row user-status">        
           <div class="col-md-1 padding-right0  bulk-label col-sm-6 top7 margin-topDefault form-inline"><label class="top7"> Bulk Action</label></div>
           <div class="col-md-2 col-sm-6 margin-topDefault form-inline">
               <select class="customeSelect howDidYou form-control  select2me confirmBtn" id="bulkaction" name="bulkaction">
                   <option value="NA">Select</option>
                   <option value="Active" <?php if($bulkaction == "Active"){ echo "selected"; } ?>>Active</option>
                   <option value="Inactive" <?php if($bulkaction == "Inactive"){ echo "selected"; } ?>>Inactive</option>
                   <option value="Remove" <?php if($bulkaction == "Remove"){ echo "selected"; } ?>>Remove</option>
               </select>
           </div>
           <input type="hidden" value="0" id="newsflag" name="newsflag" />
            <span style="color:red" id="error_bulkaction_news"></span>
           <div class="col-md-2 col-sm-6 margin-topDefault form-inline">
               <input type="submit" class="btn btn-success margintop3" value="OK" name="bulkaction_submit" id="bulkaction_submit"/>
           </div>        
       </div>
       <div class="clearfix"></div>
         <div class="blue-table">
       <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{summary}{pager}<div class="table-responsive">{items}</div> {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                    'name' => '',
                    'header' => '<span style="white-space: nowrap;"><input type="checkbox" onclick="selectAll(this)" id="selectall"></span>',
                    'value' => array($this, 'GetNewsCheckbox'),
                ),
                array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">Sl.No</span>',
                ),
                array(
                    'name' => 'title',
                    'header' => '<span style="white-space: nowrap;">Title &nbsp;</span>',
                    'value' => '($data->title)?$data->title:""',
                ),
                array(
                    'name' => 'news',
                    'header' => '<span style="white-space: nowrap;">News &nbsp;</span>',
                    'value' => '($data->news)?strip_tags($data->news):""',
                ),
                array(
                    'name' => 'image',
                    'header' => '<span style="white-space: nowrap;">Image &nbsp; &nbsp; &nbsp;</span>',
                     'value' => function($data) {
                                $image = "";
                                if($data->image != ""){
                                    $image = '<img src="'.Yii::app()->params['imagePath']['news'].$data->image.'" style="width: 80px;height: 50px;">';
                                }
                                    echo $image;
                                },
                ),
                array(
                    'name' => 'publish_date',
                    'header' => '<span style="white-space: nowrap;">Publish Date</span>',
                    'value' => '($data->publish_date)?$data->publish_date:""',
                ),
                array(
                    'name' => 'expiry_date',
                    'header' => '<span style="white-space: nowrap;">Expiry Date</span>',
                    'value' => '($data->expiry_date)?$data->expiry_date:""',
                ),
                array(
                    'name' => 'status',
                    'header' => '<span style="white-space: nowrap;">Status</span>',
                    'value' => '($data->status == 1)?"Active":"Inactive"',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Edit}',
                    'htmlOptions' => array('width' => '10%'),
                    'buttons' => array(
                        'Edit' => array(
                            'label' => '<i class="fa fa-pencil-square-o"></i>',
                            'options' => array('class' => 'action-icons', 'title' => 'Edit'),
                            'url' => 'Yii::app()->createUrl("admin/cms/news", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
        </div>
        </form>
        </div>
    </div>
        
    </div>
</div>