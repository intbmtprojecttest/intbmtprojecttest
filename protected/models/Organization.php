<?php

/**
 * This is the model class for table "organization".
 *
 * The followings are the available columns in table 'organization':
 * @property integer $id
 * @property string $registered_name
 * @property string $brand_name
 * @property string $logo
 * @property string $address
 * @property integer $zipcode
 * @property string $registration_on
 * @property string $directores
 * @property string $ceo
 * @property string $coo
 * @property string $cto
 * @property string $pan
 * @property string $tan
 * @property integer $total_staff
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Organization extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'organization';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('registered_name, logo, address, zipcode, registration_on, directores, ceo, coo, cto, pan, tan, total_staff, created_at, updated_at', 'required'),
			array('zipcode, total_staff', 'numerical', 'integerOnly'=>true),
			array('registered_name, brand_name, logo', 'length', 'max'=>100),
			array('address, directores', 'length', 'max'=>255),
			array('ceo, coo, cto, tan', 'length', 'max'=>30),
			array('pan', 'length', 'max'=>15),
			array('status', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, registered_name, brand_name, logo, address, zipcode, registration_on, directores, ceo, coo, cto, pan, tan, total_staff, status, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'registered_name' => 'Registered Name',
			'brand_name' => 'Brand Name',
			'logo' => 'Logo',
			'address' => 'Address',
			'zipcode' => 'Zipcode',
			'registration_on' => 'Registration On',
			'directores' => 'Directores',
			'ceo' => 'Ceo',
			'coo' => 'Coo',
			'cto' => 'Cto',
			'pan' => 'Pan',
			'tan' => 'Tan',
			'total_staff' => 'Total Staff',
			'status' => 'Status',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('registered_name',$this->registered_name,true);
		$criteria->compare('brand_name',$this->brand_name,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('zipcode',$this->zipcode);
		$criteria->compare('registration_on',$this->registration_on,true);
		$criteria->compare('directores',$this->directores,true);
		$criteria->compare('ceo',$this->ceo,true);
		$criteria->compare('coo',$this->coo,true);
		$criteria->compare('cto',$this->cto,true);
		$criteria->compare('pan',$this->pan,true);
		$criteria->compare('tan',$this->tan,true);
		$criteria->compare('total_staff',$this->total_staff);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Organization the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
