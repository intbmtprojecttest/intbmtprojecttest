<?php
date_default_timezone_set('UTC'); // Date Time changes. 
// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
if ($_SERVER['SERVER_ADDR'] == '199.193.6.217') {
    $config=dirname(__FILE__).'/protected/config/production.php';
    // remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG',true);
} elseif ($_SERVER['SERVER_ADDR'] == '162.144.197.77') {
    $config=dirname(__FILE__).'/protected/config/development.php';
    // remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG',true);
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    $config=dirname(__FILE__).'/protected/config/local.php';
    defined('YII_DEBUG') or define('YII_DEBUG',true);
}
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',1);
require_once($yii);
Yii::createWebApplication($config)->run();
?>